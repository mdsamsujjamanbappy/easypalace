<?php
Route::get('/logout', 'Auth\LoginController@logout');
Route::get('/notification/message', 'DashboardController@message')->name('notification.message');
Route::get('/welcome-mail', 'DashboardController@mail');

Auth::routes();

Route::group(['middleware' => 'auth'], function () {

    Route::get('/home',['middleware'=>'check-permission:super_admin|admin|executive|customer','uses'=>'DashboardController@dashboard'])->name('home');	
    Route::get('/',['middleware'=>'check-permission:super_admin|admin|executive|customer', 'uses'=>'DashboardController@dashboard'])->name('dashboard'); 
    Route::get('/dashboard',['middleware'=>'check-permission:super_admin|admin|executive|customer', 'uses'=>'DashboardController@dashboard'])->name('dashboard');	

    Route::get('/employee/profile',['middleware'=>'check-permission:super_admin|admin|executive|customer','uses'=>'EmployeeListController@my_profile'])->name('employee.my_profile');
    Route::post('/employee/profile/password/update',['middleware'=>'check-permission:super_admin|admin|executive|customer','uses'=>'EmployeeListController@my_profile_password_update'])->name('employee.my_profile_password_update');

    // Manage Company
    Route::get('/company/list',['middleware'=>'check-permission:super_admin|admin','uses'=>'CompanyInformationController@company_information_list'])->name('company_information.list');   
    Route::post('/company/new/store',['middleware'=>'check-permission:super_admin|admin','uses'=>'CompanyInformationController@store'])->name('company_information.new.store');   
    Route::get('/company/edit/{id}',['middleware'=>'check-permission:super_admin|admin','uses'=>'CompanyInformationController@edit'])->name('company_information.edit');
    Route::get('/company/details/{id}',['middleware'=>'check-permission:super_admin|admin','uses'=>'CompanyInformationController@details'])->name('company_information.details');
    Route::post('/company/update',['middleware'=>'check-permission:super_admin|admin','uses'=>'CompanyInformationController@update'])->name('company_information.update');    
    Route::get('/company/destroy/{id}',['middleware'=>'check-permission:super_admin|admin','uses'=>'CompanyInformationController@destroy'])->name('company_information.destroy');
     
    // Manage Department
    Route::get('/department/list',['middleware'=>'check-permission:super_admin|admin','uses'=>'DepartmentController@department_list'])->name('department.list');
    Route::post('/department/new/store',['middleware'=>'check-permission:super_admin|admin','uses'=>'DepartmentController@store'])->name('department.new.store');
    Route::get('/department/edit/{id}',['middleware'=>'check-permission:super_admin|admin','uses'=>'DepartmentController@edit'])->name('department.edit');
    Route::post('/department/update',['middleware'=>'check-permission:super_admin|admin','uses'=>'DepartmentController@update'])->name('department.update');    
    Route::get('/department/destroy/{id}',['middleware'=>'check-permission:super_admin|admin','uses'=>'DepartmentController@destroy'])->name('department.destroy');
    
    // Manage Designation
    Route::get('/designation/list',['middleware'=>'check-permission:super_admin|admin','uses'=>'DesignationController@designation_list'])->name('designation.list'); 
    Route::post('/designation/new/store',['middleware'=>'check-permission:super_admin|admin','uses'=>'DesignationController@store'])->name('designation.new.store');
    Route::get('/designation/edit/{id}',['middleware'=>'check-permission:super_admin|admin','uses'=>'DesignationController@edit'])->name('designation.edit');
    Route::post('/designation/update',['middleware'=>'check-permission:super_admin|admin','uses'=>'DesignationController@update'])->name('designation.update');    
    Route::get('/designation/destroy/{id}',['middleware'=>'check-permission:super_admin|admin','uses'=>'DesignationController@destroy'])->name('designation.destroy');
     
    // Manage Gender
    Route::get('/gender/list',['middleware'=>'check-permission:super_admin|admin','uses'=>'GenderController@gender_list'])->name('gender.list'); 
    Route::post('/gender/new/store',['middleware'=>'check-permission:super_admin|admin','uses'=>'GenderController@store'])->name('gender.new.store');
    Route::get('/gender/edit/{id}',['middleware'=>'check-permission:super_admin|admin','uses'=>'GenderController@edit'])->name('gender.edit');
    Route::post('/gender/update',['middleware'=>'check-permission:super_admin|admin','uses'=>'GenderController@update'])->name('gender.update');    
    Route::get('/gender/destroy/{id}',['middleware'=>'check-permission:super_admin|admin','uses'=>'GenderController@destroy'])->name('gender.destroy');

    // Manage Work Shift 
    Route::get('/work_shift/list',['middleware'=>'check-permission:super_admin|admin','uses'=>'EmployeeWorkShiftController@work_shift_list'])->name('work_shift.list'); 
    Route::post('/work_shift/new/store',['middleware'=>'check-permission:super_admin|admin','uses'=>'EmployeeWorkShiftController@store'])->name('work_shift.new.store');
    Route::get('/work_shift/edit/{id}',['middleware'=>'check-permission:super_admin|admin','uses'=>'EmployeeWorkShiftController@edit'])->name('work_shift.edit');
    Route::post('/work_shift/update',['middleware'=>'check-permission:super_admin|admin','uses'=>'EmployeeWorkShiftController@update'])->name('work_shift.update');    
    Route::get('/work_shift/weekend/edit/{id}',['middleware'=>'check-permission:super_admin|admin','uses'=>'EmployeeWorkShiftController@weekend_edit'])->name('work_shift.weekend.edit');
    Route::post('/work_shift/weekend/update',['middleware'=>'check-permission:super_admin|admin','uses'=>'EmployeeWorkShiftController@weekend_update'])->name('work_shift.weekend.update');    
    Route::get('/work_shift/destroy/{id}',['middleware'=>'check-permission:super_admin|admin','uses'=>'EmployeeWorkShiftController@destroy'])->name('work_shift.destroy');

    // Manage Weekend Holiday
    Route::get('/weekend_holiday/list',['middleware'=>'check-permission:super_admin|admin','uses'=>'WeekendHolidayController@weekend_holiday_list'])->name('weekend_holiday.list'); 
 
    // Manage Employee
    Route::get('/employee/list',['middleware'=>'check-permission:super_admin|admin','uses'=>'EmployeeListController@employee_list'])->name('employee.list');   
    Route::get('/employee/details/{id}',['middleware'=>'check-permission:super_admin|admin','uses'=>'EmployeeListController@employee_details'])->name('employee.details');
    Route::post('/employee/update',['middleware'=>'check-permission:super_admin|admin','uses'=>'EmployeeListController@update_employee_information'])->name('employee.information.update');    
    Route::get('/employee/new/create',['middleware'=>'check-permission:super_admin|admin','uses'=>'EmployeeListController@new_employee'])->name('employee.new.create');   
    Route::POST('/employee/new/store',['middleware'=>'check-permission:super_admin|admin','uses'=>'EmployeeListController@store_new_employee'])->name('employee.new.store');   
    Route::post('employee/profile/photo/update', ['middleware' => 'check-permission:super_admin|admin', 'uses' => 'EmployeeListController@employee_profile_photo'])->name('employee.profile.photo.update');

    Route::post('/employee/password/update',['middleware'=>'check-permission:super_admin|admin','uses'=>'EmployeeListController@employee_password_update'])->name('employee.employee_password_update');

    Route::post('/employee/access_role/create',['middleware'=>'check-permission:super_admin|admin','uses'=>'EmployeeListController@employee_access_role_create'])->name('employee.employee_access_role_create');
    Route::post('/employee/access_role/update',['middleware'=>'check-permission:super_admin|admin','uses'=>'EmployeeListController@employee_access_role_update'])->name('employee.employee_access_role_update');
 
    Route::post('/employee/nominee/store',['middleware'=>'check-permission:super_admin|admin','uses'=>'EmployeeNomineeController@employee_nominee_store'])->name('employee_nominee.store');
    Route::get('/employee/nominee/destroy/{id}',['middleware'=>'check-permission:super_admin|admin','uses'=>'EmployeeNomineeController@employee_nominee_destroy'])->name('employee_nominee.destroy');

    Route::post('/employee/education/store',['middleware'=>'check-permission:super_admin|admin','uses'=>'EmployeeEducationController@employee_education_store'])->name('employee_education.store');
    Route::get('/employee/education/destroy/{id}',['middleware'=>'check-permission:super_admin|admin','uses'=>'EmployeeEducationController@employee_education_destroy'])->name('employee_education.destroy');

    Route::post('/employee/employment/store',['middleware'=>'check-permission:super_admin|admin','uses'=>'EmployeeEmploymentController@employee_employment_store'])->name('employee_employment.store');
    Route::get('/employee/employment/destroy/{id}',['middleware'=>'check-permission:super_admin|admin','uses'=>'EmployeeEmploymentController@employee_employment_destroy'])->name('employee_employment.destroy');

    Route::post('/employee/additional_info/store',['middleware'=>'check-permission:super_admin|admin','uses'=>'EmployeeAdditionalInfoController@employee_additional_info_store'])->name('employee_additional_info.store');
    Route::get('/employee/additional_info/destroy/{id}',['middleware'=>'check-permission:super_admin|admin','uses'=>'EmployeeAdditionalInfoController@employee_additional_info_destroy'])->name('employee_additional_info.destroy');
    
    Route::get('/employee/list/general',['middleware'=>'check-permission:executive','uses'=>'EmployeeListController@employee_list_general'])->name('employee.list.general');   

    //Employee Attendance Management
    Route::get('/employee/attendance/history/today',['middleware'=>'check-permission:super_admin|admin','uses'=>'EmployeeAttendanceController@todays_attendance'])->name('employee.attendance.todays_attendance');   
    Route::get('/employee/attendance/history/daily_view',['middleware'=>'check-permission:super_admin|admin','uses'=>'EmployeeAttendanceController@daily_attendance_view'])->name('employee.attendance.daily_attendance_view');   
    Route::post('/employee/attendance/history/daily',['middleware'=>'check-permission:super_admin|admin','uses'=>'EmployeeAttendanceController@daily_attendance'])->name('employee.attendance.daily_attendance');   
    Route::get('/employee/attendance/import/excel',['middleware'=>'check-permission:super_admin|admin','uses'=>'EmployeeAttendanceImporterController@import_from_excel'])->name('employee.attendance.import_from_excel');   
    Route::post('/employee/attendance/import/excel/store',['middleware'=>'check-permission:super_admin|admin','uses'=>'EmployeeAttendanceImporterController@import_from_excel_store'])->name('employee.attendance.import_from_excel_store');

    Route::get('/employee/attendance/history/date_wise_view',['middleware'=>'check-permission:super_admin|admin','uses'=>'EmployeeAttendanceController@date_wise_attendance_view'])->name('employee.attendance.date_wise_attendance_view');   
    Route::POST('/employee/attendance/history/date_wise_data',['middleware'=>'check-permission:super_admin|admin','uses'=>'EmployeeAttendanceController@date_wise_attendance_data'])->name('employee.attendance.date_wise_attendance_data');

    Route::get('/employee/attendance/history/myself',['middleware'=>'check-permission:admin|executive','uses'=>'EmployeeAttendanceController@myself_date_wise_attendance_view'])->name('employee.attendance.myself_date_wise_attendance_view');   
    Route::POST('/employee/attendance/history/myself_data',['middleware'=>'check-permission:admin|executive','uses'=>'EmployeeAttendanceController@myself_date_wise_attendance_data'])->name('employee.attendance.myself_date_wise_attendance_data');

    Route::get('/employee/attendance/give_attendance/app',['middleware'=>'check-permission:admin|executive','uses'=>'EmployeeAttendanceController@give_attendance_by_app'])->name('employee.attendance.give_attendance_by_app');   

    Route::post('/employee/attendance/give_attendance/app/checkin',['middleware'=>'check-permission:admin|executive','uses'=>'EmployeeAttendanceController@give_attendance_by_app_checkin'])->name('employee.attendance.give_attendance_by_app.checkin');   

    Route::post('/employee/attendance/give_attendance/app/checkout',['middleware'=>'check-permission:admin|executive','uses'=>'EmployeeAttendanceController@give_attendance_by_app_checkout'])->name('employee.attendance.give_attendance_by_app.checkout');   

    Route::get('/test',['middleware'=>'check-permission:super_admin|admin|executive','uses'=>'EmployeeAttendanceImporterController@test'])->name('employee.attendance.test');   


    //Employee Leave Management
    Route::get('/settings/leave/type_list',['middleware'=>'check-permission:super_admin|admin','uses'=>'EmployeeLeaveManagementController@leave_type_list'])->name('employee.leave.type_list');   
    Route::post('/settings/leave/type_list/store',['middleware'=>'check-permission:super_admin|admin','uses'=>'EmployeeLeaveManagementController@leave_type_store'])->name('settings.leave.type_list.store');   
    Route::get('/settings/leave/type_list/edit/{id}',['middleware'=>'check-permission:super_admin|admin','uses'=>'EmployeeLeaveManagementController@leave_type_edit'])->name('settings.leave.type_list.edit');   
    Route::post('/settings/leave/type_list/update',['middleware'=>'check-permission:super_admin|admin','uses'=>'EmployeeLeaveManagementController@leave_type_update'])->name('settings.leave.type_list.update');   
    Route::get('/settings/leave/type_list/destroy/{id}',['middleware'=>'check-permission:super_admin|admin','uses'=>'EmployeeLeaveManagementController@leave_type_destroy'])->name('settings.leave.type_list.destroy');   

    //Employee Holiday Management
    Route::get('/settings/leave/holidays_observances/list',['middleware'=>'check-permission:super_admin|admin','uses'=>'EmployeeLeaveManagementController@holidays_list'])->name('settings.leave.holidays_observances_list');   
    Route::post('/settings/leave/holidays_observances/store',['middleware'=>'check-permission:super_admin|admin','uses'=>'EmployeeLeaveManagementController@holiday_store'])->name('settings.leave.holidays_observances.store');   
    Route::get('/settings/leave/holidays_observances/edit/{id}',['middleware'=>'check-permission:super_admin|admin','uses'=>'EmployeeLeaveManagementController@holidays_observances_edit'])->name('settings.leave.holidays_observances.edit');   
    Route::post('/settings/leave/holidays_observances/update',['middleware'=>'check-permission:super_admin|admin','uses'=>'EmployeeLeaveManagementController@holidays_observances_update'])->name('settings.leave.holidays_observances.update');   
    Route::get('/settings/leave/holidays_observances/destroy/{id}',['middleware'=>'check-permission:super_admin|admin','uses'=>'EmployeeLeaveManagementController@holidays_observances_destroy'])->name('settings.leave.holidays_observances.destroy');   

    // Assign Leave 
    Route::get('/employee/leave/assign/form',['middleware'=>'check-permission:super_admin|admin','uses'=>'EmployeeLeaveManagementController@leave_assign_view'])->name('employee.leave.assign.view');   
    Route::get('/ajax/get_employee/company_department/{company_id}/{department_id}',['middleware'=>'check-permission:super_admin|admin','uses'=>'EmployeeLeaveManagementController@get_ajax_employee_list'])->name('ajax.employee_list.company_department');   
    Route::get('/ajax/assign_leave/get_leave_type/{id}','EmployeeLeaveManagementController@get_ajax_leave_type')->name('ajax.assign_leave.get_leave_type');
    Route::get('/ajax/assign_leave/get_employee_available_leave/{leave_id}/{emp_id}','EmployeeLeaveManagementController@get_ajax_employee_available_leave')->name('ajax.assign_leave.get_employee_available_leave');
    
    Route::post('/employee/leave/assign/store',['middleware'=>'check-permission:super_admin|admin','uses'=>'EmployeeLeaveManagementController@leave_assign_store'])->name('employee.leave.assign.store');   
    
    // Employee Leave Request 
    Route::get('/employee/leave/new_request',['middleware'=>'check-permission:super_admin|admin|executive','uses'=>'EmployeeLeaveManagementController@employee_new_leave_request'])->name('employee.leave.new_request');   
    
    // Manage Employee Award List
    Route::get('/employee_award/view',['middleware'=>'check-permission:super_admin|admin','uses'=>'EmployeeAwardManagementController@employee_award_view'])->name('employee_award.view');
    Route::post('/employee_award/data',['middleware'=>'check-permission:super_admin|admin','uses'=>'EmployeeAwardManagementController@employee_award_data'])->name('employee_award.data');
    Route::get('/employee_award/new',['middleware'=>'check-permission:super_admin|admin','uses'=>'EmployeeAwardManagementController@new_award'])->name('employee_award.new');
    Route::post('/employee_award/new/store',['middleware'=>'check-permission:super_admin|admin','uses'=>'EmployeeAwardManagementController@store'])->name('employee_award.new.store');
    Route::get('/employee_award/destroy/{id}',['middleware'=>'check-permission:super_admin|admin','uses'=>'EmployeeAwardManagementController@destroy'])->name('employee_award.destroy');

    // Manage Notice List
    Route::get('/notice_list/list',['middleware'=>'check-permission:super_admin|admin','uses'=>'NoticeListController@notice_list'])->name('notice_list.list');
    Route::get('/notice_list/general',['middleware'=>'check-permission:executive','uses'=>'NoticeListController@notice_list_general'])->name('notice_list.general');
    Route::get('/notice_list/new',['middleware'=>'check-permission:super_admin|admin','uses'=>'NoticeListController@new_notice'])->name('notice_list.new');
    Route::post('/notice_list/new/store',['middleware'=>'check-permission:super_admin|admin','uses'=>'NoticeListController@store'])->name('notice_list.new.store');
    Route::get('/notice_list/destroy/{id}',['middleware'=>'check-permission:super_admin|admin','uses'=>'NoticeListController@destroy'])->name('notice_list.destroy');

    // Manage Customers
    Route::get('/customer/list',['middleware'=>'check-permission:super_admin|admin|executive','uses'=>'CustomerListController@customer_list'])->name('customer.list');   
    Route::get('/customer/create',['middleware'=>'check-permission:super_admin|admin|executive','uses'=>'CustomerListController@create'])->name('customer.create');   
    Route::post('/customer/store',['middleware'=>'check-permission:super_admin|admin|executive','uses'=>'CustomerListController@store'])->name('customer.store');   
    Route::post('/customer/photo/update',['middleware'=>'check-permission:super_admin|admin|executive','uses'=>'CustomerListController@customer_profile_photo'])->name('customer.photo.update');   
    Route::get('/customer/details/{id}',['middleware'=>'check-permission:super_admin|admin|executive','uses'=>'CustomerListController@details'])->name('customer.details');   
    Route::get('/customer/edit/{id}',['middleware'=>'check-permission:super_admin|admin|executive','uses'=>'CustomerListController@edit'])->name('customer.edit');   
    Route::post('/customer/update',['middleware'=>'check-permission:super_admin|admin|executive','uses'=>'CustomerListController@update'])->name('customer.update');   
    Route::get('/customer/destroy/{id}',['middleware'=>'check-permission:super_admin|admin|executive','uses'=>'CustomerListController@destroy'])->name('customer.destroy');   


    // Manage Room Category
    Route::get('/room_category/list',['middleware'=>'check-permission:super_admin|admin','uses'=>'RoomCategoryController@list'])->name('room_category.list');
    Route::post('/room_category/new/store',['middleware'=>'check-permission:super_admin|admin','uses'=>'RoomCategoryController@store'])->name('room_category.new.store');
    Route::get('/room_category/edit/{id}',['middleware'=>'check-permission:super_admin|admin','uses'=>'RoomCategoryController@edit'])->name('room_category.edit');
    Route::post('/room_category/update',['middleware'=>'check-permission:super_admin|admin','uses'=>'RoomCategoryController@update'])->name('room_category.update');    
    Route::get('/room_category/destroy/{id}',['middleware'=>'check-permission:super_admin|admin','uses'=>'RoomCategoryController@destroy'])->name('room_category.destroy');


    // Manage Room 
    Route::get('/hotel_room/list',['middleware'=>'check-permission:super_admin|admin','uses'=>'HotelRoomListController@list'])->name('hotel_room.list');
    Route::post('/hotel_room/new/store',['middleware'=>'check-permission:super_admin|admin','uses'=>'HotelRoomListController@store'])->name('hotel_room.new.store');
    Route::get('/hotel_room/edit/{id}',['middleware'=>'check-permission:super_admin|admin','uses'=>'HotelRoomListController@edit'])->name('hotel_room.edit');
    Route::post('/hotel_room/update',['middleware'=>'check-permission:super_admin|admin','uses'=>'HotelRoomListController@update'])->name('hotel_room.update');    
    Route::get('/hotel_room/destroy/{id}',['middleware'=>'check-permission:super_admin|admin','uses'=>'HotelRoomListController@destroy'])->name('hotel_room.destroy');

    // Manage Expense Category
    Route::get('/expense_category/list',['middleware'=>'check-permission:super_admin|admin','uses'=>'ExpenseCategoryListController@list'])->name('expense_category.list');
    Route::post('/expense_category/new/store',['middleware'=>'check-permission:super_admin|admin','uses'=>'ExpenseCategoryListController@store'])->name('expense_category.new.store');
    Route::get('/expense_category/edit/{id}',['middleware'=>'check-permission:super_admin|admin','uses'=>'ExpenseCategoryListController@edit'])->name('expense_category.edit');
    Route::post('/expense_category/update',['middleware'=>'check-permission:super_admin|admin','uses'=>'ExpenseCategoryListController@update'])->name('expense_category.update');    
    Route::get('/expense_category/destroy/{id}',['middleware'=>'check-permission:super_admin|admin','uses'=>'ExpenseCategoryListController@destroy'])->name('expense_category.destroy');

    // Manage Expense List
    Route::get('/expense_list/view',['middleware'=>'check-permission:super_admin|admin','uses'=>'ExpenseListController@expense_list_view'])->name('expense_list.view');
    Route::post('/expense_list/data',['middleware'=>'check-permission:super_admin|admin','uses'=>'ExpenseListController@expense_list_data'])->name('expense_list.data');
    Route::get('/expense_list/new',['middleware'=>'check-permission:super_admin|admin','uses'=>'ExpenseListController@new_expense'])->name('expense_list.new');
    Route::post('/expense_list/new/store',['middleware'=>'check-permission:super_admin|admin','uses'=>'ExpenseListController@store'])->name('expense_list.new.store');
    Route::get('/expense_list/destroy/{id}',['middleware'=>'check-permission:super_admin|admin','uses'=>'ExpenseListController@destroy'])->name('expense_list.destroy');

    // Manage Customer Reservation 
    Route::get('/customer/reservation/new',['middleware'=>'check-permission:super_admin|admin|executive','uses'=>'CustomerReservationInformationController@new_reservation'])->name('customer_reservation.new_reservation');
    Route::get('/ajax/room_type/get_hotel_room/{id}',['middleware'=>'check-permission:super_admin|admin|executive','uses'=>'CustomerReservationInformationController@ajax_get_hotel_room'])->name('customer_reservation.ajax.get_hotel_room');
    Route::post('/customer/reservation/new/store',['middleware'=>'check-permission:super_admin|admin|executive','uses'=>'CustomerReservationInformationController@new_reservation_store'])->name('customer_reservation.new_reservation.store');
    Route::post('/customer/reservation/cancellation/store',['middleware'=>'check-permission:super_admin|admin|executive','uses'=>'CustomerReservationInformationController@cancellation_store'])->name('customer_reservation.cancellation.store');
    Route::post('/customer/reservation/checkout/store',['middleware'=>'check-permission:super_admin|admin|executive','uses'=>'CustomerReservationInformationController@reservation_checkout_store'])->name('customer_reservation.checkout.store');
    Route::get('/customer/reservation/list',['middleware'=>'check-permission:super_admin|admin|executive','uses'=>'CustomerReservationInformationController@reservation_list'])->name('customer_reservation.reservation_list');
    Route::get('/customer/reservation/list/excel',['middleware'=>'check-permission:super_admin|admin|executive','uses'=>'CustomerReservationInformationController@reservation_list_excel'])->name('customer_reservation.reservation_list_excel');
    Route::get('/customer/reservation/due_payment/list',['middleware'=>'check-permission:super_admin|admin|executive','uses'=>'CustomerReservationInformationController@due_payment_reservation_list'])->name('customer_reservation.due_payment_reservation_list');
    Route::get('/customer/reservation/due_payment/list/excel',['middleware'=>'check-permission:super_admin|admin|executive','uses'=>'CustomerReservationInformationController@due_payment_reservation_list_excel'])->name('customer_reservation.due_payment_reservation_list_excel');

    Route::get('/customer/reservation/cancelled/list',['middleware'=>'check-permission:super_admin|admin|executive','uses'=>'CustomerReservationInformationController@cancelled_reservation_list'])->name('customer_reservation.cancelled_reservation_list');
    Route::get('/customer/reservation/cancelled/details/{id}',['middleware'=>'check-permission:super_admin|admin|executive','uses'=>'CustomerReservationInformationController@cancelled_reservation_details'])->name('customer_reservation.cancelled_reservation_details');
    Route::get('/customer/reservation/cancelled/list/excel',['middleware'=>'check-permission:super_admin|admin|executive','uses'=>'CustomerReservationInformationController@cancelled_reservation_list_excel'])->name('customer_reservation.cancelled_reservation_list_excel');
       
    Route::get('/customer/reservation/details/{id}',['middleware'=>'check-permission:super_admin|admin|executive','uses'=>'CustomerReservationInformationController@reservation_details'])->name('customer_reservation.reservation_details');
    Route::get('/customer/reservation/cancellation/{id}',['middleware'=>'check-permission:super_admin|admin|executive','uses'=>'CustomerReservationInformationController@reservation_cancellation'])->name('customer_reservation.cancellation');
    Route::post('/customer/reservation/billing_information/store',['middleware'=>'check-permission:super_admin|admin|executive','uses'=>'CustomerReservationInformationController@billing_information_store'])->name('customer_reservation.billing_information.store');
    Route::post('/customer/reservation/payment_information/store',['middleware'=>'check-permission:super_admin|admin|executive','uses'=>'CustomerReservationInformationController@payment_information_store'])->name('customer_reservation.payment_information.store');
    
    Route::get('/customer/reservation/billing/destroy/{id}',['middleware'=>'check-permission:super_admin|admin|executive','uses'=>'CustomerReservationInformationController@billing_destroy'])->name('customer_reservation.billing.destroy');
    Route::get('/customer/reservation/payment/destroy/{id}',['middleware'=>'check-permission:super_admin|admin|executive','uses'=>'CustomerReservationInformationController@payment_destroy'])->name('customer_reservation.payment.destroy');
    
    Route::get('/customer/reservation/checkout/{id}',['middleware'=>'check-permission:super_admin|admin|executive','uses'=>'CustomerReservationInformationController@reservation_checkout'])->name('customer_reservation.reservation_checkout');
  
    // Manage Customer Service Request
    Route::get('/customer_request/pending_list',['middleware'=>'check-permission:super_admin|admin|executive','uses'=>'CustomerServiceRequestController@pending_list'])->name('customer_request.pending_list');
    Route::get('/customer_request/onprocessing_list',['middleware'=>'check-permission:super_admin|admin|executive','uses'=>'CustomerServiceRequestController@onprocessing_list'])->name('customer_request.onprocessing_list');
    Route::get('/customer_request/completed_list',['middleware'=>'check-permission:super_admin|admin|executive','uses'=>'CustomerServiceRequestController@completed_list'])->name('customer_request.completed_list');
    Route::get('/customer_request/rejected_list',['middleware'=>'check-permission:super_admin|admin|executive','uses'=>'CustomerServiceRequestController@rejected_list'])->name('customer_request.rejected_list');
    
    Route::post('/customer_request/accept',['middleware'=>'check-permission:super_admin|admin|executive','uses'=>'CustomerServiceRequestController@accept_request'])->name('customer_request.accept_request');
    Route::post('/customer_request/reject',['middleware'=>'check-permission:super_admin|admin|executive','uses'=>'CustomerServiceRequestController@reject_request'])->name('customer_request.reject_request');

    Route::get('/customer_portal/reservation/list',['middleware'=>'check-permission:customer','uses'=>'CustomerReservationInformationController@customer_panel_reservation_list'])->name('customer_portal.reservation_list');
    Route::get('/customer_portal/service_request_list',['middleware'=>'check-permission:customer','uses'=>'CustomerServiceRequestController@customer_service_request_list'])->name('customer_portal.service_request_list');
    Route::get('/customer_portal/new_service_request',['middleware'=>'check-permission:customer','uses'=>'CustomerServiceRequestController@new_service_request'])->name('customer_portal.new_service_request');
    Route::post('/customer_portal/new_service_request/store',['middleware'=>'check-permission:customer','uses'=>'CustomerServiceRequestController@new_service_request_store'])->name('customer_portal.new_service_request.store');

});

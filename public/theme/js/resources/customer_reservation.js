$(function(){
  'use strict';
    

        $('#discount_percentage').on('keyup', function() {
            var current_room_rent_amount = $('#current_room_rent_amount').val();
            var current_billing_amount = $('#current_billing_amount').val();
            var discount_percentage = $('#discount_percentage').val();
            var paid_amount = $('#paid_amount').val();
            // var discount_amount = $('#discount_amount').val();
            $('#discount_amount_lbl').html((current_room_rent_amount/100)*discount_percentage);
            $('#discount_amount').val((current_room_rent_amount/100)*discount_percentage);
            // alert((current_room_rent_amount/100)*discount_percentage);
            $('#calculate_payable_amount').click();
        });


        $('#calculate_payable_amount').on('click', function() {
            var current_room_rent_amount = parseInt($('#current_room_rent_amount').val());
            var current_billing_amount = parseInt($('#current_billing_amount').val());
            var discount_percentage = parseInt($('#discount_percentage').val());
            var previous_due_amount = parseInt($('#previous_due_amount').val());
            var current_payment_amount = parseInt($('#current_payment_amount').val());
            var discount_amount = parseInt($('#discount_amount').val());

            var total_payable_amount  = ((current_room_rent_amount+current_billing_amount+previous_due_amount)-(current_payment_amount+discount_amount));
            $('#total_payable_amount').html(total_payable_amount);
            $('#calculate_summary_amount').click();

        });

        $('#paid_amount').on('keyup', function() {
            $('#discount_percentage').keyup();
            $('#calculate_payable_amount').click();
            $('#calculate_summary_amount').click();
        });

        $('#calculate_summary_amount').on('click', function() {
            var current_room_rent_amount = parseInt($('#current_room_rent_amount').val());
            var current_billing_amount = parseInt($('#current_billing_amount').val());
            var discount_percentage = parseInt($('#discount_percentage').val());
            var previous_due_amount = parseInt($('#previous_due_amount').val());
            var paid_amount = parseInt($('#paid_amount').val());
            var discount_amount = parseInt($('#discount_amount').val());
            var current_payment_amount = parseInt($('#current_payment_amount').val());
            if(discount_amount==""){
                discount_amount=0;
            }
            var total_due_amount  = ((current_room_rent_amount+current_billing_amount+previous_due_amount)-(current_payment_amount+paid_amount+discount_amount));
            $('#total_due_amount').html(total_due_amount);
            $('#total_due_amount_input').val(total_due_amount);
        });
        
        $('#calculate_summary_amount').click();


});

-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 20, 2020 at 12:28 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_hrpalace`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(31, '2014_10_12_000000_create_users_table', 1),
(32, '2014_10_12_100000_create_password_resets_table', 1),
(33, '2019_08_19_000000_create_failed_jobs_table', 1),
(34, '2020_01_09_034113_create_company_information_table', 1),
(35, '2020_01_29_035326_create_employee_list_table', 1),
(36, '2020_01_29_040341_create_department_list_table', 1),
(37, '2020_01_29_040400_create_designation_list_table', 1),
(38, '2020_01_29_040427_create_gender_list_table', 1),
(39, '2020_01_29_040500_create_work_shift_list_table', 1),
(40, '2020_01_29_040705_create_employee_education_info_table', 1),
(41, '2020_01_29_040818_create_employee_others_info_table', 1),
(42, '2020_01_29_040833_create_employee_work_history_table', 1),
(43, '2020_01_29_040932_create_employee_nominee_table', 1),
(44, '2020_02_13_100716_create_attendance_history_table', 1),
(45, '2020_02_13_111257_create_weekend_holiday_table', 1),
(46, '2020_02_29_053017_create_employee_leave_application_table', 1),
(47, '2020_02_29_091207_create_attendance_history_tmp_table', 1),
(48, '2020_03_02_125625_create_app_attendance_history_table', 1),
(49, '2020_03_03_153047_create_employee_shift_weekend_table', 1),
(50, '2020_03_03_175107_create_employee_leave_type_setting_table', 1),
(51, '2020_03_07_124155_create_holidays_observances_leave_table', 1),
(52, '2020_03_11_170541_create_jobs_table', 1),
(53, '2020_03_16_170623_create_customer_list_table', 1),
(54, '2020_03_18_212212_create_room_category_table', 1),
(55, '2020_03_18_215517_create_hotel_room_list_table', 1),
(56, '2020_03_22_202741_create_customer_reservation_table', 1),
(57, '2020_03_22_203331_create_reservation_room_table', 1),
(59, '2020_03_25_170842_create_reservation_billing_head_table', 1),
(60, '2020_03_25_172602_create_reservation_payment_table', 1),
(61, '2020_03_25_170403_create_reservation_billing_table', 2),
(64, '2020_05_24_154528_create_reservation_checkout_history_table', 3),
(65, '2020_05_27_142900_create_customer_reservation_cancellation_table', 4),
(70, '2020_05_28_101003_create_expense_category_table', 5),
(71, '2020_05_28_101140_create_expense_list_table', 5),
(72, '2020_06_15_112632_create_employee_award_history_table', 6),
(73, '2020_06_15_124830_create_notice_list_table', 6),
(77, '2020_06_20_120527_create_service_request_list_table', 7);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_app_attendance_history`
--

CREATE TABLE `tb_app_attendance_history` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `attendance_ref_id` int(11) DEFAULT NULL,
  `emp_id` int(11) DEFAULT NULL,
  `attendance_date` date DEFAULT NULL,
  `check_in` time DEFAULT NULL,
  `check_out` time DEFAULT NULL,
  `check_in_lat` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `check_in_long` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `check_out_lat` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `check_out_long` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `check_in_ip` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `check_out_ip` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_attendance_history`
--

CREATE TABLE `tb_attendance_history` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `emp_id` int(11) DEFAULT NULL,
  `attendance_date` date DEFAULT NULL,
  `in_time` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `out_time` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attendance_type` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_attendance_history_tmp`
--

CREATE TABLE `tb_attendance_history_tmp` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `emp_id` int(11) DEFAULT NULL,
  `attendance_date` date DEFAULT NULL,
  `punch_time` time DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_company_information`
--

CREATE TABLE `tb_company_information` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `company_logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_tagline` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_phone` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_address1` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_address2` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT 1 COMMENT '1 for Active, 0 for Inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_company_information`
--

INSERT INTO `tb_company_information` (`id`, `company_logo`, `company_name`, `company_tagline`, `company_phone`, `company_email`, `company_address1`, `company_address2`, `status`, `created_at`, `updated_at`) VALUES
(1, 'default.png', 'EasyPalace', 'One Of The Leading Hotels In Dhaka', '+880-1824168996', 'samsujjamanbappy@gmail.com', '31/D Topkhana Road, Dhaka 1000, Bangladesh', NULL, 1, '2020-03-26 14:31:46', '2020-03-26 14:31:46');

-- --------------------------------------------------------

--
-- Table structure for table `tb_customer_list`
--

CREATE TABLE `tb_customer_list` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `customer_id` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_name` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `father_name` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mother_name` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_of_birth` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_address1` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_address2` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_email` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_phone` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emergency_contact` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nid_passport` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_photo` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nid_frontside` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nid_backside` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `p_id` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` varchar(4) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_customer_list`
--

INSERT INTO `tb_customer_list` (`id`, `customer_id`, `customer_name`, `father_name`, `mother_name`, `date_of_birth`, `customer_address1`, `customer_address2`, `customer_email`, `customer_phone`, `emergency_contact`, `nid_passport`, `status`, `customer_photo`, `nid_frontside`, `nid_backside`, `p_id`, `company_name`, `remarks`, `created_by`, `created_at`, `updated_at`) VALUES
(4, '2', 'MR.CHINMOY CHOWDHORY', 'MR.LATE CHITTA RONJAN CHOWDHORY', NULL, '28-11-1959', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', 'DO', '', '0171122334', '0175566889', '332256565889', 'REGULER', 'default.jpg', 'default.jpg', 'default.jpg', '75', '', 'NON AC 10% AC 15%', NULL, NULL, NULL),
(5, '3', 'MD. RASHEDUL ALAM MAMUN', 'ALHAZ MOHAMMED JANE ALAM', NULL, '20-01-1971', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', 'yahia tower 335 cda avenue,lalkhan bazar,chitagong.', 'managingdirector@pdgbd.com', '0171122334', '0175566889', '332256565889', 'REGULER', 'default.jpg', 'default.jpg', 'default.jpg', '148', '', '30%', NULL, NULL, NULL),
(6, '4', 'ALHAZ SYED ALI', 'Late Alhaz nasir Uddin Sarker', NULL, '12-01-1955', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', 'do', '', '0171122334', '0175566889', '332256565889', 'REGULER', 'default.jpg', 'default.jpg', 'default.jpg', '943', '', '15%', NULL, NULL, NULL),
(7, '5', 'AHAMADUR RAHMAN AKRAM', 'Late moh. Solaiman', NULL, '15-11-1973', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', 's. Haque chamber (3rdfloor) 45-47 stand road,majirghat,chittagong', 'ahmed.group17@yahoo.com', '0171122334', '0175566889', '332256565889', 'REGULER', 'default.jpg', 'default.jpg', 'default.jpg', '3122', '', 'business man (15%)', NULL, NULL, NULL),
(8, '6', 'MD. ANISUR RAHMAN', 'MD. ABDUL SATTAR', NULL, '05-03-1973', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', '', '', '0171122334', '0175566889', '332256565889', 'REGULER', 'default.jpg', 'default.jpg', 'default.jpg', '1248', '', 'Upzilla Chairman Taragonj, Rangpur.', NULL, NULL, NULL),
(9, '7', 'MOHAMMED FAHIMUL ISLAM', 'Late Alhaz Farrukh Ahmed', NULL, '16-04-1968', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', '73/1 Green Road,(S.ALM PROPERTIS),FIRM GATE,DHAKA-1215', 'ajc.dhaka@gmail.com', '0171122334', '0175566889', '332256565889', 'vip', 'default.jpg', 'default.jpg', 'default.jpg', '115', 'A&J CONSTRUCTION', 'DIS-20%', NULL, NULL, NULL),
(10, '8', 'DIDARUL ALAM', 'ABUL BASHAR CHY', NULL, '01-08-1982', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', '', '', '0171122334', '0175566889', '332256565889', 'REGULER', 'default.jpg', 'default.jpg', 'default.jpg', '4402', '', '15%', NULL, NULL, NULL),
(11, '9', 'MD SALIM UDDIN', 'Late Moh.jamal uddin', NULL, '04-12-1979', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', '', '', '0171122334', '0175566889', '332256565889', 'REGULER', 'default.jpg', 'default.jpg', 'default.jpg', '7129', '', '15%', NULL, NULL, NULL),
(12, '10', 'MAHABUB ZAMAN BHULU', 'MOZIR UDDIN AHMED', NULL, '01-03-1950', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', 'Bangladesh Redcrescent Socity', '', '0171122334', '0175566889', '332256565889', 'vip', 'default.jpg', 'default.jpg', 'default.jpg', '485', '', 'DEAD', NULL, NULL, NULL),
(13, '11', 'MD.SHAKHAWAT HOSSAIN (PARTHO)', 'MD.SHAHADAT HOSAAIN', NULL, '08-09-1993', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', '', '', '0171122334', '0175566889', '332256565889', 'REGULER', 'default.jpg', 'default.jpg', 'default.jpg', '', '', 'P.S. of Mahabub Zaman Bhulu', NULL, NULL, NULL),
(14, '12', 'MD.GOLAM NABI', 'MD.LOKMAN HOSSAIN', NULL, '01-05-1955', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', '', '', '0171122334', '0175566889', '332256565889', 'REGULER', 'default.jpg', 'default.jpg', 'default.jpg', '88', '', '15%', NULL, NULL, NULL),
(15, '13', 'RABIUL ISLAM', 'ABDUL GAFFUR', NULL, '14-08-1952', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', 'KUSTIA CHAMBER OF COMERCE & INDUSTRY', '', '0171122334', '0175566889', '332256565889', 'vip', 'default.jpg', 'default.jpg', 'default.jpg', '5284', '', 'KUSTIA ZILLA AWAMI LEAGUE\r\n20%', NULL, NULL, NULL),
(16, '14', 'AUNGSUIPRU CHOWDHURY', 'KUNGSUIPRU CHOWDHURY', NULL, '25-05-1964', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', 'Rangamati ZIlla Parishaod', '', '0171122334', '0175566889', '332256565889', 'REGULER', 'default.jpg', 'default.jpg', 'default.jpg', '7780', '', '20%', NULL, NULL, NULL),
(17, '15', 'S.M.MOHSIN', 'S.M.SORMAN ALI', NULL, '04-03-1964', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', 'KHULNA FOOD OFFICE', '', '0171122334', '0175566889', '332256565889', 'REGULER', 'default.jpg', 'default.jpg', 'default.jpg', '2695', '', '15% first choice room 607/508', NULL, NULL, NULL),
(18, '16', 'AKHTER ALAM', 'Late Sher Ullah', NULL, '10-05-1960', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', '', '', '0171122334', '0175566889', '332256565889', 'REGULER', 'default.jpg', 'default.jpg', 'default.jpg', '2920', '', '15%', NULL, NULL, NULL),
(19, '17', 'RIYAJ UL ALAM', 'Late Ahmad Kabir', NULL, '11-07-1972', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', '', '', '0171122334', '0175566889', '332256565889', 'REGULER', 'default.jpg', 'default.jpg', 'default.jpg', '1695', '', '', NULL, NULL, NULL),
(20, '18', 'NASIR UDDIN AHMED', 'Late Shamsuddin Ahmed', NULL, '28-12-1954', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', '', '', '0171122334', '0175566889', '332256565889', 'VIP', 'default.jpg', 'default.jpg', 'default.jpg', '50', '', 'mayor chandpur pourashobha\r\nFirst choice room  601(dis:20%)', NULL, NULL, NULL),
(21, '19', 'MD.SHAHJAHAN MIA', 'Late Ali Asraf Mia', NULL, '07-07-1963', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', '', '', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '449', '', '', NULL, NULL, NULL),
(22, '20', 'Sk. Fakharuddin Chy.', 'Late Sekh Mostfa Ali Chy', NULL, '07-08-1963', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', '', '', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '524', '', 'dis: 15%', NULL, NULL, NULL),
(23, '21', 'RAFIQ AHAMED CHOWDHURY', 'LATE AHAMED HOSSAIN CHY', NULL, '26-01-1955', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', '', '', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '6471', '', 'FOOD', NULL, NULL, NULL),
(24, '22', 'SAZZAD HOSSAIN CHOWDHURY', 'ATM AFAZUDDIN CHY', NULL, '18-10-1966', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', '', '', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '5134', '', 'BUSINESS', NULL, NULL, NULL),
(25, '23', 'MOHAMMAD MANZUR HASAN', 'MOHAMMAD YUNUS', NULL, '01-08-1968', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', 'CHITTAGONG DEVLOPMENT AUTHURITY', '', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '', '', 'ENGR', NULL, NULL, NULL),
(26, '24', 'NAZRUL ISLAM CHOWDHURY', 'LATE FAZLUL HOQUE', NULL, '10-05-1947', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', '', '', '0171122334', '0175566889', '332256565889', 'VIP', 'default.jpg', 'default.jpg', 'default.jpg', '500', '', 'POLITICAL LEADER\r\n(First choice room )707', NULL, NULL, NULL),
(27, '25', 'MD.ABDUL AHAD BHUIYAN', 'ABDUR RAHIM BHUIYAN', NULL, '01-12-1976', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', 'T.K GROUP,CTG', '', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '17', '', 'T K GROUP', NULL, NULL, NULL),
(28, '26', 'PRITOM PAUL', 'RANJAN KANTI PAUL', NULL, '17-11-1992', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', 'S.ALAM GROUP', '', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '4239', '', 'S.ALAM GROUP', NULL, NULL, NULL),
(29, '27', 'MD.MAHBUBUL ALAM', 'ALHAJ SAIFUL MULOK', NULL, '01-01-1963', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', 'FEDAREL INSURANCE CO.', '', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '853', '', 'FIC', NULL, NULL, NULL),
(30, '28', 'SYED MAHMUDUL HOQ', 'SYED ABU SIDDIUQE', NULL, '01-01-1957', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', '', '', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '463', '', 'POLITICAL LEADER', NULL, NULL, NULL),
(31, '29', 'MOHAMMAD MAHEEDUL HOSSAIN', 'LATE MOJAKKER HOSSAIN', NULL, '21-06-1971', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', '', '', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '5495', '', 'KAFCO', NULL, NULL, NULL),
(32, '30', 'MD.ROBIUL ISLAM', 'LATE TORU MIAH MOLLA', NULL, '22-10-1964', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', '', '', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '3730', '', '', NULL, NULL, NULL),
(33, '31', 'GOLAM HADI', 'GOLAM RAZZAK', NULL, '10-06-1966', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', '', '', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '4930', '', '', NULL, NULL, NULL),
(34, '32', 'ZAHANGIR ALAM', 'MOKTHAR AHMED', NULL, '06-03-1978', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', '263 JUBILEE ROAD,CTG', '', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '2578', '', 'DIS-10/15%', NULL, NULL, NULL),
(35, '33', 'MOHAMMED ZALAL UDDIN CHY', 'LATE ABDUL MOTALEB CHY', NULL, '20-01-1966', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', 'MINISTRY OF EDUCATION', '', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '2892', '', 'DIS-15/10', NULL, NULL, NULL),
(36, '34', 'MUKTADER MOWLA MILLAD', 'ABUL HOSSAIN', NULL, '31-12-1973', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', 'KAZI TRADING(01816807899)\r\n1637/A KARIM BHABAN,P.C ROAD,NAYABAZAR,CTG', '', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '5678', '', 'DIS-15/10', NULL, NULL, NULL),
(37, '35', 'ABUL FAZAL MD.ANAMUL HAQUE', 'LATE HELAL UDDIN', NULL, '26-11-1974', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', '', '', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '6296', '', 'DIS-15/10', NULL, NULL, NULL),
(38, '36', 'MUFTI MUHAMMAD IZHARUL ISLAM', 'KAJI AZIZ AHMED', NULL, '03-04-1945', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', 'KHAN BAHADUR BARI,BOILCHARI,BANSKHALI,CTG\r\nP.P NO:BP0926425', '', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '6140', '', '', NULL, NULL, NULL),
(39, '37', 'Mohammed Shawkat Hossain', 'Late Nuruzzaman', NULL, '27-10-2017', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', 'SBL CHATKTAI BRANCH, 300 CHAUPOTTI, NATUN CHAKTAI, CHITTAGONG.', '', '0171122334', '0175566889', '332256565889', '', 'default.jpg', 'default.jpg', 'default.jpg', '2300', '', '', NULL, NULL, NULL),
(40, '38', 'MOHAMMAD KHORSHED ALAM', 'MOHAMMAD YAKUB ALI', NULL, '27-10-2017', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', 'SBL, CEPZ BRANCH, GAZI COMPLEX (1st Floor), CEPZ, CHITTAGONG', '', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '6130', '', '', NULL, NULL, NULL),
(41, '39', 'MOHAMMAD AMAN ULLAH JAHANGIR', 'MASTER M A JAHER', NULL, '01-01-1980', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', '', '', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '1357', '', 'DIS-15/10%', NULL, NULL, NULL),
(42, '40', 'MOHAMMED YOUSUF', 'LATE JALAL AHMED', NULL, '01-07-1963', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', '', '', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '2124', '', 'DIS-20%', NULL, NULL, NULL),
(43, '41', 'MD. NURUL AZIM RONY', 'MD. NURUL ABSER', NULL, '08-08-1988', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', 'DARUL FAZAL MARKET, BSL, CHITTAGONG.', '', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '4238', '', 'DIS-15/10%', NULL, NULL, NULL),
(44, '42', 'MOHAMMED EKRAMUL HOQ', 'MOHAMMED NURUL HOQ', NULL, '17-03-1970', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', '', '', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '1120', '', 'DIS-15/10%', NULL, NULL, NULL),
(45, '43', 'MOHAMMED ZOBAIER', 'LATE MOFIZUR RASHMAN', NULL, '08-04-1974', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', '', '', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '4420', '', 'DIS-15/10%', NULL, NULL, NULL),
(46, '44', 'MOHAMMAD KAMAL PASHA', 'LATE MOHAMMAD NURUL ALAM', NULL, '01-09-1947', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', '', '', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '3391', '', 'DIS-15/10%', NULL, NULL, NULL),
(47, '45', 'MD SALIM', 'LATE HFIZUR RAHMAN', NULL, '24-11-1959', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', '', '', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '6059', '', 'REF:RASHEDUL ALAM MAMUN', NULL, NULL, NULL),
(48, '46', 'MOHAMMAD SHIDUR RAHMAN', 'MOHAMMAD MANNAN ALI', NULL, '07-08-1976', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', 'ASSTRCH', '', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '51', '', 'DIS-20%', NULL, NULL, NULL),
(49, '47', 'MD ABEDIN AL MAMUN', 'MD.JAINAL ABEDIN SUJA', NULL, '01-03-1980', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', 'SITAKUNDU,CTG', 'abedin@pacificjns.com', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '3809', 'PACIFIC JEANS', 'dis-15/10%', NULL, NULL, NULL),
(50, '48', 'NURUL ABSER', 'LATE BELAYET ALI', NULL, '16-07-1973', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', '29 TAMAKUMONDI LANE,PROFESSOR MARKET(3RD FLR),REAZUDDIN BAZAR,CTG', 'nurulabsar655@yahoo.com', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '1068', '', 'DIS-15/10%', NULL, NULL, NULL),
(51, '49', 'MD.MAHAFUJUL HOQUE SAJU', 'LATE HAJI MAMTAJUL HOQUE', NULL, '03-01-1957', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', 'RUMA ENTERPRISE,KHATUNONJ CTG', '', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '3134', '', 'DIS-20%', NULL, NULL, NULL),
(52, '50', 'MOHAMMAD BELAL', 'JAHURUL HOQ', NULL, '10-05-1970', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', '', '', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '2742', '', 'DS-15/10%', NULL, NULL, NULL),
(53, '51', 'MOHAMMAD AMINUR RAHMAN', 'LATE KAZI ABDUR RASHID', NULL, '01-12-1961', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', 'AL-ARAFAH ISLAMIBANK LTD,KHATUNGONJ BRANCH,CTG', 'msramin2@gmail.com', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '4280', '', 'DIS-20%\r\nID:4280', NULL, NULL, NULL),
(54, '52', 'A.S.M.MOHSIN SARKER', 'SARAFAT ALI SARKER', NULL, '01-01-1968', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', '', '', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '2868', '', 'DIS-15/10%', NULL, NULL, NULL),
(55, '53', 'MD.MONIRUZZAMAN', 'SOLIM UDDIN FAKIR', NULL, '09-05-1953', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', 'MAYOR,KHULNA CITY CORPORATION', '', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '4745', '', 'DIS-15%', NULL, NULL, NULL),
(56, '54', 'MORSHEDUL SHAFI', 'AHMED SHAFI', NULL, '15-01-1977', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', 'CHANDGAON R/A,BLOCK*A,ROAD*02,CHANDGAON,CHITTAGONG\r\nP:P NO:AE3708767', '', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '21', '', 'DIS-25%,FIC', NULL, NULL, NULL),
(57, '55', 'MOHAMMAD QUASHEM', 'LATE OMARA MEAH', NULL, '17-11-1947', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', 'R*2,H*27,FLAT-5/A,NASIRABAD HOUSING SOCITY,CTG', '', '0171122334', '0175566889', '332256565889', 'VIP', 'default.jpg', 'default.jpg', 'default.jpg', '1420', '', 'DIS-30%', NULL, NULL, NULL),
(58, '56', 'KAZI MORSHED AHMED BABU', 'LATE KAZI TOFAYEL AHMED', NULL, '05-06-976', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', 'LIGHT HOUSE ROAD,12 NO WARD,COXSBAZAR', '', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '2104', '', 'DIS-10/15%', NULL, NULL, NULL),
(59, '57', 'MD.KHURSHID ALAM', 'MD.YAKUB', NULL, '10-05-1965', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', '', '', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '6310', '', 'DIS-10/20%', NULL, NULL, NULL),
(60, '58', 'MD MOINUL ALAM SAYAN', 'MD KHURSHID ALAM', NULL, '28-08-2000', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', 'BP0472055', '', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '4268', '', 'DIS-10/15%', NULL, NULL, NULL),
(61, '59', 'PRODIP KUMAR AGARWAL', 'HARDOAR AGARWAL', NULL, '16-01-1963', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', 'DAKSHIN BORGACHA,STATION BAZAR,NATORE', '', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '', '', 'DIS-10/15%', NULL, NULL, NULL),
(62, '60', 'ANWER SADAT', 'MD.SHADAT HOSSAIN', NULL, '12-02-1975', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', '', '', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '3630', '', 'DIS-15/10%', NULL, NULL, NULL),
(63, '61', 'A.K.M NURUL HOQUE', 'LATE ABDUL KHALEQUE', NULL, '20-10-1955', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', 'SHEKERKIL,(MOULOVEPARA),SHEKERKIL,BANSKHALI,CTG', '', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '193', '', 'DS-20%', NULL, NULL, NULL),
(64, '62', 'ASIF MAHFUZ', 'ABUL HAYET', NULL, '02-03-1980', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', '', '', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '6121', '', 'DIS-15/10%', NULL, NULL, NULL),
(65, '63', 'MOSTAK AHMED ANGUR', 'LATE NAZIR AHMED', NULL, '10-03-1963', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', 'able shipping line\r\n23/b ,s.f.chamber,strand road,chittagong', '', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '1620', '', 'DIS-25%', NULL, NULL, NULL),
(66, '64', 'MD.PARVEZ MAHMUD', 'MD.ZAYES UDDIN', NULL, '29-10-2017', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', 'RAJSHAHI WASA EXN', '', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '1671', '', 'DIS-10/15%', NULL, NULL, NULL),
(67, '65', 'MD.JANE ALAM', 'LATE HAZI BADSHA MEAH', NULL, '02-11-1961', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', 'ICDP,CHTDB\r\nPROJECT MANAGER\r\nRANGAMATTI', '', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '113', '', 'DIS-10/15%', NULL, NULL, NULL),
(68, '66', 'SHIHAB UDDIN RATAN', 'LATE A,F,M MONSEF ALI', NULL, '31-01-1962', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', 'ADVOCATE CTG DISTRICT COURT', '', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '631', '', 'DIS-30%', NULL, NULL, NULL),
(69, '67', 'MD.FAZLA ELAHEE', 'LATE HABIBUR RAHMAN', NULL, '01-07-1967', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', '969 EAST NASIRABAD,CTG', '', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '2000', '', 'DIS-10/15%', NULL, NULL, NULL),
(70, '68', 'SEIK YESEEN ARFAT GONI', 'SULTAN GANI CHY', NULL, '04-07-1988', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', 'RIAD STORE,5/A BANSKHALI BHABAN,IQBAL ROAD,PATHARGHATA,CHITTAGONG', '', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '', '', 'DIS-15/10%', NULL, NULL, NULL),
(71, '69', 'HAJI SADER AHMED', 'NAZIR AHMED', NULL, '01-06-1948', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', '', '', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '1729', '', 'DIS-10/15%', NULL, NULL, NULL),
(73, '71', 'ABDUL HAKIM', 'LATE ABDUR RAHMAN', NULL, '10-07-1960', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', '', '', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '', '', 'DIS-10/15%', NULL, NULL, NULL),
(74, '72', 'MUHAMMAD SHAHINUR EKRAM CHY', 'MD.SHAHIDUL ISLAM CHY', NULL, '01-11-1986', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', 'DIRECTOR,\r\nNATIONAL LIFE INSURANCE CO.', '', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '5101', '', 'DIS-20%', NULL, NULL, NULL),
(75, '73', 'MD.SYFULLAH ANCHARI', 'MD.ABU JAFAR ANCHARI', NULL, '06-06-1981', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', '', '', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '1172', '', 'DIS10/20%', NULL, NULL, NULL),
(76, '74', 'MUZHARUL ISLAM CHOWDHURY', 'NURUL ISLAM CHOWDHURY', NULL, '19-06-1952', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', '', '', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '', '', 'DSI-10/15%', NULL, NULL, NULL),
(77, '75', 'MOHAMMED ASIF IQBAL', 'ABU YOUSUF MD MOSTAFA', NULL, '06-03-1993', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', '', '', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '43', '', 'DIS-10/15%', NULL, NULL, NULL),
(78, '76', 'NIAZ AHAMAD RASHED', 'SAMSUDDIN AHAMAD', NULL, '30-12-1975', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', 'BANK ASIA', '', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '', '', 'DIS10/15%', NULL, NULL, NULL),
(79, '77', 'HELAL UDDIN', 'ABDUL MANNAN', NULL, '12-11-1978', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', '', '', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '1018', '', 'DIS-10/15%', NULL, NULL, NULL),
(80, '78', 'RABI ROY DAS', 'AJIT KANTI DAS', NULL, '15-07-1962', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', '', '', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '', '', 'DIS-10/15%', NULL, NULL, NULL),
(81, '79', 'MD ABUL KASHEM', 'LATE AKERUZZAMAN', NULL, '30-06-1958', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', 'BBC STEEL BHABAN SOUTH SITINPUR SITAKONDO,CHITTAGONG.', '', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '2297', '', 'DIS-15%', NULL, NULL, NULL),
(82, '80', 'MOHAMMAD ALI', 'MD.IBRAHIM SHOWDAGAR', NULL, '01-01-1970', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', '7 ABY MITTERGHAT,PATHARGHATA,CTG', '', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '196', '', 'DIS-10/15%', NULL, NULL, NULL),
(83, '81', 'SHYAMAL CHOWDHURY', 'CHIRA RANJAN CHOWDHURY', NULL, '02-10-1963', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', '74/B SK MUJROAD(2ND FLOOR),AGRABAD,CTG\r\nBB0840522', 'intdynamic@gmail.com', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '1207', '', 'dis-15/20%', NULL, NULL, NULL),
(84, '82', 'MD.ABUL MASUD CHOWDHURY BOKUL', 'ABUL KASHEM CHY', NULL, '17-12-1970', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', '', '', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '433', '', 'DIS-10/15%', NULL, NULL, NULL),
(85, '83', 'YAKUB MD SHAHJAHAN', 'MD SHAMSUL ALAM', NULL, '30-06-1973', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', 'SBL,JUBILEE ROAD BRANCH,CTG', '', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '', '', 'DIS-15/20%', NULL, NULL, NULL),
(86, '84', 'S M SALEH NOOR SIDDIQUE', 'LATE MD IDRIS MEAH', NULL, '02-12-1988', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', 'R*22,H*606(1ST FLR) CDA R/A,AGRABAD,CTG', 'ih.trading81@gmail.com', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '2961', '', 'DIS-10/20%', NULL, NULL, NULL),
(87, '85', 'SHAMSUL HUDA AHMED', 'MD.SHAFI', NULL, '12-01-1972', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', '734 D.T ROAD,DEWANHAT,CTG', 'rsacorporation@yahoo.com', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '', '', 'DIS-10/20%', NULL, NULL, NULL),
(88, '86', 'AHASANUL HAQUE', 'LATE OLLI AHMED', NULL, '08-05-1974', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', 'UP CHAIRMAN', '', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '2304', '', '15/20%', NULL, NULL, NULL),
(89, '87', 'MOHAMMAD BELAL UDDIN', 'MD.BASARAT ALI', NULL, '20-01-1983', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', 'HOTEL FARS\r\nSTORE KEEPER', '', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '', '', 'DIS-20%', NULL, NULL, NULL),
(90, '88', 'MD.KAMAL UDDIN', 'LATE SIRAJUL ISLAM', NULL, '15-04-1972', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', 'VIP TOWER (2ND FLR),KAZIR DEWRI,CTG', 'info.ornate@yahoo.com', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '', '', 'dis-10/15%', NULL, NULL, NULL),
(91, '89', 'Mohammed Mir Shahriar Kayes', 'Late Mir Hafez Ahmed', NULL, '01-02-1974', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', 'Standard Bank Ltd.', '', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '', '', '15/10%', NULL, NULL, NULL),
(92, '90', 'M. R. Azim', 'Late Md. Nurul Abser', NULL, '05-05-1974', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', '', '', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '2793', '', '20%', NULL, NULL, NULL),
(93, '91', 'Mustaque Ahmad', 'Abdul Gani', NULL, '02-02-1955', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', 'The Daily Observer, Karnafully Tower (7th Floor), S.S. Khaled Road,Chittagong.', 'mustaque.ahmed1955@gmail.com', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '1796', '', '', NULL, NULL, NULL),
(94, '92', 'Sayeed Faruk Ahamed', 'Alhaj M A Abdul Baten Mia', NULL, '15-02-1975', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', 'Rangpur Group, Medical East Gate, Rangpur.', 'sfahamed1975@gmail.com', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '4267', '', '10/15%', NULL, NULL, NULL),
(95, '93', 'Md. Musa Alam', 'Late Hajee Abdul Gafur', NULL, '24-06-1970', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', '12 no, O. R. Nizam Road, Panchlaish, Chittagong.', '', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '', '', '', NULL, NULL, NULL),
(96, '94', 'S M Jalil', 'Late Shunam Uddin', NULL, '05-10-1964', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', '', '', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '3486', '', '', NULL, NULL, NULL),
(97, '95', 'MOHAMMAD SAGIR', 'LATE MOHAMMAD SIRAJ', NULL, '18-08-1973', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', 'SUPER GROUP\r\nELITE HOUSE,CDA AVENUE O.R .NIZAM ROAD,CTG', 'sagirbd@yahoo.com', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '07', '', 'dis-15/10%', NULL, NULL, NULL),
(98, '96', 'MOHAMMED ARIFUR RAHMAN', 'MOHAMMED MOSTAFA', NULL, '01-07-1980', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', 'JAGHATPUR,PRATAPPUR,DAGONBHUIYAN,FENI', '', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '4991', '', 'DIS-15/10%', NULL, NULL, NULL),
(99, '97', 'MD.SHAHADAT HOSSAIN JEWEL', 'MD.SHA ALAM', NULL, '20-04-1983', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', '', '', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '', '', 'DIS-10/15%', NULL, NULL, NULL),
(100, '98', 'MD NURA ALAM RONY', 'LATE ABDUL WAHAB SARKER', NULL, '05-10-1979', 'Chowdhory bari,gonjor,companigonj,3542 ,moradnogar,comilla.', '', '', '0171122334', '0175566889', '332256565889', 'REGULAR', 'default.jpg', 'default.jpg', 'default.jpg', '', '', 'DIS-10/15%', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_customer_reservation_cancellation`
--

CREATE TABLE `tb_customer_reservation_cancellation` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `reservation_id` varchar(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reason_of_cancellation` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_customer_reservation_cancellation`
--

INSERT INTO `tb_customer_reservation_cancellation` (`id`, `reservation_id`, `reason_of_cancellation`, `created_by`, `created_at`, `updated_at`) VALUES
(1, '1', 'nONE', 2, '2020-05-27 08:33:56', '2020-05-27 08:33:56');

-- --------------------------------------------------------

--
-- Table structure for table `tb_customer_reservation_list`
--

CREATE TABLE `tb_customer_reservation_list` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `customer_id` varchar(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reservation_number` varchar(12) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reservation_date` date DEFAULT NULL,
  `check_in_date` date DEFAULT NULL,
  `check_out_date` date DEFAULT NULL,
  `adult_number` tinyint(4) DEFAULT NULL,
  `children_number` tinyint(4) DEFAULT NULL,
  `remarks` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '"1" is enable or  "0" disable',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_customer_reservation_list`
--

INSERT INTO `tb_customer_reservation_list` (`id`, `customer_id`, `reservation_number`, `reservation_date`, `check_in_date`, `check_out_date`, `adult_number`, `children_number`, `remarks`, `created_by`, `status`, `created_at`, `updated_at`) VALUES
(1, '4', '200527-001', '2020-05-27', '2020-05-05', '2020-05-05', 2, 6, 'None', 2, 1, '2020-05-27 08:14:53', '2020-05-27 08:33:56'),
(2, '5', '200527-002', '2020-05-27', '2020-05-05', '2020-05-05', 5, 2, NULL, 2, 1, '2020-05-27 08:16:02', '2020-05-27 08:16:02');

-- --------------------------------------------------------

--
-- Table structure for table `tb_department_list`
--

CREATE TABLE `tb_department_list` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `department_name` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '"1" is enable or  "0" disable',
  `created_by` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_department_list`
--

INSERT INTO `tb_department_list` (`id`, `department_name`, `remarks`, `status`, `created_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Account', 'None', 1, 2, '2020-03-26 14:31:46', '2020-05-29 03:06:37', NULL),
(2, 'Reception', 'None', 1, NULL, '2020-03-26 14:31:46', '2020-03-26 14:31:46', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_designation_list`
--

CREATE TABLE `tb_designation_list` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `designation_name` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '"1" is enable or  "0" disable',
  `created_by` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_designation_list`
--

INSERT INTO `tb_designation_list` (`id`, `designation_name`, `remarks`, `status`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'Manager', 'None', 1, NULL, '2020-03-26 14:31:46', '2020-03-26 14:31:46'),
(2, 'Asst. Manager', 'None', 1, NULL, '2020-03-26 14:31:46', '2020-03-26 14:31:46'),
(3, 'Staff', 'None', 1, NULL, '2020-03-26 14:31:46', '2020-03-26 14:31:46');

-- --------------------------------------------------------

--
-- Table structure for table `tb_employee_award_history`
--

CREATE TABLE `tb_employee_award_history` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `employee_id` bigint(20) DEFAULT NULL COMMENT 'employee_id will come from ("tb_employee_list") table',
  `award_name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `award_gift` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cash_price` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `award_period` date DEFAULT NULL,
  `remarks` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `award_attachment` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '"1" is enable or  "0" disable',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_employee_education_info`
--

CREATE TABLE `tb_employee_education_info` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `emp_id` bigint(20) DEFAULT NULL COMMENT 'emp_id will come from ("tb_employee_list.id") table',
  `emp_exam_title` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_institution_name` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_result` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_scale` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_passing_year` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_attachment` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_employee_leave_application`
--

CREATE TABLE `tb_employee_leave_application` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `unique_id` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `employee_id` bigint(20) DEFAULT NULL,
  `leave_type_id` bigint(20) DEFAULT NULL,
  `leave_starting_date` date DEFAULT NULL,
  `leave_ending_date` date DEFAULT NULL,
  `actual_days` int(11) DEFAULT NULL,
  `approved_by` tinyint(4) DEFAULT NULL,
  `attachment` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT 0,
  `created_by` varchar(4) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_employee_leave_type_setting`
--

CREATE TABLE `tb_employee_leave_type_setting` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `company_id` tinyint(4) DEFAULT NULL,
  `leave_type_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_leave_days` int(11) DEFAULT NULL,
  `remarks` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `status` tinyint(4) DEFAULT 1 COMMENT '"1" for active,  "0" for inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_employee_leave_type_setting`
--

INSERT INTO `tb_employee_leave_type_setting` (`id`, `company_id`, `leave_type_name`, `total_leave_days`, `remarks`, `created_by`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'Sick Leave', 10, NULL, 2, 1, '2020-05-27 15:20:24', '2020-05-27 15:20:24');

-- --------------------------------------------------------

--
-- Table structure for table `tb_employee_list`
--

CREATE TABLE `tb_employee_list` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `employee_id` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_id` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_first_name` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_last_name` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_department_id` int(11) DEFAULT NULL,
  `emp_designation_id` int(11) DEFAULT NULL,
  `emp_gender_id` int(11) DEFAULT NULL,
  `emp_shift_id` int(11) DEFAULT NULL,
  `emp_email` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_photo` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_dob` date DEFAULT NULL,
  `emp_joining_date` date DEFAULT NULL,
  `emp_probation_period` int(11) DEFAULT NULL,
  `emp_religion` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_marital_status` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_bank_account` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_bank_info` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_card_number` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_blood_group` varchar(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_of_discontinuation` date DEFAULT NULL,
  `reason_of_discontinuation` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_nid` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_nationality` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_parmanent_address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_current_address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_father_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_mother_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_ipbx_extension` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_account_status` tinyint(4) DEFAULT NULL,
  `created_by` varchar(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_employee_list`
--

INSERT INTO `tb_employee_list` (`id`, `employee_id`, `company_id`, `emp_first_name`, `emp_last_name`, `emp_department_id`, `emp_designation_id`, `emp_gender_id`, `emp_shift_id`, `emp_email`, `emp_phone`, `emp_photo`, `emp_dob`, `emp_joining_date`, `emp_probation_period`, `emp_religion`, `emp_marital_status`, `emp_bank_account`, `emp_bank_info`, `emp_card_number`, `emp_blood_group`, `date_of_discontinuation`, `reason_of_discontinuation`, `emp_nid`, `emp_nationality`, `emp_parmanent_address`, `emp_current_address`, `emp_father_name`, `emp_mother_name`, `emp_ipbx_extension`, `emp_account_status`, `created_by`, `created_at`, `updated_at`) VALUES
(1, '1001', '1', 'Samsujjaman', 'Bappy', 2, 1, 1, 1, 'bappy@email.com', NULL, NULL, '2020-05-27', NULL, NULL, NULL, NULL, NULL, 'admin@email.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Uttara', NULL, NULL, NULL, 1, '2', '2020-05-27 15:22:57', '2020-05-27 15:22:57');

-- --------------------------------------------------------

--
-- Table structure for table `tb_employee_nominee`
--

CREATE TABLE `tb_employee_nominee` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `emp_id` bigint(20) DEFAULT NULL COMMENT 'emp_id will come from ("tb_employee_list.id") table',
  `nominee_name` varchar(220) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nominee_details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nominee_photo` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nominee_phone` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nominee_relation` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nominee_address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `nominee_attachment` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_employee_others_info`
--

CREATE TABLE `tb_employee_others_info` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `emp_id` bigint(20) DEFAULT NULL COMMENT 'emp_id will come from ("tb_employee_list.id") table',
  `title` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attachment` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_employee_shift_weekend`
--

CREATE TABLE `tb_employee_shift_weekend` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `shift_id` tinyint(4) DEFAULT NULL,
  `day_name` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL COMMENT '"1" is weekend or  "0" Office Day',
  `created_by` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_employee_shift_weekend`
--

INSERT INTO `tb_employee_shift_weekend` (`id`, `shift_id`, `day_name`, `status`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 1, 'Friday', 1, 2, '2020-05-27 15:28:29', '2020-05-27 15:28:29');

-- --------------------------------------------------------

--
-- Table structure for table `tb_employee_work_history`
--

CREATE TABLE `tb_employee_work_history` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `emp_id` bigint(20) DEFAULT NULL COMMENT 'emp_id will come from ("tb_employee_list.id") table',
  `wh_company_name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wh_designation` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wh_joining_date` date DEFAULT NULL,
  `wh_resign_date` date DEFAULT NULL,
  `wh_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wh_attachment` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_expense_category`
--

CREATE TABLE `tb_expense_category` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_name` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '"1" is enable or  "0" disable',
  `created_by` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_expense_category`
--

INSERT INTO `tb_expense_category` (`id`, `category_name`, `description`, `status`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'Food', NULL, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_expense_list`
--

CREATE TABLE `tb_expense_list` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) DEFAULT NULL COMMENT 'category_id come from ("tb_expense_category") table',
  `amount` double(18,2) DEFAULT NULL,
  `expense_date` date DEFAULT NULL,
  `attachment` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '"1" is enable or  "0" disable',
  `approved_by` bigint(20) DEFAULT NULL COMMENT 'approved By',
  `created_by` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_expense_list`
--

INSERT INTO `tb_expense_list` (`id`, `category_id`, `amount`, `expense_date`, `attachment`, `remarks`, `status`, `approved_by`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 1, 500.00, '2020-05-12', '', 'None', 1, 2, 2, '2020-05-28 06:58:02', '2020-05-28 06:58:02'),
(2, 1, 2520.00, '2020-05-28', '', NULL, 1, 2, 2, '2020-05-28 07:06:52', '2020-05-28 07:06:52'),
(3, 1, 1500.00, '2020-05-28', '1590649711.jpg', NULL, 1, 2, 2, '2020-05-28 07:08:31', '2020-05-28 07:08:31');

-- --------------------------------------------------------

--
-- Table structure for table `tb_gender_list`
--

CREATE TABLE `tb_gender_list` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `gender_name` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '"1" is enable or  "0" disable',
  `created_by` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_gender_list`
--

INSERT INTO `tb_gender_list` (`id`, `gender_name`, `remarks`, `status`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'Male', 'None', 1, NULL, '2020-03-26 14:31:46', '2020-03-26 14:31:46'),
(2, 'Female', 'None', 1, NULL, '2020-03-26 14:31:46', '2020-03-26 14:31:46'),
(3, 'Other', 'None', 1, NULL, '2020-03-26 14:31:46', '2020-03-26 14:31:46');

-- --------------------------------------------------------

--
-- Table structure for table `tb_holidays_observances_leave`
--

CREATE TABLE `tb_holidays_observances_leave` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `company_id` tinyint(4) DEFAULT NULL,
  `holiday_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `remarks` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `status` tinyint(4) DEFAULT 1 COMMENT '"1" for active,  "0" for inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_holidays_observances_leave`
--

INSERT INTO `tb_holidays_observances_leave` (`id`, `company_id`, `holiday_title`, `start_date`, `end_date`, `remarks`, `created_by`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'ABC', '2020-05-05', '2020-05-05', 'AA', 2, 1, '2020-05-27 15:19:37', '2020-05-27 15:19:37');

-- --------------------------------------------------------

--
-- Table structure for table `tb_hotel_room_list`
--

CREATE TABLE `tb_hotel_room_list` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `room_name_number` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `room_category_id` varchar(4) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `room_rent_amount` varchar(12) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `room_facility_feature` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '"1" is enable or  "0" disable',
  `created_by` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_hotel_room_list`
--

INSERT INTO `tb_hotel_room_list` (`id`, `room_name_number`, `room_category_id`, `room_rent_amount`, `room_facility_feature`, `remarks`, `status`, `created_by`, `created_at`, `updated_at`) VALUES
(1, '1001', '1', '850', NULL, NULL, 1, NULL, '2020-03-26 14:42:39', '2020-03-26 14:42:39'),
(2, '1002', '1', '900', NULL, NULL, 1, NULL, '2020-03-26 14:42:39', '2020-03-26 14:42:39');

-- --------------------------------------------------------

--
-- Table structure for table `tb_notice_list`
--

CREATE TABLE `tb_notice_list` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `notice_title` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attachment` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '"1" is enable or  "0" disable',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_reservation_billing`
--

CREATE TABLE `tb_reservation_billing` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `reservation_id` varchar(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_head_id` varchar(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reservation_room_id` varchar(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_amount` varchar(12) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bill_date` date DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_reservation_billing`
--

INSERT INTO `tb_reservation_billing` (`id`, `reservation_id`, `billing_head_id`, `reservation_room_id`, `billing_description`, `billing_amount`, `bill_date`, `created_by`, `created_at`, `updated_at`) VALUES
(1, '1', '1', '1', 'Room No: 1001', '850', '2020-05-05', 2, '2020-05-27 08:14:53', '2020-05-27 08:14:53'),
(2, '2', '1', '2', 'Room No: 1001', '850', '2020-05-05', 2, '2020-05-27 08:16:02', '2020-05-27 08:16:02');

-- --------------------------------------------------------

--
-- Table structure for table `tb_reservation_billing_head`
--

CREATE TABLE `tb_reservation_billing_head` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `billing_head_name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '"1" is enable or  "0" disable',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_reservation_billing_head`
--

INSERT INTO `tb_reservation_billing_head` (`id`, `billing_head_name`, `created_by`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Room Rent', NULL, 1, NULL, NULL),
(2, 'Food', NULL, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_reservation_checkout_history`
--

CREATE TABLE `tb_reservation_checkout_history` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `reservation_id` varchar(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_bill_amount` varchar(12) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `advance_payment_amount` varchar(12) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paid_payment` varchar(12) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `previous_due_amount` varchar(12) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `current_due_amount` varchar(12) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_discount_amount` varchar(12) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discount_percentage_amount` tinyint(4) DEFAULT NULL,
  `due_status` tinyint(4) DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_reservation_payment`
--

CREATE TABLE `tb_reservation_payment` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `reservation_id` varchar(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_amount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_date` date DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_reservation_payment`
--

INSERT INTO `tb_reservation_payment` (`id`, `reservation_id`, `payment_description`, `payment_amount`, `payment_date`, `created_by`, `created_at`, `updated_at`) VALUES
(1, '2', 'Advance', '500', '2020-05-27', 4, '2020-05-27 15:31:33', '2020-05-27 15:31:33');

-- --------------------------------------------------------

--
-- Table structure for table `tb_reservation_room`
--

CREATE TABLE `tb_reservation_room` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `reservation_id` varchar(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `room_id` varchar(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_reservation_room`
--

INSERT INTO `tb_reservation_room` (`id`, `reservation_id`, `room_id`, `created_at`, `updated_at`) VALUES
(1, '1', '1', '2020-05-27 08:14:53', '2020-05-27 08:14:53'),
(2, '2', '1', '2020-05-27 08:16:02', '2020-05-27 08:16:02'),
(3, '1', '2', '2020-05-27 08:14:53', '2020-05-27 08:14:53');

-- --------------------------------------------------------

--
-- Table structure for table `tb_room_category`
--

CREATE TABLE `tb_room_category` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `room_category_name` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT 1 COMMENT '"1" for active,  "0" for inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_room_category`
--

INSERT INTO `tb_room_category` (`id`, `room_category_name`, `remarks`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Premium', NULL, 1, '2020-03-26 14:37:12', '2020-03-26 14:37:12');

-- --------------------------------------------------------

--
-- Table structure for table `tb_service_request_list`
--

CREATE TABLE `tb_service_request_list` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `reservation_id` varchar(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_title` varchar(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `request_date` date DEFAULT NULL,
  `request_details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `request_feedback` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `accepted_by` bigint(20) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_service_request_list`
--

INSERT INTO `tb_service_request_list` (`id`, `reservation_id`, `service_title`, `request_date`, `request_details`, `request_feedback`, `accepted_by`, `status`, `created_at`, `updated_at`) VALUES
(1, '1', 'Food', '2020-06-20', 'None', 'Ok', 2, 1, '2020-06-20 09:59:17', '2020-06-20 10:02:20');

-- --------------------------------------------------------

--
-- Table structure for table `tb_weekend_holiday`
--

CREATE TABLE `tb_weekend_holiday` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `day_name` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `company_id` varchar(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_weekend_holiday`
--

INSERT INTO `tb_weekend_holiday` (`id`, `day_name`, `status`, `company_id`, `created_at`, `updated_at`) VALUES
(1, 'Saturday', 1, '1', '2020-03-26 14:31:46', '2020-03-26 14:31:46'),
(2, 'Sunday', 0, '1', '2020-03-26 14:31:46', '2020-03-26 14:31:46'),
(3, 'Monday', 0, '1', '2020-03-26 14:31:46', '2020-03-26 14:31:46'),
(4, 'Tuesday', 0, '1', '2020-03-26 14:31:46', '2020-03-26 14:31:46'),
(5, 'Wednesday', 0, '1', '2020-03-26 14:31:46', '2020-03-26 14:31:46'),
(6, 'Thursday', 0, '1', '2020-03-26 14:31:46', '2020-03-26 14:31:46'),
(7, 'Friday', 1, '1', '2020-03-26 14:31:46', '2020-03-26 14:31:46');

-- --------------------------------------------------------

--
-- Table structure for table `tb_work_shift_list`
--

CREATE TABLE `tb_work_shift_list` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `shift_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `entry_time` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `exit_time` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `buffer_time` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '"1" is enable or  "0" disable',
  `created_by` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_work_shift_list`
--

INSERT INTO `tb_work_shift_list` (`id`, `shift_name`, `entry_time`, `exit_time`, `buffer_time`, `remarks`, `status`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'Default', '09:00', '18:00', '09:15', 'None', 1, 2, '2020-05-27 15:28:29', '2020-05-27 15:28:29');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `ref_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_type` tinyint(4) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `login_ip` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0 for inactive user and 1 for active user',
  `last_login_at` datetime DEFAULT NULL,
  `created_by` bigint(20) NOT NULL DEFAULT 1,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ref_id`, `user_type`, `company_id`, `name`, `username`, `email`, `password`, `login_ip`, `status`, `last_login_at`, `created_by`, `email_verified_at`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, NULL, 1, NULL, 'super admin', NULL, 'super@email.com', '$2y$10$7dbVYdL2ygqO1Lh5Lzf6C.TRT60xEekjgHWhFdnrB8.qt5udnjj/m', NULL, 1, '2020-03-26 20:31:46', 1, NULL, NULL, '2020-03-26 14:31:46', '2020-03-26 14:31:46'),
(2, NULL, 2, NULL, 'admin', '123', 'admin@email.com', '$2y$10$B2djweSVygQSh20oYl9JAewgT3sB578LXly6wpwWjZgjb0oXARb7e', NULL, 1, '2020-03-26 20:31:46', 1, NULL, NULL, '2020-03-26 14:31:46', '2020-03-26 14:31:46'),
(3, NULL, 3, NULL, 'Executive', NULL, 'executive@email.com', '$2y$10$tHyKGrXFgJ07lh7kKsNTc.10J.XY2CyKlMNedoaCQRbczUQ5W5o66', NULL, 1, '2020-03-26 20:31:46', 1, NULL, NULL, '2020-03-26 14:31:46', '2020-03-26 14:31:46'),
(4, '4', 4, 1, 'Samsujjaman Bappy', '01824168996', 'bappy@email.com', '$2y$10$B2djweSVygQSh20oYl9JAewgT3sB578LXly6wpwWjZgjb0oXARb7e', NULL, 1, NULL, 1, NULL, NULL, '2020-05-27 15:22:57', '2020-05-27 15:22:57');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_index` (`queue`(191));

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`(191));

--
-- Indexes for table `tb_app_attendance_history`
--
ALTER TABLE `tb_app_attendance_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tb_app_attendance_history_attendance_ref_id_index` (`attendance_ref_id`),
  ADD KEY `tb_app_attendance_history_emp_id_index` (`emp_id`);

--
-- Indexes for table `tb_attendance_history`
--
ALTER TABLE `tb_attendance_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tb_attendance_history_emp_id_index` (`emp_id`);

--
-- Indexes for table `tb_attendance_history_tmp`
--
ALTER TABLE `tb_attendance_history_tmp`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tb_attendance_history_tmp_emp_id_index` (`emp_id`);

--
-- Indexes for table `tb_company_information`
--
ALTER TABLE `tb_company_information`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_customer_list`
--
ALTER TABLE `tb_customer_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_customer_reservation_cancellation`
--
ALTER TABLE `tb_customer_reservation_cancellation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tb_customer_reservation_cancellation_reservation_id_index` (`reservation_id`);

--
-- Indexes for table `tb_customer_reservation_list`
--
ALTER TABLE `tb_customer_reservation_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_department_list`
--
ALTER TABLE `tb_department_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_designation_list`
--
ALTER TABLE `tb_designation_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_employee_award_history`
--
ALTER TABLE `tb_employee_award_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_employee_education_info`
--
ALTER TABLE `tb_employee_education_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tb_employee_education_info_emp_id_index` (`emp_id`);

--
-- Indexes for table `tb_employee_leave_application`
--
ALTER TABLE `tb_employee_leave_application`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tb_employee_leave_application_employee_id_index` (`employee_id`),
  ADD KEY `tb_employee_leave_application_leave_type_id_index` (`leave_type_id`);

--
-- Indexes for table `tb_employee_leave_type_setting`
--
ALTER TABLE `tb_employee_leave_type_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_employee_list`
--
ALTER TABLE `tb_employee_list`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tb_employee_list_company_id_index` (`company_id`),
  ADD KEY `tb_employee_list_emp_department_id_index` (`emp_department_id`),
  ADD KEY `tb_employee_list_emp_designation_id_index` (`emp_designation_id`),
  ADD KEY `tb_employee_list_emp_shift_id_index` (`emp_shift_id`);

--
-- Indexes for table `tb_employee_nominee`
--
ALTER TABLE `tb_employee_nominee`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tb_employee_nominee_emp_id_index` (`emp_id`);

--
-- Indexes for table `tb_employee_others_info`
--
ALTER TABLE `tb_employee_others_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tb_employee_others_info_emp_id_index` (`emp_id`);

--
-- Indexes for table `tb_employee_shift_weekend`
--
ALTER TABLE `tb_employee_shift_weekend`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_employee_work_history`
--
ALTER TABLE `tb_employee_work_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tb_employee_work_history_emp_id_index` (`emp_id`);

--
-- Indexes for table `tb_expense_category`
--
ALTER TABLE `tb_expense_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_expense_list`
--
ALTER TABLE `tb_expense_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_gender_list`
--
ALTER TABLE `tb_gender_list`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tb_gender_list_gender_name_unique` (`gender_name`);

--
-- Indexes for table `tb_holidays_observances_leave`
--
ALTER TABLE `tb_holidays_observances_leave`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_hotel_room_list`
--
ALTER TABLE `tb_hotel_room_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_notice_list`
--
ALTER TABLE `tb_notice_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_reservation_billing`
--
ALTER TABLE `tb_reservation_billing`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tb_reservation_billing_reservation_id_index` (`reservation_id`),
  ADD KEY `tb_reservation_billing_billing_head_id_index` (`billing_head_id`),
  ADD KEY `tb_reservation_billing_reservation_room_id_index` (`reservation_room_id`);

--
-- Indexes for table `tb_reservation_billing_head`
--
ALTER TABLE `tb_reservation_billing_head`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_reservation_checkout_history`
--
ALTER TABLE `tb_reservation_checkout_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tb_reservation_checkout_history_reservation_id_index` (`reservation_id`);

--
-- Indexes for table `tb_reservation_payment`
--
ALTER TABLE `tb_reservation_payment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tb_reservation_payment_reservation_id_index` (`reservation_id`);

--
-- Indexes for table `tb_reservation_room`
--
ALTER TABLE `tb_reservation_room`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_room_category`
--
ALTER TABLE `tb_room_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_service_request_list`
--
ALTER TABLE `tb_service_request_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_weekend_holiday`
--
ALTER TABLE `tb_weekend_holiday`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tb_weekend_holiday_company_id_index` (`company_id`);

--
-- Indexes for table `tb_work_shift_list`
--
ALTER TABLE `tb_work_shift_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;

--
-- AUTO_INCREMENT for table `tb_app_attendance_history`
--
ALTER TABLE `tb_app_attendance_history`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_attendance_history`
--
ALTER TABLE `tb_attendance_history`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_attendance_history_tmp`
--
ALTER TABLE `tb_attendance_history_tmp`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_company_information`
--
ALTER TABLE `tb_company_information`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_customer_list`
--
ALTER TABLE `tb_customer_list`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3667;

--
-- AUTO_INCREMENT for table `tb_customer_reservation_cancellation`
--
ALTER TABLE `tb_customer_reservation_cancellation`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_customer_reservation_list`
--
ALTER TABLE `tb_customer_reservation_list`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_department_list`
--
ALTER TABLE `tb_department_list`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_designation_list`
--
ALTER TABLE `tb_designation_list`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_employee_award_history`
--
ALTER TABLE `tb_employee_award_history`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_employee_education_info`
--
ALTER TABLE `tb_employee_education_info`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_employee_leave_application`
--
ALTER TABLE `tb_employee_leave_application`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_employee_leave_type_setting`
--
ALTER TABLE `tb_employee_leave_type_setting`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_employee_list`
--
ALTER TABLE `tb_employee_list`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_employee_nominee`
--
ALTER TABLE `tb_employee_nominee`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_employee_others_info`
--
ALTER TABLE `tb_employee_others_info`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_employee_shift_weekend`
--
ALTER TABLE `tb_employee_shift_weekend`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_employee_work_history`
--
ALTER TABLE `tb_employee_work_history`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_expense_category`
--
ALTER TABLE `tb_expense_category`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_expense_list`
--
ALTER TABLE `tb_expense_list`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_gender_list`
--
ALTER TABLE `tb_gender_list`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_holidays_observances_leave`
--
ALTER TABLE `tb_holidays_observances_leave`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_hotel_room_list`
--
ALTER TABLE `tb_hotel_room_list`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_notice_list`
--
ALTER TABLE `tb_notice_list`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_reservation_billing`
--
ALTER TABLE `tb_reservation_billing`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_reservation_billing_head`
--
ALTER TABLE `tb_reservation_billing_head`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_reservation_checkout_history`
--
ALTER TABLE `tb_reservation_checkout_history`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_reservation_payment`
--
ALTER TABLE `tb_reservation_payment`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_reservation_room`
--
ALTER TABLE `tb_reservation_room`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_room_category`
--
ALTER TABLE `tb_room_category`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_service_request_list`
--
ALTER TABLE `tb_service_request_list`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_weekend_holiday`
--
ALTER TABLE `tb_weekend_holiday`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tb_work_shift_list`
--
ALTER TABLE `tb_work_shift_list`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

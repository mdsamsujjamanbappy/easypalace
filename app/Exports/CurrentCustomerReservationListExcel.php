<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;

class CurrentCustomerReservationListExcel implements FromView, ShouldAutoSize
{
	use Exportable;
	
	public function __construct($data_list)
    {
        $this->data_list = $data_list;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        return view('customer_reservation.excel.current_reservation_list', [
            'data_list' => $this->data_list
        ]);
    }
}

<?php
function checkPermission($permissions){
    $userAccess = getMyPermission(auth()->user()->user_type);
    foreach ($permissions as $key => $value) {
        if($value == $userAccess){
            return true;
        }
    }
    return false;
}

function getMyPermission($id)
{
    switch ($id) {

            case 1:
            return 'super_admin';
            break;

            case 2:
            return 'admin';
            break;

            case 3:
            return 'executive';
            break;

            case 4:
            return 'customer';
            break;
    }
}


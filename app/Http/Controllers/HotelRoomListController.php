<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Session;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;

class HotelRoomListController extends Controller
{
    public function list()
    {   
        $list = DB::table('tb_hotel_room_list')
        ->leftjoin('tb_room_category','tb_room_category.id','=','tb_hotel_room_list.room_category_id')
        ->select('tb_hotel_room_list.*', 'tb_room_category.room_category_name')
        ->get();

        $room_category_list = DB::table('tb_room_category')->where('status', '=', 1)->get();
        return view('hotel_room.list', compact('list', 'room_category_list'));
    }


    public function store(Request $request)
    {
        $str = DB::table('tb_hotel_room_list')->insert([
            'room_category_id'		=>	$request->room_category_id,
            'room_name_number'		=>	$request->room_name_number,
            'room_rent_amount'		=>	$request->room_rent_amount,
            'room_facility_feature'	=>	$request->room_facility_feature,
            'remarks'				=>	$request->remarks,
            'status'				=>	$request->status,
            'created_at'			=>	Carbon::now()->toDateTimeString(),
            'updated_at'			=>	Carbon::now()->toDateTimeString()
        ]);

        Session::flash('successMessage','Room information has been successfully added.');
        return redirect()->back();
    }

    public function edit($id)
    {   
    	$id=base64_decode($id);
    	$info = DB::table('tb_hotel_room_list')->where('id', '=', $id)->first();
        return response()->json($info);
    }


    public function update(Request $request)
    {
        $str = DB::table('tb_hotel_room_list')->where('id', '=', $request->id)->update([
            'room_category_id'		=>	$request->room_category_id,
            'room_name_number'		=>	$request->room_name_number,
            'room_rent_amount'		=>	$request->room_rent_amount,
            'room_facility_feature'	=>	$request->room_facility_feature,
            'remarks'				=>	$request->remarks,
            'status'				=>	$request->status,
            'updated_at'			=>	Carbon::now()->toDateTimeString()
        ]);

        Session::flash('successMessage','Room information has been successfully updated.');
        return redirect()->back();
    }
}


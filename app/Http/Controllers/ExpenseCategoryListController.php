<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Session;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ExpenseCategoryListController extends Controller
{
    public function list()
    {   
        $expense_category_list = DB::table('tb_expense_category')->get();
        return view('expense_category.expense_category_list',compact('expense_category_list'));
    }

    public function store(Request $request)
    {
        $str = DB::table('tb_expense_category')->insert([
            'category_name'		=>	$request->category_name,
            'description'		=>	$request->description,
            'status'			=>	1,
      		'created_by'        => 	Auth::user()->id,
            'created_at'		=>	Carbon::now()->toDateTimeString(),
            'updated_at'		=>	Carbon::now()->toDateTimeString()
        ]);

        Session::flash('successMessage','Expense category has been successfully added.');
        return redirect()->back();
    }

    public function edit($id)
    {   
    	$id=base64_decode($id);
    	$expense_category_info = DB::table('tb_expense_category')->where('id', '=', $id)->first();
        return response()->json($expense_category_info);
    }

    public function update(Request $request)
    {
        $str = DB::table('tb_expense_category')->where('id', '=', $request->id)->update([
            'category_name'		=>	$request->category_name,
            'description'		=>	$request->description,
            'status'			=>	$request->status,
      		'created_by'        => 	Auth::user()->id,
            'updated_at'		=>	Carbon::now()->toDateTimeString()
        ]);

	    Session::flash('successMessage','Expense category has been successfully updated.');
	    return redirect()->back();
    }

    public function destroy($id)
    {   
    	$id=base64_decode($id);
        $str_count = DB::table('tb_expense_list')->where('category_id', '=', $id)->count();

        if($str_count>0){
        	Session::flash('failedMessage','Destroy request failed. There are already some data use this resource.');
        }else{
        	DB::table('tb_expense_category')->where('id', '=', $id)->delete();
        	Session::flash('successMessage','Expense category has been successfully destroyed.');
        }

        return redirect()->back();
    }
}

<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Session;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;

class RoomCategoryController extends Controller
{
    
    public function list()
    {   
        $list = DB::table('tb_room_category')->get();
        // dd($list);
        return view('room_category.list',compact('list'));
    }

    public function store(Request $request)
    {
        $str = DB::table('tb_room_category')->insert([
            'room_category_name'=>	$request->room_category_name,
            'remarks'			=>	$request->remarks,
            'status'			=>	1,
            'created_at'		=>	Carbon::now()->toDateTimeString(),
            'updated_at'		=>	Carbon::now()->toDateTimeString()
        ]);

        Session::flash('successMessage','Room category has been successfully added.');
        return redirect()->back();
    }

    public function edit($id)
    {   
    	$id=base64_decode($id);
    	$department_info = DB::table('tb_room_category')->where('id', '=', $id)->first();
        return response()->json($department_info);
    }

    public function update(Request $request)
    {
        $str = DB::table('tb_room_category')->where('id', '=', $request->id)->update([
            'room_category_name'=>	$request->room_category_name,
            'remarks'			=>	$request->remarks,
            'status'			=>	$request->status,
            'updated_at'		=>	Carbon::now()->toDateTimeString()
        ]);

	    Session::flash('successMessage','Room category has been successfully updated.');
	    return redirect()->back();
    }

    public function destroy($id)
    {   
    	$id=base64_decode($id);
        $emp_department_count = DB::table('tb_employee_list')->where('emp_department_id', '=', $id)->count();

        if($emp_department_count>0){
        	Session::flash('failedMessage','Destroy request failed. There are already some data use this resource.');
        }else{
        	$company_info = DB::table('tb_room_category')->where('id', '=', $id)->delete();
        	Session::flash('successMessage','Room category has been successfully destroyed.');
        }

        return redirect()->back();
    }
}

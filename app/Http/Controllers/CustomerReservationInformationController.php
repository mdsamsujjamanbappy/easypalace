<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Excel;
use Session;
use Validator;
use Datatables;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Exports\CurrentCustomerReservationListExcel;
use App\Exports\DueCustomerReservationListExcel;
use App\Exports\CancelledReservationListExcel;

class CustomerReservationInformationController extends Controller
{
    public function reservation_list()
    {   
        $reservation_list = DB::table('tb_customer_reservation_list')
        ->leftjoin('tb_customer_list','tb_customer_list.id','=','tb_customer_reservation_list.customer_id')
        ->select('tb_customer_reservation_list.*', 'tb_customer_list.customer_name', 'tb_customer_list.customer_phone')
        ->where('tb_customer_reservation_list.status', '=', 1)
        ->get();
        foreach($reservation_list as $rl){
            $reservation_room = DB::table('tb_reservation_room')
            ->leftjoin('tb_hotel_room_list','tb_hotel_room_list.id','=','tb_reservation_room.room_id')
            ->leftjoin('tb_room_category','tb_room_category.id','=','tb_hotel_room_list.room_category_id')
            ->select('tb_hotel_room_list.room_name_number', 'tb_room_category.room_category_name')
            ->where('reservation_id', '=', $rl->id)
            ->get();
            
            $rl->reservation_room_list = $reservation_room;
  
            $billing_history = DB::table('tb_reservation_billing')->where('reservation_id', '=', $rl->id)->sum('billing_amount');
            $payment_history = DB::table('tb_reservation_payment')->where('reservation_id', '=', $rl->id)->sum('payment_amount');
            
            $rl->billing_history = $billing_history;
            $rl->payment_history = $payment_history;
        }

        return view('customer_reservation.reservation_list', compact('reservation_list'));
    }

    public function reservation_list_excel()
    {   
        $reservation_list = DB::table('tb_customer_reservation_list')
        ->leftjoin('tb_customer_list','tb_customer_list.id','=','tb_customer_reservation_list.customer_id')
        ->select('tb_customer_reservation_list.*', 'tb_customer_list.customer_name', 'tb_customer_list.customer_phone')
        ->where('tb_customer_reservation_list.status', '=', 1)
        ->get();
        foreach($reservation_list as $rl){
            $reservation_room = DB::table('tb_reservation_room')
            ->leftjoin('tb_hotel_room_list','tb_hotel_room_list.id','=','tb_reservation_room.room_id')
            ->leftjoin('tb_room_category','tb_room_category.id','=','tb_hotel_room_list.room_category_id')
            ->select('tb_hotel_room_list.room_name_number', 'tb_room_category.room_category_name')
            ->where('reservation_id', '=', $rl->id)
            ->get();
            
            $rl->reservation_room_list = $reservation_room;
  
            $billing_history = DB::table('tb_reservation_billing')->where('reservation_id', '=', $rl->id)->sum('billing_amount');
            $payment_history = DB::table('tb_reservation_payment')->where('reservation_id', '=', $rl->id)->sum('payment_amount');
            
            $rl->billing_history = $billing_history;
            $rl->payment_history = $payment_history;
        }

        $reservation_list->downloaded_by = Auth::user()->name;
        return Excel::download(new CurrentCustomerReservationListExcel($reservation_list), date('ymd').' Current Reservation List.xlsx');
    }

    public function cancelled_reservation_list()
    {   
        $data_list = DB::table('tb_customer_reservation_list')
        ->leftjoin('tb_customer_list','tb_customer_list.id','=','tb_customer_reservation_list.customer_id')
        ->select('tb_customer_reservation_list.*', 'tb_customer_list.customer_name', 'tb_customer_list.customer_phone')
        ->where('tb_customer_reservation_list.status', '=', 3)
        ->get();

        foreach($data_list as $rl){
            $reservation_room = DB::table('tb_reservation_room')
            ->leftjoin('tb_hotel_room_list','tb_hotel_room_list.id','=','tb_reservation_room.room_id')
            ->leftjoin('tb_room_category','tb_room_category.id','=','tb_hotel_room_list.room_category_id')
            ->select('tb_hotel_room_list.room_name_number', 'tb_room_category.room_category_name')
            ->where('reservation_id', '=', $rl->id)
            ->get();
            
            $rl->reservation_room_list = $reservation_room;
        }

        return view('customer_reservation.cancelled_reservation_list', compact('data_list'));
    }

    public function cancelled_reservation_details($id)
    {   
        $id=base64_decode($id);
        $details_data = DB::table('tb_customer_reservation_list')
        ->leftjoin('tb_customer_reservation_cancellation','tb_customer_reservation_cancellation.reservation_id','=','tb_customer_reservation_list.id')
        ->leftjoin('tb_customer_list','tb_customer_list.id','=','tb_customer_reservation_list.customer_id')
        ->select('tb_customer_reservation_cancellation.reason_of_cancellation', 'tb_customer_reservation_list.*', 'tb_customer_list.customer_name', 'tb_customer_list.customer_phone')
        ->where('tb_customer_reservation_list.id', '=', $id)
        ->first();

        return response()->json($details_data);
    }

    public function cancelled_reservation_list_excel()
    {   
        $data_list = DB::table('tb_customer_reservation_list')
        ->leftjoin('tb_customer_reservation_cancellation', 'tb_customer_reservation_cancellation.reservation_id', '=', 'tb_customer_reservation_list.id')
        ->leftjoin('tb_customer_list', 'tb_customer_list.id', '=', 'tb_customer_reservation_list.customer_id')
        ->select('tb_customer_reservation_list.*', 'tb_customer_list.customer_name', 'tb_customer_list.customer_phone', 'tb_customer_reservation_cancellation.reason_of_cancellation')
        ->where('tb_customer_reservation_list.status', '=', 3)
        ->get();
        foreach($data_list as $rl){
            $reservation_room = DB::table('tb_reservation_room')
            ->leftjoin('tb_hotel_room_list','tb_hotel_room_list.id','=','tb_reservation_room.room_id')
            ->leftjoin('tb_room_category','tb_room_category.id','=','tb_hotel_room_list.room_category_id')
            ->select('tb_hotel_room_list.room_name_number', 'tb_room_category.room_category_name')
            ->where('reservation_id', '=', $rl->id)
            ->get();
            
            $rl->reservation_room_list = $reservation_room;
        }

        $data_list->downloaded_by = Auth::user()->name;
        return Excel::download(new CancelledReservationListExcel($data_list), date('ymd').' Cancelled Reservation List.xlsx');
    }

    public function due_payment_reservation_list()
    {   
        $reservation_list = DB::table('tb_reservation_checkout_history')
        ->leftjoin('tb_customer_reservation_list','tb_customer_reservation_list.id','=','tb_reservation_checkout_history.reservation_id')
        ->leftjoin('tb_customer_list','tb_customer_list.id','=','tb_customer_reservation_list.customer_id')
        ->select('tb_reservation_checkout_history.*', 'tb_customer_reservation_list.reservation_number', 'tb_customer_reservation_list.reservation_date', 'tb_customer_reservation_list.check_in_date', 'tb_customer_reservation_list.check_out_date', 'tb_customer_list.customer_name', 'tb_customer_list.customer_phone')
        ->orderBy('tb_reservation_checkout_history.id', 'desc')
        ->where([['tb_reservation_checkout_history.due_status', '=', 1], ['tb_reservation_checkout_history.current_due_amount', '!=', 0]])
        ->get();

        return view('customer_reservation.due_payment_reservation_list', compact('reservation_list'));
    }

    public function due_payment_reservation_list_excel()
    {   
        $reservation_list = DB::table('tb_reservation_checkout_history')
        ->leftjoin('tb_customer_reservation_list','tb_customer_reservation_list.id','=','tb_reservation_checkout_history.reservation_id')
        ->leftjoin('tb_customer_list','tb_customer_list.id','=','tb_customer_reservation_list.customer_id')
        ->select('tb_reservation_checkout_history.*', 'tb_customer_reservation_list.reservation_number', 'tb_customer_reservation_list.reservation_date', 'tb_customer_reservation_list.check_in_date', 'tb_customer_reservation_list.check_out_date', 'tb_customer_list.customer_name', 'tb_customer_list.customer_phone')
        ->orderBy('tb_reservation_checkout_history.id', 'desc')
        ->where([['tb_reservation_checkout_history.due_status', '=', 1], ['tb_reservation_checkout_history.current_due_amount', '!=', 0]])
        ->get();

        $reservation_list->downloaded_by = Auth::user()->name;
        return Excel::download(new DueCustomerReservationListExcel($reservation_list), date('ymd').' Due Customer List.xlsx');
    }

    public function reservation_details($id)
    {   
        $id=base64_decode($id);
        $reservation_details = DB::table('tb_customer_reservation_list')
        ->leftjoin('tb_customer_list','tb_customer_list.id','=','tb_customer_reservation_list.customer_id')
        ->select('tb_customer_reservation_list.*', 'tb_customer_list.customer_name', 'tb_customer_list.customer_phone')
        ->where('tb_customer_reservation_list.id', '=', $id)
        ->first();

        if($reservation_details->status==2){
            Session::flash('failedMessage','Checkout already has been successfully completed for selected reservation.');
            return redirect()->route('customer_reservation.reservation_list');
        }

        if(!empty($reservation_details)){
        $billing_amount = DB::table('tb_reservation_billing')->where('reservation_id', '=', $id)->sum('billing_amount');
        $payment_amount = DB::table('tb_reservation_payment')->where('reservation_id', '=', $id)->sum('payment_amount');

        $reservation_details->billing_amount = $billing_amount;
        $reservation_details->payment_amount = $payment_amount;

        $reservation_rooms = DB::table('tb_reservation_room')
        ->leftjoin('tb_hotel_room_list','tb_hotel_room_list.id','=','tb_reservation_room.room_id')
        ->leftjoin('tb_room_category','tb_room_category.id','=','tb_hotel_room_list.room_category_id')
        ->select('tb_reservation_room.id', 'tb_hotel_room_list.room_name_number', 'tb_room_category.room_category_name')
        ->where('reservation_id', '=', $reservation_details->id)
        ->get();
        
        $billing_history = DB::table('tb_reservation_billing')
        ->leftjoin('tb_reservation_billing_head','tb_reservation_billing_head.id','=','tb_reservation_billing.billing_head_id')
        ->leftjoin('users','users.id','=','tb_reservation_billing.created_by')
        ->select('tb_reservation_billing.*', 'tb_reservation_billing_head.billing_head_name', 'users.name as createdBy')
        ->where('tb_reservation_billing.reservation_id', '=', $reservation_details->id)
        ->get();
        
        $payment_history = DB::table('tb_reservation_payment')
        ->leftjoin('users','users.id','=','tb_reservation_payment.created_by')
        ->select('tb_reservation_payment.*', 'users.name as createdBy')
        ->where('tb_reservation_payment.reservation_id', '=', $reservation_details->id)
        ->get();
        
        $billing_head_list = DB::table('tb_reservation_billing_head')->where('status', 1)->get();
        
        $due_amount = DB::table('tb_reservation_checkout_history')
        ->leftjoin('tb_customer_reservation_list','tb_customer_reservation_list.id','=','tb_reservation_checkout_history.reservation_id')
        ->leftjoin('tb_customer_list','tb_customer_list.id','=','tb_customer_reservation_list.customer_id')
        ->select('tb_reservation_checkout_history.current_due_amount')
        ->where('tb_customer_reservation_list.customer_id', $reservation_details->customer_id)
        ->orderBy('tb_reservation_checkout_history.id', 'desc')
        ->first();
        
        if(!empty($due_amount)){
            $reservation_details->previous_due_amount   = $due_amount->current_due_amount;
        }else{
            $reservation_details->previous_due_amount   = 0;
        }
      
        return view('customer_reservation.reservation_details', compact('reservation_details', 'reservation_rooms', 'billing_history', 'payment_history', 'billing_head_list', 'due_amount'));
        }else{
            return "Invalid Reservation ID.";
        }
    }


    public function new_reservation()
    {   
        $customer_list = DB::table('tb_customer_list')
        ->select('tb_customer_list.id', 'tb_customer_list.customer_name', 'tb_customer_list.customer_phone')
        ->orderBy('tb_customer_list.customer_name', 'asc')
        ->where('tb_customer_list.customer_name', '!=', '')
        ->get();

        $room_category_list = DB::table('tb_room_category')->where('status', 1)->get();
        
        return view('customer_reservation.new_reservation', compact('customer_list', 'room_category_list'));
    }

    public function ajax_get_hotel_room($id){
    	$hotel_room_list = DB::table('tb_hotel_room_list')
	        ->orderBy('tb_hotel_room_list.room_name_number', 'asc')
	        ->where([['tb_hotel_room_list.room_category_id', '=', $id], ['tb_hotel_room_list.status', '=', 1]])
            ->get();
            
        return view('customer_reservation.ajax_get_hotel_room',compact('hotel_room_list'));
    }

    public static function returnBetweenDates( $startDate, $endDate ){
        $startStamp = strtotime(  $startDate );
        $endStamp   = strtotime(  $endDate );

        if( $endStamp > $startStamp ){
            while( $endStamp >= $startStamp ){

                $dateArr[] = date( 'Y-m-d', $startStamp );

                $startStamp = strtotime( ' +1 day ', $startStamp );

            }
            return $dateArr;    
        }else{
            return $startDate;
        }

    }
    public function new_reservation_store(Request $request){

        $dates = $this->returnBetweenDates($request->check_in_date, $request->check_out_date);
        $data = DB::table('tb_customer_reservation_list')->select('reservation_number')->orderBy('reservation_number', 'desc')->first(); 
        if(!empty($data->reservation_number)){
            $reservation_number = substr($data->reservation_number, 7);
            $reservation_number++;
            $reservation_number = date('ymd')."-".sprintf("%03d", $reservation_number);
        }else{
            $reservation_number = date('ymd')."-001";
        }

        $reservation_str = DB::table('tb_customer_reservation_list')->insertGetId([
            'customer_id'         =>  $request->customer_id,
            'reservation_number'  =>  $reservation_number,
            'reservation_date'    =>  date('Y-m-d'),
            'check_in_date'       =>  $request->check_in_date,
            'check_out_date'      =>  $request->check_out_date,
            'adult_number'        =>  $request->adult_number,
            'children_number'     =>  $request->children_number,
            'remarks'             =>  $request->remarks,
            'status'              =>  1,
            'created_by'          =>  Auth::user()->id,
            'created_at'          =>  Carbon::now()->toDateTimeString(),
            'updated_at'          =>  Carbon::now()->toDateTimeString(),
        ]);

        $customer_str = DB::table('tb_reservation_room')->insertGetId([
            'reservation_id'      =>  $reservation_str,
            'room_id'             =>  $request->room_id,
            'created_at'          =>  Carbon::now()->toDateTimeString(),
            'updated_at'          =>  Carbon::now()->toDateTimeString(),
        ]);

        $hotel_room_list = DB::table('tb_hotel_room_list')->select('room_name_number', 'room_rent_amount')->where('id', $request->room_id)->first();

            if($request->check_in_date==$request->check_out_date){
                DB::table('tb_reservation_billing')->insert([
                    'reservation_id'        =>  $reservation_str,
                    'billing_head_id'       =>  1,
                    'reservation_room_id'   =>  $customer_str,
                    'billing_description'   =>  "Room No: ".$hotel_room_list->room_name_number,
                    'billing_amount'        =>  $hotel_room_list->room_rent_amount,
                    'bill_date'             =>  $dates,
                    'created_by'            =>  Auth::user()->id,
                    'created_at'            =>  Carbon::now()->toDateTimeString(),
                    'updated_at'            =>  Carbon::now()->toDateTimeString(),
                ]);
            }else{
            if(count($dates)>1){
                foreach ($dates as $value) {
                    DB::table('tb_reservation_billing')->insert([
                        'reservation_id'        =>  $reservation_str,
                        'billing_head_id'       =>  1,
                        'reservation_room_id'   =>  $customer_str,
                        'billing_description'   =>  "Room No: ".$hotel_room_list->room_name_number,
                        'billing_amount'        =>  $hotel_room_list->room_rent_amount,
                        'bill_date'             =>  $value,
                        'created_by'            =>  Auth::user()->id,
                        'created_at'            =>  Carbon::now()->toDateTimeString(),
                        'updated_at'            =>  Carbon::now()->toDateTimeString(),
                    ]);
                }
            }else{
                    DB::table('tb_reservation_billing')->insert([
                        'reservation_id'        =>  $reservation_str,
                        'billing_head_id'       =>  1,
                        'reservation_room_id'   =>  $customer_str,
                        'billing_description'   =>  "Room No: ".$hotel_room_list->room_name_number,
                        'billing_amount'        =>  $hotel_room_list->room_rent_amount,
                        'bill_date'             =>  $dates,
                        'created_by'            =>  Auth::user()->id,
                        'created_at'            =>  Carbon::now()->toDateTimeString(),
                        'updated_at'            =>  Carbon::now()->toDateTimeString(),
                ]);
            }
        }

      Session::flash('successMessage','Reservation information has been successfully added.');
      return redirect()->route('customer_reservation.reservation_details', base64_encode($reservation_str));
    }

    public function cancellation_store(Request $request){

        DB::table('tb_customer_reservation_list')->where('id', $request->reservation_id)->update([
            'status'        =>  3,
            'updated_at'    =>  Carbon::now()->toDateTimeString(),
        ]);

        $reservation_str = DB::table('tb_customer_reservation_cancellation')->insert([
            'reservation_id'        =>  $request->reservation_id,
            'reason_of_cancellation'=>  $request->reason_of_cancellation,
            'created_by'            =>  Auth::user()->id,
            'created_at'            =>  Carbon::now()->toDateTimeString(),
            'updated_at'            =>  Carbon::now()->toDateTimeString(),
        ]);

      Session::flash('successMessage','Reservation billing has been successfully added.');
      return redirect()->route('customer_reservation.cancellation_reservation_list');
    }
    
    public function billing_information_store(Request $request){
        $reservation_str = DB::table('tb_reservation_billing')->insert([
            'reservation_id'        =>  $request->reservation_id,
            'billing_head_id'       =>  $request->billing_head_id,
            'reservation_room_id'   =>  NULL,
            'billing_description'   =>  $request->billing_description,
            'billing_amount'        =>  $request->billing_amount,
            'bill_date'             =>  $request->bill_date,
            'created_by'            =>  Auth::user()->id,
            'created_at'            =>  Carbon::now()->toDateTimeString(),
            'updated_at'            =>  Carbon::now()->toDateTimeString(),
        ]);

      Session::flash('successMessage','Reservation billing has been successfully added.');
      return redirect()->back();
    }

    public function payment_information_store(Request $request){
        $reservation_str = DB::table('tb_reservation_payment')->insert([
            'reservation_id'        =>  $request->reservation_id,
            'payment_description'   =>  $request->payment_description,
            'payment_amount'        =>  $request->payment_amount,
            'payment_date'          =>  $request->payment_date,
            'created_by'            =>  Auth::user()->id,
            'created_at'            =>  Carbon::now()->toDateTimeString(),
            'updated_at'            =>  Carbon::now()->toDateTimeString(),
        ]);

      Session::flash('successMessage','Reservation payment has been successfully added.');
      return redirect()->back();
    }

    public function billing_destroy($id){
        $id=base64_decode($id);
        DB::table('tb_reservation_billing')->where('id', $id)->delete();

        Session::flash('successMessage','Reservation billing information has been successfully destroyed.');
        return redirect()->back();
    }

    public function payment_destroy($id){
        $id=base64_decode($id);
        DB::table('tb_reservation_payment')->where('id', $id)->delete();

        Session::flash('successMessage','Reservation payment information has been successfully destroyed.');
        return redirect()->back();
    }

    public function reservation_checkout($id)
    {   
        $id=base64_decode($id);
        $reservation_details = DB::table('tb_customer_reservation_list')
        ->leftjoin('tb_customer_list','tb_customer_list.id','=','tb_customer_reservation_list.customer_id')
        ->select('tb_customer_reservation_list.*', 'tb_customer_list.customer_name', 'tb_customer_list.customer_phone')
        ->where('tb_customer_reservation_list.id', '=', $id)
        ->first();

        if($reservation_details->status==2){
            Session::flash('failedMessage','Checkout already has been successfully completed for selected reservation.');
            return redirect()->route('customer_reservation.reservation_list');
        }

        $reservation_rooms = DB::table('tb_reservation_room')
        ->leftjoin('tb_hotel_room_list','tb_hotel_room_list.id','=','tb_reservation_room.room_id')
        ->leftjoin('tb_room_category','tb_room_category.id','=','tb_hotel_room_list.room_category_id')
        ->select('tb_reservation_room.id', 'tb_hotel_room_list.room_name_number', 'tb_room_category.room_category_name')
        ->where('reservation_id', '=', $reservation_details->id)
        ->get();
        
        $billing_amount = DB::table('tb_reservation_billing')->where([['reservation_id', '=', $id], ['billing_head_id', '!=', 1]])->sum('billing_amount');
        $billing_amount_room = DB::table('tb_reservation_billing')->where([['reservation_id', '=', $id], ['billing_head_id', '=', 1]])->sum('billing_amount');
        $payment_amount = DB::table('tb_reservation_payment')->where('reservation_id', '=', $id)->sum('payment_amount');

        $due_amount = DB::table('tb_reservation_checkout_history')
        ->leftjoin('tb_customer_reservation_list','tb_customer_reservation_list.id','=','tb_reservation_checkout_history.reservation_id')
        ->leftjoin('tb_customer_list','tb_customer_list.id','=','tb_customer_reservation_list.customer_id')
        ->select('tb_reservation_checkout_history.id', 'tb_reservation_checkout_history.current_due_amount')
        ->where('tb_customer_reservation_list.customer_id', $reservation_details->customer_id)
        ->orderBy('tb_reservation_checkout_history.id', 'desc')
        ->first();

        $reservation_details->billing_amount        = $billing_amount;
        $reservation_details->billing_amount_room   = $billing_amount_room;
        $reservation_details->payment_amount        = $payment_amount;
        
        if(!empty($due_amount)){
            $reservation_details->previous_due_amount   = $due_amount->current_due_amount;
            $reservation_details->previous_reservation_checkout_id   = $due_amount->id;
        }else{
            $reservation_details->previous_due_amount   = 0;
            $reservation_details->previous_reservation_checkout_id   = NULL;
        }
      
      
        return view('customer_reservation.reservation_checkout', compact('reservation_details', 'reservation_rooms'));
    }

    public function reservation_checkout_store(Request $request)
    {
        DB::table('tb_reservation_checkout_history')->insert([
            'reservation_id'            =>  $request->reservation_id,
            'total_bill_amount'         =>  $request->bill_amount,
            'advance_payment_amount'    =>  $request->advance_payment_amount,
            'paid_payment'              =>  $request->paid_amount,
            'previous_due_amount'       =>  $request->previous_due_amount,
            'current_due_amount'        =>  $request->current_due_amount,
            'discount_percentage_amount'=>  $request->discount_percentage,
            'total_discount_amount'     =>  $request->discount_amount,
            'due_status'                =>  1,
            'created_by'                =>  Auth::user()->id,
            'created_at'                =>  Carbon::now()->toDateTimeString(),
            'updated_at'                =>  Carbon::now()->toDateTimeString(),
        ]);

        DB::table('tb_customer_reservation_list')->where('id', $request->reservation_id)->update([
            'status'        =>  2,
            'updated_at'    =>  Carbon::now()->toDateTimeString(),
        ]);

        if(!empty($request->previous_reservation_checkout_id)){
            DB::table('tb_reservation_checkout_history')->where('id', $request->previous_reservation_checkout_id)->update([
                'due_status'    =>  0,
                'updated_at'    =>  Carbon::now()->toDateTimeString(),
            ]);
        }

        Session::flash('successMessage','Reservation checkout has been successfully completed.');
        return redirect()->route('customer_reservation.reservation_list');
    }

    public function customer_panel_reservation_list()
    {   
        $reservation_list = DB::table('tb_customer_reservation_list')
        ->leftjoin('tb_customer_list','tb_customer_list.id','=','tb_customer_reservation_list.customer_id')
        ->select('tb_customer_reservation_list.*', 'tb_customer_list.customer_name', 'tb_customer_list.customer_phone')
        ->where([['tb_customer_reservation_list.customer_id', Auth::user()->ref_id]])
        ->get();
        foreach($reservation_list as $rl){
            $reservation_room = DB::table('tb_reservation_room')
            ->leftjoin('tb_hotel_room_list','tb_hotel_room_list.id','=','tb_reservation_room.room_id')
            ->leftjoin('tb_room_category','tb_room_category.id','=','tb_hotel_room_list.room_category_id')
            ->select('tb_hotel_room_list.room_name_number', 'tb_room_category.room_category_name')
            ->where('reservation_id', '=', $rl->id)
            ->get();
            
            $rl->reservation_room_list = $reservation_room;
  
            $billing_history = DB::table('tb_reservation_billing')->where('reservation_id', '=', $rl->id)->sum('billing_amount');
            $payment_history = DB::table('tb_reservation_payment')->where('reservation_id', '=', $rl->id)->sum('payment_amount');
            
            $rl->billing_history = $billing_history;
            $rl->payment_history = $payment_history;
        }

        return view('customer_portal.reservation_list', compact('reservation_list'));
    }


}

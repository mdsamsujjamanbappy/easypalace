<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Session;
use Validator;
use Datatables;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CustomerListController extends Controller
{
    public function customer_list(){

        $customer_list = DB::table('tb_customer_list')
        ->select('tb_customer_list.*')
        ->orderBy('tb_customer_list.created_at', 'desc')
        ->get();
        
      if(request()->ajax()){
          return Datatables::of($customer_list)
             ->addColumn('action', function($row){
             $btn = '<a target="_BLANK" href="'.route('customer.details',base64_encode($row->id)).'" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>';
             $btn .= ' <a target="_BLANK" href="'.route('customer.edit',base64_encode($row->id)).'" class="btn btn-success btn-sm"><i class="fa fa-edit"></i></a>';
             $btn .= ' <a target="_BLANK" href="'.route('customer.destroy',base64_encode($row->id)).'" class="btn btn-danger btn-sm"><i class="fas fa-trash-alt"></i></a>';
              return $btn;
          })
          ->addIndexColumn()
      	->rawColumns(['action'])->make(true);
      };

       return view('customer_list.list');
   } 
  
  public function details($id){
    $id=base64_decode($id);

    $customer_details = DB::table('tb_customer_list')
        ->select('tb_customer_list.*')
        ->orderBy('tb_customer_list.created_at', 'desc')
        ->where('tb_customer_list.id', '=', $id)
        ->first();
        // dd($customer_details);
       return view('customer_list.details', compact('customer_details'));
  }


  public function create(){
    return view('customer_list.create');
  }


  public function store(Request $request)
  {
    // dd($request->all());

    $customer_id = DB::table('tb_customer_list')->orderBy('tb_customer_list.id', 'desc')->first('customer_id');
    $customer_id = $customer_id->customer_id;
    $customer_id++;

      $customer_photo='';
      if ($request->hasFile('customer_photo')) {
          $customer_photo = 'customer_photo_'.time().'.'.$request->customer_photo->getClientOriginalExtension();
          $request->customer_photo->move('customer_images', $customer_photo);
      }

      $nid_frontside='';
      if ($request->hasFile('nid_frontside')) {
          $nid_frontside = 'nid_frontside_'.time().'.'.$request->nid_frontside->getClientOriginalExtension();
          $request->nid_frontside->move('customer_images', $nid_frontside);
      }

      $nid_backside='';
      if ($request->hasFile('nid_backside')) {
          $nid_backside = 'nid_backside_'.time().'.'.$request->nid_backside->getClientOriginalExtension();
          $request->nid_backside->move('customer_images', $nid_backside);
      }

    $customer_str = DB::table('tb_customer_list')->insertGetId([
      'customer_id'         =>  $customer_id,
      'customer_name'       =>  $request->customer_name,
      'p_id'                =>  $request->p_id,
      'father_name'         =>  $request->father_name,
      'mother_name'         =>  $request->mother_name,
      'customer_phone'      =>  $request->customer_phone,
      'customer_email'      =>  $request->customer_email,
      'date_of_birth'       =>  date('d-m-Y', strtotime($request->date_of_birth)),
      'nid_passport'        =>  $request->nid_passport,
      'customer_address1'   =>  $request->customer_address1,
      'customer_address2'   =>  $request->customer_address2,
      'company_name'        =>  $request->company_name,
      'emergency_contact'   =>  $request->emergency_contact,
      'status'              =>  $request->status,
      'remarks'             =>  $request->remarks,
      'customer_photo'      => $customer_photo,
      'nid_frontside'       => $nid_frontside,
      'nid_backside'        => $nid_backside,
      'created_by'          => Auth::user()->id,
      'updated_at'          => Carbon::now()->toDateTimeString()
    ]);

    if(!empty($request->password)){
      $str = DB::table('users')->insert([
        'ref_id'         => $customer_str,
        'user_type'      => 4,
        'company_id'     => 1,
        'name'           => $request->customer_name,
        'email'          => $request->customer_email,
        'username'       => $request->customer_phone,
        'password'       => bcrypt($request->password),
        'status'         => 1,
        'created_at'     => Carbon::now()->toDateTimeString(),
        'updated_at'     => Carbon::now()->toDateTimeString()
      ]);
    }

    Session::flash('successMessage','Customer information has been successfully added.');
    return redirect()->back();
  }


  public function update(Request $request)
  {
    $customer_str = DB::table('tb_customer_list')->where('id', $request->id)->update([
      'customer_name'       => $request->customer_name,
      'p_id'                => $request->p_id,
      'father_name'         =>  $request->father_name,
      'mother_name'         =>  $request->mother_name,
      'customer_phone'      =>  $request->customer_phone,
      'customer_email'      =>  $request->customer_email,
      'date_of_birth'       =>  date('d-m-Y', strtotime($request->date_of_birth)),
      'nid_passport'        =>  $request->nid_passport,
      'customer_address1'   =>  $request->customer_address1,
      'customer_address2'   =>  $request->customer_address2,
      'company_name'        =>  $request->company_name,
      'emergency_contact'   =>  $request->emergency_contact,
      'status'              =>  $request->status,
      'remarks'             =>  $request->remarks,
      'created_by'          => Auth::user()->id,
      'updated_at'          => Carbon::now()->toDateTimeString()
    ]);

    Session::flash('successMessage','Customer information has been successfully updated.');
    return redirect()->back();
  }

  //Customer photo update
  public function customer_profile_photo(Request $request){
      $id = base64_decode($request->id);

      $customer_photo='';
      if ($request->hasFile('customer_photo')) {
          $file=DB::table('tb_customer_list')->where('id', $id)->select('customer_photo')->first();
          if(!empty($file->customer_photo)){
            if(($file->customer_photo != "default.png")){
              $file_path="customer_images"."/".$file->customer_photo;
              if(file_exists($file_path)){
                unlink($file_path);
              }
            }
          }
          
          $customer_photo = 'customer_photo_'.time().'.'.$request->customer_photo->getClientOriginalExtension();
          $request->customer_photo->move('customer_images', $customer_photo);

          $photo=DB::table('tb_customer_list')->where('id', $id)->update([
              'customer_photo'  => $customer_photo
          ]);
      }

      $nid_frontside='';
      if ($request->hasFile('nid_frontside')) {
          $file=DB::table('tb_customer_list')->where('id', $id)->select('nid_frontside')->first();
          if(!empty($file->nid_frontside)){
            if(($file->nid_frontside != "default.png")){
              $file_path="customer_images"."/".$file->nid_frontside;
              if(file_exists($file_path)){
                unlink($file_path);
              }
            }
          }
          
          $nid_frontside = 'nid_frontside_'.time().'.'.$request->nid_frontside->getClientOriginalExtension();
          $request->nid_frontside->move('customer_images', $nid_frontside);

          $photo=DB::table('tb_customer_list')->where('id', $id)->update([
              'nid_frontside'  => $nid_frontside
          ]);
      }

      $nid_backside='';
      if ($request->hasFile('nid_backside')) {
          $file=DB::table('tb_customer_list')->where('id', $id)->select('nid_backside')->first();
          if(!empty($file->nid_backside)){
            if(($file->nid_backside != "default.png")){
              $file_path="customer_images"."/".$file->nid_backside;
              if(file_exists($file_path)){
                unlink($file_path);
              }
            }
          }
          
          $nid_backside = 'nid_backside_'.time().'.'.$request->nid_backside->getClientOriginalExtension();
          $request->nid_backside->move('customer_images', $nid_backside);

          $photo=DB::table('tb_customer_list')->where('id', $id)->update([
              'nid_backside'  => $nid_backside
          ]);
      }

      Session::flash('successMessage','Photos has been successfully updated');
      return redirect()->back();
  }

}

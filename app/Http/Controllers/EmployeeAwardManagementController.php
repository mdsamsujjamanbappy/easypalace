<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Session;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;

class EmployeeAwardManagementController extends Controller
{  
	public function employee_award_view()
    {   
        return view('employee_award.employee_award_view');
    }

	public function employee_award_data(Request $request)
    {   
    	$award_list = DB::table('tb_employee_award_history')
        ->leftJoin('users','tb_employee_award_history.created_by','=','users.id')
        ->leftJoin('tb_employee_list','tb_employee_award_history.employee_id','=','tb_employee_list.id')
        ->leftjoin('tb_department_list','tb_employee_list.emp_department_id','=','tb_department_list.id')
        ->leftjoin('tb_designation_list','tb_employee_list.emp_designation_id','=','tb_designation_list.id')
        ->select('tb_employee_award_history.*', 'users.name', 'tb_employee_list.emp_first_name', 'tb_employee_list.emp_last_name', 'tb_employee_list.employee_id', 'tb_department_list.department_name', 'tb_designation_list.designation_name' )
        ->whereBetween('tb_employee_award_history.award_period', [$request->start_date."-01", $request->end_date."-30"])
        ->get();
        // dd($award_list);
        return view('employee_award.employee_award_data', compact('award_list'));
    }


  	public function new_award(){
        $employee_list = DB::table('tb_employee_list')
        ->select('tb_employee_list.id', 'tb_employee_list.emp_first_name', 'tb_employee_list.emp_last_name', 'tb_employee_list.employee_id')
        ->orderBy('tb_employee_list.employee_id', 'asc')
        ->get();
        
       return view('employee_award.new_award', compact('employee_list'));
   	} 

	public function store(Request $request)
    {   
	     $attachment='';
	     if ($request->hasFile('attachment')) {
	         $attachment = time().'.'.$request->attachment->getClientOriginalExtension();
	         $request->attachment->move('award_attachment', $attachment);
	     }

        $str = DB::table('tb_employee_award_history')->insert([
            'employee_id'	=>	$request->employee_id,
            'award_name'	=>	$request->award_name,
            'award_gift'	=>	$request->award_gift,
            'cash_price'	=>	$request->cash_price,
            'award_period'	=>	$request->award_period."-01",
            'award_attachment'	=>	$attachment,
            'remarks'		=>	$request->remarks,
            'status'		=>	1,
      		'created_by'    => 	Auth::user()->id,
            'created_at'	=>	Carbon::now()->toDateTimeString(),
            'updated_at'	=>	Carbon::now()->toDateTimeString()
        ]);

    	Session::flash('successMessage','New award information has been successfully saved.');
        return redirect()->back();
    }

    public function destroy($id)
    {   
    	$id=base64_decode($id);
    	DB::table('tb_employee_award_history')->where('id', '=', $id)->delete();
    	Session::flash('messageTitle','Confirmation');
    	Session::flash('messageSuccess','Employee award information has been successfully destroyed. Refresh previous page..');
    	return redirect()->route('notification.message');
    }
  
}

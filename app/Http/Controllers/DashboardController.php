<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Excel;
use Session;
use Validator;
use Datatables;
use Carbon\Carbon;
use App\Mail\WelcomeMail;
use App\Jobs\SendEmailTest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class DashboardController extends Controller
{
    public function Dashboard(){
       if(checkPermission(['super_admin']) || checkPermission(['admin']) || checkPermission(['executive'])){

            $customer_count = DB::table('tb_customer_list')
            ->count();

            $employee_count = DB::table('tb_employee_list')
            ->where('tb_employee_list.emp_account_status', '=', 1)
            ->count();

            $current_reservation_count = DB::table('tb_customer_reservation_list')
            ->where('tb_customer_reservation_list.status', '=', 1)
            ->count();

            $due_customer_count = DB::table('tb_reservation_checkout_history')
            ->where([['tb_reservation_checkout_history.due_status', '=', 1], ['tb_reservation_checkout_history.current_due_amount', '!=', 0]])
            ->count();

            $checkout_reservation_count = DB::table('tb_customer_reservation_list')
            ->where('tb_customer_reservation_list.status', '=', 2)
            ->count();

            $cancelled_reservation_count = DB::table('tb_customer_reservation_list')
            ->where('tb_customer_reservation_list.status', '=', 3)
            ->count();

            $room_count = DB::table('tb_hotel_room_list')
            ->where('tb_hotel_room_list.status', '=', 1)
            ->count();

            $today = date('Y-m-d');
            $todays_expense = DB::table('tb_expense_list')
            ->whereDate('tb_expense_list.expense_date', '=', $today)
            ->sum('amount');

            return view('dashboard.dashboard', compact('customer_count', 'employee_count', 'current_reservation_count', 'due_customer_count', 'checkout_reservation_count', 'cancelled_reservation_count', 'room_count', 'todays_expense'));
        }else{
            return view('dashboard.customer_dashboard');

        }
    }

    public function mail(){
        $details['email'] = 'system@dongyi-bd.com';

        dispatch(new SendEmailTest($details));

        dd('done');
    }

    public function message(){
        return view('message');
        
    }
}

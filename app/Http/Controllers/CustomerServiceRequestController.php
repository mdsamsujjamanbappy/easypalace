<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Session;
use Validator;
use Datatables;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CustomerServiceRequestController extends Controller
{
    public function pending_list(){
        $pending_list = DB::table('tb_service_request_list')
        ->leftjoin('tb_customer_reservation_list','tb_customer_reservation_list.id','=','tb_service_request_list.reservation_id')
        ->leftjoin('tb_customer_list','tb_customer_list.id','=','tb_customer_reservation_list.customer_id')
        ->select('tb_service_request_list.*', 'tb_customer_reservation_list.reservation_number', 'tb_customer_list.customer_phone')
        ->orderBy('tb_service_request_list.created_at', 'desc')
        ->where('tb_service_request_list.status', 0)
        ->get();

       return view('customer_service_request.pending_list', compact('pending_list'));
   	} 

    public function onprocessing_list(){
        $onprocessing_list = DB::table('tb_service_request_list')
        ->leftjoin('tb_customer_reservation_list','tb_customer_reservation_list.id','=','tb_service_request_list.reservation_id')
        ->leftjoin('tb_customer_list','tb_customer_list.id','=','tb_customer_reservation_list.customer_id')
        ->leftjoin('users','users.id','=','tb_service_request_list.accepted_by')
        ->select('tb_service_request_list.*', 'tb_customer_reservation_list.reservation_number', 'tb_customer_list.customer_phone', 'users.name as acceptedBy')
        ->orderBy('tb_service_request_list.created_at', 'desc')
        ->where('tb_service_request_list.status', 1)
        ->get();

       return view('customer_service_request.onprocessing_list', compact('onprocessing_list'));
   } 
  
    public function completed_list(){
        $completed_list = DB::table('tb_service_request_list')
        ->leftjoin('tb_customer_reservation_list','tb_customer_reservation_list.id','=','tb_service_request_list.reservation_id')
        ->leftjoin('tb_customer_list','tb_customer_list.id','=','tb_customer_reservation_list.customer_id')
        ->leftjoin('users','users.id','=','tb_service_request_list.accepted_by')
        ->select('tb_service_request_list.*', 'tb_customer_reservation_list.reservation_number', 'tb_customer_list.customer_phone', 'users.name as updatedBy')
        ->orderBy('tb_service_request_list.created_at', 'desc')
        ->where('tb_service_request_list.status', 2)
        ->get();

       return view('customer_service_request.completed_list', compact('completed_list'));
   	} 

    public function rejected_list(){
        $rejected_list = DB::table('tb_service_request_list')
        ->leftjoin('tb_customer_reservation_list','tb_customer_reservation_list.id','=','tb_service_request_list.reservation_id')
        ->leftjoin('tb_customer_list','tb_customer_list.id','=','tb_customer_reservation_list.customer_id')
        ->leftjoin('users','users.id','=','tb_service_request_list.accepted_by')
        ->select('tb_service_request_list.*', 'tb_customer_reservation_list.reservation_number', 'tb_customer_list.customer_phone', 'users.name as updatedBy')
        ->orderBy('tb_service_request_list.created_at', 'desc')
        ->where('tb_service_request_list.status', 3)
        ->get();

       return view('customer_service_request.rejected_list', compact('rejected_list'));
   	} 
  
    public function accept_request(Request $request){
        $str = DB::table('tb_service_request_list')->where('id', '=', base64_decode($request->id))->update([
            'request_feedback'	=>	$request->request_feedback,
            'status'			=>	$request->status,
      		'accepted_by'       => 	Auth::user()->id,
            'updated_at'		=>	Carbon::now()->toDateTimeString()
        ]);
       return redirect()->back();
   	} 
  
    public function reject_request(Request $request){
        $str = DB::table('tb_service_request_list')->where('id', '=', base64_decode($request->id))->update([
            'request_feedback'	=>	$request->request_feedback,
            'status'			=>	3,
      		'accepted_by'       => 	Auth::user()->id,
            'updated_at'		=>	Carbon::now()->toDateTimeString()
        ]);
       return redirect()->back();
   	} 
  
    public function customer_service_request_list(){
        $data_list = DB::table('tb_service_request_list')
        ->leftjoin('tb_customer_reservation_list','tb_customer_reservation_list.id','=','tb_service_request_list.reservation_id')
        ->leftjoin('tb_customer_list','tb_customer_list.id','=','tb_customer_reservation_list.customer_id')
        ->leftjoin('users','users.id','=','tb_service_request_list.accepted_by')
        ->select('tb_service_request_list.*', 'tb_customer_reservation_list.reservation_number', 'tb_customer_list.customer_phone', 'users.name as updatedBy')
        ->orderBy('tb_service_request_list.created_at', 'desc')
        ->where('tb_customer_reservation_list.customer_id', Auth::user()->ref_id)
        ->get();

       return view('customer_portal.customer_service_request_list', compact('data_list'));
   	} 
  
    public function new_service_request(){

        $reservations = DB::table('tb_customer_reservation_list')
        ->leftjoin('tb_customer_list','tb_customer_list.id','=','tb_customer_reservation_list.customer_id')
        ->select('tb_customer_reservation_list.*', 'tb_customer_list.customer_name', 'tb_customer_list.customer_phone')
        ->where([['tb_customer_reservation_list.customer_id', Auth::user()->ref_id], ['tb_customer_reservation_list.status', '=', 1]])
        ->get();

       return view('customer_portal.new_service_request', compact('reservations'));
   	} 
  
    public function new_service_request_store(Request $request){
        $str = DB::table('tb_service_request_list')->insert([
            'reservation_id'	=>	$request->reservation_id,
            'service_title'		=>	$request->service_title,
            'request_details'	=>	$request->request_details,
            'request_date'		=>	date('y-m-d'),
            'status'			=>	0,
            'created_at'		=>	Carbon::now()->toDateTimeString(),
            'updated_at'		=>	Carbon::now()->toDateTimeString()
        ]);

       Session::flash('successMessage','Service request has been successfully submitted.');
       return redirect()->back();
   	} 
  
}

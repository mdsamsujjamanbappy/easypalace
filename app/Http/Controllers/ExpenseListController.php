<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Session;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ExpenseListController extends Controller
{
	public function expense_list_view()
    {   
        $expense_category = DB::table('tb_expense_category')->select('id','category_name')->get();
      
        return view('expense_list.expense_list_view', compact('expense_category'));
    }

	public function expense_list_data(Request $request)
    {   
    	if($request->expense_category_id=='all'){
	        $expense_list = DB::table('tb_expense_list')
	        ->leftJoin('users','tb_expense_list.created_by','=','users.id')
	        ->leftJoin('tb_expense_category','tb_expense_list.category_id','=','tb_expense_category.id')
	        ->select('tb_expense_list.*', 'users.name', 'tb_expense_category.category_name')
            ->whereBetween('tb_expense_list.expense_date', [$request->start_date, $request->end_date])
	        ->get();
	    }else{
	    	$expense_list = DB::table('tb_expense_list')
	        ->leftJoin('users','tb_expense_list.created_by','=','users.id')
	        ->leftJoin('tb_expense_category','tb_expense_list.category_id','=','tb_expense_category.id')
	        ->select('tb_expense_list.*', 'users.name', 'tb_expense_category.category_name')
            ->whereBetween('tb_expense_list.expense_date', [$request->start_date, $request->end_date])
            ->where('tb_expense_list.category_id', $request->expense_category_id)
	        ->get();
	    }

        return view('expense_list.expense_list_data', compact('expense_list'));
    }

	public function new_expense()
    {   
        $expense_category = DB::table('tb_expense_category')->select('id','category_name')->get();
        return view('expense_list.new_expense', compact('expense_category'));
    }

	public function store(Request $request)
    {   
	     $attachment='';
	     if ($request->hasFile('attachment')) {
	         $attachment = time().'.'.$request->attachment->getClientOriginalExtension();
	         $request->attachment->move('expense_attachment', $attachment);
	     }

        $str = DB::table('tb_expense_list')->insert([
            'category_id'	=>	$request->expense_category_id,
            'expense_date'	=>	$request->expense_date,
            'amount'		=>	$request->amount,
            'attachment'	=>	$attachment,
            'remarks'		=>	$request->remarks,
            'status'		=>	1,
      		'approved_by'   => 	Auth::user()->id,
      		'created_by'    => 	Auth::user()->id,
            'created_at'	=>	Carbon::now()->toDateTimeString(),
            'updated_at'	=>	Carbon::now()->toDateTimeString()
        ]);

    	Session::flash('successMessage','New expense has been successfully saved.');
        return redirect()->back();
    }

    public function destroy($id)
    {   
    	$id=base64_decode($id);
    	DB::table('tb_expense_list')->where('id', '=', $id)->delete();
    	Session::flash('messageTitle','Confirmation');
    	Session::flash('messageSuccess','Expense category has been successfully destroyed. Refresh previous page..');
    	return redirect()->route('notification.message');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_employee_list', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('employee_id', 30)->nullable();
            $table->string('company_id', 30)->index()->nullable();
            $table->string('emp_first_name', 20)->nullable();
            $table->string('emp_last_name', 20)->nullable();
            $table->integer('emp_department_id')->index()->nullable();
            $table->integer('emp_designation_id')->index()->nullable();
            $table->integer('emp_gender_id')->nullable();
            $table->integer('emp_shift_id')->index()->nullable();
            $table->string('emp_email', 50)->nullable();
            $table->string('emp_phone', 20)->nullable();
            $table->text('emp_photo')->nullable();
            $table->date('emp_dob')->nullable();
            $table->date('emp_joining_date')->nullable();
            $table->integer('emp_probation_period')->nullable();
            $table->string('emp_religion', 30)->nullable();
            $table->string('emp_marital_status', 30)->nullable();
            $table->string('emp_bank_account', 30)->nullable();
            $table->text('emp_bank_info')->nullable();
            $table->string('emp_card_number', 30)->nullable();
            $table->string('emp_blood_group', 8)->nullable();
            $table->date('date_of_discontinuation')->nullable();
            $table->text('reason_of_discontinuation')->nullable();
            $table->string('emp_nid', 30)->nullable();
            $table->string('emp_nationality', 40)->nullable();
            $table->text('emp_parmanent_address')->nullable();
            $table->text('emp_current_address')->nullable();
            $table->string('emp_father_name', 50)->nullable();
            $table->string('emp_mother_name', 50)->nullable();
            $table->string('emp_ipbx_extension', 10)->nullable();
            $table->tinyInteger('emp_account_status')->nullable();
            $table->string('created_by', 8)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_employee_list');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReservationBillingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_reservation_billing', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('reservation_id', 8)->index()->nullable();
            $table->string('billing_head_id', 8)->index()->nullable();
            $table->string('reservation_room_id', 8)->index()->nullable();
            $table->text('billing_description')->nullable();
            $table->string('billing_amount', 12)->nullable();
            $table->date('bill_date')->nullable();
            $table->bigInteger('created_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_reservation_billing');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWeekendHolidayTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_weekend_holiday', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('day_name', 150)->nullable();
            $table->tinyInteger('status')->nullable();
            $table->string('company_id', 8)->index()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_weekend_holiday');
    }
}

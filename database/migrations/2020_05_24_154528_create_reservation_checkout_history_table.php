<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReservationCheckoutHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_reservation_checkout_history', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('reservation_id', 8)->index()->nullable();
            $table->string('total_bill_amount', 12)->nullable();
            $table->string('advance_payment_amount', 12)->nullable();
            $table->string('paid_payment', 12)->nullable();
            $table->string('previous_due_amount', 12)->nullable();
            $table->string('current_due_amount', 12)->nullable();
            $table->string('total_discount_amount', 12)->nullable();
            $table->tinyInteger('discount_percentage_amount')->nullable();
            $table->tinyInteger('due_status')->nullable();
            $table->bigInteger('created_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_reservation_checkout_history');
    }
}

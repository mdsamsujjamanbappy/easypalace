<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiceRequestListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_service_request_list', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('reservation_id', 8)->nullable();
            $table->string('service_title', 8)->nullable();
            $table->date('request_date')->nullable();
            $table->text('request_details')->nullable();
            $table->text('request_feedback')->nullable();
            $table->bigInteger('accepted_by')->nullable();
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_service_request_list');
    }
}

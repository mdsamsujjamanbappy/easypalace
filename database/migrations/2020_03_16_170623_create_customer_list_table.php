<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomerListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_customer_list', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('customer_id', 30)->nullable();
            $table->string('customer_name', 250)->nullable();
            $table->string('father_name', 250)->nullable();
            $table->string('mother_name', 250)->nullable();
            $table->string('date_of_birth', 50)->nullable();
            $table->text('customer_address1')->nullable();
            $table->text('customer_address2')->nullable();
            $table->string('customer_email', 200)->nullable();
            $table->string('customer_phone', 25)->nullable();
            $table->string('emergency_contact', 30)->nullable();
            $table->string('nid_passport', 200)->nullable();
            $table->string('status', 30)->nullable();
            $table->text('customer_photo')->nullable();
            $table->text('nid_frontside')->nullable();
            $table->text('nid_backside')->nullable();
            $table->string('p_id', 30)->nullable();
            $table->string('company_name', 200)->nullable();
            $table->text('remarks')->nullable();
            $table->string('created_by', 4)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_customer_list');
    }
}

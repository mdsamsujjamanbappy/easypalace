<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttendanceHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_attendance_history', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer("emp_id")->index()->nullable();
            $table->date("attendance_date")->nullable();
            $table->string("in_time", 15)->nullable();
            $table->string("out_time", 15)->nullable();
            $table->tinyinteger("attendance_type")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_attendance_history');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoomCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_room_category', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('room_category_name', 250)->nullable();
            $table->text('remarks')->nullable();
            $table->tinyInteger('status')->comment('"1" for active,  "0" for inactive')->default()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_room_category');
    }
}

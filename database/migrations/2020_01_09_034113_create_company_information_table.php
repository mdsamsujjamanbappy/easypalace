<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanyInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_company_information', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('company_logo', 255)->nullable();
            $table->string('company_name', 255)->nullable();
            $table->string('company_tagline', 255)->nullable();
            $table->string('company_phone', 30)->nullable();
            $table->string('company_email', 255)->nullable();
            $table->text('company_address1')->nullable();
            $table->text('company_address2')->nullable();
            $table->tinyInteger('status')->default('1')->comment('1 for Active, 0 for Inactive')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_company_information');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReservationPaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_reservation_payment', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('reservation_id', 8)->index()->nullable();
            $table->text('payment_description')->nullable();
            $table->string('payment_amount', 12)->nullable();
            $table->date('payment_date')->nullable();
            $table->bigInteger('created_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_reservation_payment');
    }
}

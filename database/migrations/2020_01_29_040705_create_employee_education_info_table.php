<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeEducationInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_employee_education_info', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('emp_id')->comment('emp_id will come from ("tb_employee_list.id") table')->nullable()->index();
            $table->string('emp_exam_title', 150)->nullable();
            $table->string('emp_institution_name', 150)->nullable();
            $table->string('emp_result', 50)->nullable();
            $table->string('emp_scale', 50)->nullable();
            $table->string('emp_passing_year', 50)->nullable();
            $table->text('emp_attachment')->nullable();
            $table->bigInteger('created_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_employee_education_info');
    }
}

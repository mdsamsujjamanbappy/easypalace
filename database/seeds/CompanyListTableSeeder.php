<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class CompanyListTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tb_company_information')->insert([
            'id'                => 1,
            'company_logo'      => "hrpalace.png",
            'company_name'      => "Hotel Royal Palace (PVT) Ltd.",
            'company_tagline'   => 'One Of The Leading Hotels In Dhaka',
            'company_phone'     => '+880-1747906414',
            'company_email'     => 'info@hrpalace.com',
            'company_address1'  => '31/D Topkhana Road, Dhaka 1000, Bangladesh',
            'status'            => 1,
            'created_at'        => Carbon::now()->toDateTimeString(),
            'updated_at'        => Carbon::now()->toDateTimeString()
        ]);
    }
}

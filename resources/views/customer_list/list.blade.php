@extends('layout.master')
@section('title','Customer List')
@section('extra_css')
@endsection
@section('content')

    <div class="br-pagetitle my-pagetitle">
        <div>
            <h4> <i class="icon icon ion-ios-bookmarks-outline"></i> Customer List</h4>
        </div>

        <div class="pagetitle-btn">
            <a href="{{route('employee.new.create')}}" target="_BLANK" class="btn  btn-info btn-sm custom-btn-1 btn-1 tx-11 tx-uppercase pd-y-8 pd-x-18 tx-mont tx-medium"> <i class="fas fa-plus-circle"></i> Add New</a>
        </div>
    </div>
    <div class="br-pagebody br-pagebody-1">
        <div class="br-section-wrapper">
            <div class="table-wrapper scrollme">
            
                <table id="customer_table" class="table table-striped table-bordered  display responsive" >
                    <thead>
                        <tr>
                            <th>Serial</th>
                            <th>Customer Name</th>
                            <th>Phone Number</th>
                            <th>NID/Passport</th>
                            <th width="20%">Customer Address</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>

        </div>
    </div>
@endsection

@section('extra_js')

<script>
    $(function() {
        'use strict';

       $('#customer_table').DataTable({
       "pageLength": 50,
       processing: true,
       serverSide: true,
       ajax: '{{ route('customer.list') }}',
       columns: [
                { data: 'DT_RowIndex', name: 'DT_RowIndex' },
                { data: 'customer_name', name: 'customer_name' },
                { data: 'customer_phone', name: 'customer_phone' },
                { data: 'nid_passport', name: 'nid_passport' },
                { data: 'customer_address1', name: 'customer_address1' },
                {data: 'action', name: 'action', orderable: false, searchable: false}
             ]
        });
    });
</script>

@endsection




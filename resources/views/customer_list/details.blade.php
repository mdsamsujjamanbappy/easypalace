@extends('layout.master')
@section('title','Customer Profile')
@section('extra_css')
@endsection

@section('content')
    <!-- ########## START: MAIN PANEL ########## -->
    <div class="br-profile-page">
        <div class="card shadow-base bd-0 rounded-0 widget-4">
            <div class="card-header ht-75">
                <div class="hidden-xs-down">
                </div>
                <div class="tx-24 hidden-xs-down">

                </div>
            </div><!-- card-header -->
            <div class="card-body card_body_profile_1">
                <div class="card-profile-img">
                    <img src="{{asset('customer_images/'.$customer_details->customer_photo)}}" alt="">
                </div><!-- card-profile-img -->
                <h3 class="tx-normal tx-roboto tx-white"><b>{{$customer_details->customer_name}}</b></h5>
            </div><!-- card-body -->
        </div><!-- card -->
        <div class="tab-content br-profile-body">
            <div class="tab-pane fade active show" id="profile">
                <div class="row">
                    <div class="col-lg-12 m-auto">
                        <div class="card">
                            @if ($errors->any())
                                @foreach ($errors->all() as $error)
                                   <li class="msb-txt-red">{{ $error }}</li>
                               @endforeach
                             @endif
                            <div class="card-header card_header_2">
                                <div class="row">
                                    <div class="col-md-6">
                                        <p>Customer Information</p>
                                    </div>
                                    <div class="col-md-6 msb-txt-right">
                                        <a href="" class="btn btn-info btn-sm " data-toggle="modal" data-target="#photo_modal"> <i class="fas fa-edit"></i> Update Photo</a>
                                        <a href="" class="btn btn-info btn-sm " data-toggle="modal" data-target="#update_modal"> <i class="fas fa-edit"></i> Edit Information</a>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body card_body_2">
                                <div class="row">
                                    <div class="col-md-6">
                                        <p><span>Customer Name</span><span>: <b>{{$customer_details->customer_name}}</b></span></p>
                                    </div>
                                    <div class="col-md-6">
                                        <p><span>PID</span><span>: <b>{{$customer_details->p_id}}</b></span></p>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-6">
                                        <p><span>Father Name</span><span>: <b>{{$customer_details->father_name}}</b></span></p>
                                    </div>
                                    <div class="col-md-6">
                                        <p><span>Mother Name</span><span>: <b>{{$customer_details->mother_name}}</b></span></p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <p><span>Customer Phone</span><span>: <b>{{$customer_details->customer_phone}}</b></span></p>
                                    </div>
                                    <div class="col-md-6">
                                        <p><span>Customer Email</span><span>: <b>{{$customer_details->customer_email}}</b></span></p>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-6">
                                        <p><span>Date of Birth</span><span>: <b>{{$customer_details->date_of_birth}}</b></span></p>
                                    </div>
                                    <div class="col-md-6">
                                        <p><span>NID/Passport</span><span>: <b>{{$customer_details->nid_passport}}</b></span></p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <p><span>Customer Address1</span><span>: <b>{{$customer_details->customer_address1}}</b></span></p>
                                    </div>
                                    <div class="col-md-6">
                                        <p><span>Customer Address2</span><span>: <b>{{$customer_details->customer_address2}}</b></span></p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <p><span>Company Name</span><span>: <b>{{$customer_details->company_name}}</b></span></p>
                                    </div>
                                    <div class="col-md-6">
                                        <p><span>Emergency Contact</span><span>: <b>{{$customer_details->emergency_contact}}</b></span></p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <p><span>Status</span><span>: <b>{{$customer_details->status}}</b></span></p>
                                    </div>
                                    <div class="col-md-6">
                                        <p><span>Remarks</span><span>: <b>{{$customer_details->remarks}}</b></span></p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 msb-txt-center"><br></div>
                                    <div class="col-md-1 msb-txt-center"></div>
                                    <div class="col-md-4 msb-txt-center">
                                       <img src="{{asset('customer_images/'.$customer_details->nid_frontside)}}" width="250px;" >
                                       <br>
                                       Photo 1
                                       <br>
                                       <a href="{{asset('customer_images/'.$customer_details->nid_frontside)}}" target="NEW" class="btn btn-default btn-xs"><i class="fa fa-search"></i> View Full Image</a><br>
                                    </div>
                                    <div class="col-md-2 msb-txt-center"></div>
                                    <div class="col-md-4 msb-txt-center">
                                       <img src="{{asset('customer_images/'.$customer_details->nid_backside)}}" width="250px;" >
                                       <br>
                                       Photo 2
                                       <br>
                                       <a href="{{asset('customer_images/'.$customer_details->nid_backside)}}" target="NEW" class="btn btn-default btn-xs"><i class="fa fa-search"></i> View Full Image</a><br>
                                    </div>
                                    <div class="col-md-1 msb-txt-center"></div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div><!-- row -->
            </div><!-- tab-pane -->
            
        </div>
    </div>

    @include('customer_list.modal.photo_update_modal')
    @include('customer_list.modal.profile_update_modal')

@endsection

@section('extra_js')
@endsection
































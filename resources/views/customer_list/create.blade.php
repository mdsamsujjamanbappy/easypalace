\@extends('layout.master')
@section('title','Add New Customer')
@section('extra_css')
@endsection
@section('content')

    <div class="br-pagetitle my-pagetitle">
        <div>
            <h4> <i class="icon fa fa-user"></i> New Customer Information</h4>
        </div>

        <div class="pagetitle-btn">
            <a href="{{route('customer.list')}}" class="btn  btn-info btn-sm custom-btn-1 btn-1 tx-11 tx-uppercase pd-y-8 pd-x-18 tx-mont tx-medium"> <i class="fas fa-users"></i> Customer List</a>
        </div>
    </div>
    <div class="br-pagebody br-pagebody-1">
        <div class="br-section-wrapper">
            <div class="table-wrapper">
               {!! Form::open(['method'=>'POST','route'=>'customer.store', 'files' =>true]) !!}
                    <div class="row">
                
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-3 control-label form-label-1"> Customer Name <span style="color:red">*</span></label>
                                <div class="col-sm-8 pl-0">
                                    <input type="text" class="form-control" name="customer_name" autocomplete="off" required="" placeholder="Customer Name">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-3 control-label form-label-1">PID</label>
                                <div class="col-sm-8 pl-0">
                                    <input type="text" class="form-control" name="p_id" autocomplete="off" placeholder="PID">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-3 control-label form-label-1">Father Name </label>
                                <div class="col-sm-8 pl-0">
                                    <input type="text" class="form-control" name="father_name" autocomplete="off" placeholder="Father Name">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-3 control-label form-label-1">Mother Name </label>
                                <div class="col-sm-8 pl-0">
                                    <input type="text" class="form-control" name="mother_name" autocomplete="off" placeholder="Mother Name">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-3 control-label form-label-1">Phone <span class="msb-txt-red">*</span></label>
                                <div class="col-sm-8 pl-0">
                                    <input type="text" class="form-control" name="customer_phone" autocomplete="off" required="" placeholder="Customer Phone">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-3 control-label form-label-1">Email </label>
                                <div class="col-sm-8 pl-0">
                                    <input type="email" class="form-control" name="customer_email" autocomplete="off" placeholder="Customer Email">
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-3  control-label form-label-1">Date of Birth</label>
                                <div class="col-sm-8 pl-0">
                                    <input autocomplete="off"  type="text" name="date_of_birth" class="form-control fc-datepicker" placeholder="YYYY-MM-DD">

                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-3 control-label form-label-1">NID/Passport</label>
                                <div class="col-sm-8 pl-0">
                                    <input type="text" class="form-control" name="nid_passport" autocomplete="off" placeholder="NID/Passport">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-3 control-label form-label-1">Address1 <span class="msb-txt-red">*</span></label>
                                <div class="col-sm-8 pl-0">
                                    <textarea name="customer_address1" rows="3" class="form-control" placeholder="Address1" required=""></textarea>

                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-3 control-label form-label-1">Address2</label>
                                <div class="col-sm-8 pl-0">
                                    <textarea name="customer_address2"  rows="3" class="form-control" placeholder="Address2"></textarea>

                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-3 control-label form-label-1">Company Name</label>
                                <div class="col-sm-8 pl-0">
                                    <input type="text" class="form-control" name="company_name" autocomplete="off" placeholder="Company Name">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-3 control-label form-label-1">Emergency Contact</label>
                                <div class="col-sm-8 pl-0">
                                    <input type="text" class="form-control" name="emergency_contact" autocomplete="off" placeholder="Emergency Contact">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-3 control-label form-label-1">Status</label>
                                <div class="col-sm-8 pl-0">
                                    <input type="text" class="form-control" name="status" autocomplete="off" placeholder="Status">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-3 control-label form-label-1">Remarks</label>
                                <div class="col-sm-8 pl-0">
                                    <input type="text" class="form-control" name="remarks" autocomplete="off" placeholder="Remarks">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-3 control-label form-label-1">Customer Photo</label>
                                <div class="col-sm-8 pl-0">
                                    <input type="file" class="form-control" name="customer_photo" autocomplete="off" >
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-3 control-label form-label-1">Password (Login Access)</label>
                                <div class="col-sm-8 pl-0">
                                    <input type="text" maxlength="12" placeholder="Enter password to give software access" class="form-control" name="password" autocomplete="off" >
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-3 control-label form-label-1">Photo1</label>
                                <div class="col-sm-8 pl-0">
                                    <input type="file" class="form-control" name="nid_frontside" autocomplete="off" >
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-3 control-label form-label-1">Photo2</label>
                                <div class="col-sm-8 pl-0">
                                    <input type="file" class="form-control" name="nid_backside" autocomplete="off" >
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <hr>
                    <div class="form-group text-center col-sm-12">
                        <button class="btn btn-info btn-sm custom-btn-1 ml-2" type="submit"> Save Information</button>
                    </div>
                    {!! Form::close() !!}
                
            </div>
        </div>
    </div>
@endsection

@section('extra_js')
@endsection




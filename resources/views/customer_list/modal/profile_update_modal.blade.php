<div id="update_modal" class="modal fade effect-sign">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content bd-0 tx-14">
                <div class="modal-header pd-y-15 pd-x-25 modal_header_1">
                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"> <i class="fas fa-edit"></i> Edit Customer Information </h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fas fa-times"></i></span>
                    </button>
                </div>
                <div class="modal-body pd-20">
                    <span id="form_result"></span>
                    <div class="modal_body_inner">
                        {!! Form::open(['method'=>'POST','route'=>'customer.update']) !!}
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <label for="form-label" class="col-sm-3 control-label form-label-1"> Customer Name <span class="msb-txt-red">*</span></label>
                                    <div class="col-sm-8 pl-0">
                                        <input type="text" class="form-control" name="customer_name" autocomplete="off" value="{{$customer_details->customer_name}}" required="" placeholder="Customer Name">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <label for="form-label" class="col-sm-3 control-label form-label-1">PID</label>
                                    <div class="col-sm-8 pl-0">
                                        <input type="text" class="form-control" name="p_id" value="{{$customer_details->p_id}}" autocomplete="off" placeholder="PID">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <label for="form-label" class="col-sm-3 control-label form-label-1">Father Name </label>
                                    <div class="col-sm-8 pl-0">
                                        <input type="text" class="form-control" name="father_name" value="{{$customer_details->father_name}}" autocomplete="off" placeholder="Father Name">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <label for="form-label" class="col-sm-3 control-label form-label-1">Mother Name </label>
                                    <div class="col-sm-8 pl-0">
                                        <input type="text" class="form-control" name="mother_name" value="{{$customer_details->mother_name}}" autocomplete="off" placeholder="Mother Name">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <label for="form-label" class="col-sm-3 control-label form-label-1">Phone <span class="msb-txt-red">*</span></label>
                                    <div class="col-sm-8 pl-0">
                                        <input type="text" class="form-control" name="customer_phone" value="{{$customer_details->customer_phone}}"  autocomplete="off" required="" placeholder="Customer Phone">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <label for="form-label" class="col-sm-3 control-label form-label-1">Email </label>
                                    <div class="col-sm-8 pl-0">
                                        <input type="email" class="form-control" name="customer_email" value="{{$customer_details->customer_email}}"  autocomplete="off" placeholder="Customer Email">
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <label for="form-label" class="col-sm-3  control-label form-label-1">Date of Birth</label>
                                    <div class="col-sm-8 pl-0">
                                        <input autocomplete="off"  type="text" value="{{$customer_details->date_of_birth}}" name="date_of_birth" class="form-control datepicker" placeholder="YYYY-MM-DD">

                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <label for="form-label" class="col-sm-3 control-label form-label-1">NID/Passport</label>
                                    <div class="col-sm-8 pl-0">
                                        <input type="text" class="form-control" value="{{$customer_details->nid_passport}}" name="nid_passport" autocomplete="off" placeholder="NID/Passport">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <label for="form-label" class="col-sm-3  control-label form-label-1">Address1 <span class="msb-txt-red">*</span></label>
                                    <div class="col-sm-8 pl-0">
                                        <textarea name="customer_address1" rows="3" class="form-control" placeholder="Address1" required="">{{$customer_details->customer_address1}}</textarea>

                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <label for="form-label" class="col-sm-3  control-label form-label-1">Address2</label>
                                    <div class="col-sm-8 pl-0">
                                        <textarea name="customer_address2"  rows="3" class="form-control" placeholder="Address2">{{$customer_details->customer_address2}}</textarea>

                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <label for="form-label" class="col-sm-3 control-label form-label-1">Company Name</label>
                                    <div class="col-sm-8 pl-0">
                                        <input type="text" class="form-control" value="{{$customer_details->company_name}}" name="company_name" autocomplete="off" placeholder="Company Name">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <label for="form-label" class="col-sm-3 control-label form-label-1">Emergency Contact</label>
                                    <div class="col-sm-8 pl-0">
                                        <input type="text" class="form-control" value="{{$customer_details->emergency_contact}}" name="emergency_contact" autocomplete="off" placeholder="Emergency Contact">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <label for="form-label" class="col-sm-3 control-label form-label-1">Status</label>
                                    <div class="col-sm-8 pl-0">
                                        <input type="text" class="form-control" value="{{$customer_details->status}}" name="status" autocomplete="off" placeholder="Status">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <label for="form-label" class="col-sm-3 control-label form-label-1">Remarks</label>
                                    <div class="col-sm-8 pl-0">
                                        <input type="text" class="form-control" value="{{$customer_details->remarks}}" name="remarks" autocomplete="off" placeholder="Remarks">
                                    </div>
                                </div>
                            </div>
                            
                            </div>
                             <input type="hidden" name="id" value="{{($customer_details->id)}}">
                             <div class="form-group text-center col-sm-12">
                                <button class="btn btn-info btn-sm custom-btn-1 ml-2" type="submit">Update Information</button>
                                <button type="button" class="btn btn-danger btn-sm custom-btn-1 tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium" data-dismiss="modal">Close</button>
                            </div>
                            {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div><!-- modal-dialog -->
    </div><!-- modal -->

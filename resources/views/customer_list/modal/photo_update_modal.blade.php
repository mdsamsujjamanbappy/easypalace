<div id="photo_modal" class="modal fade effect-sign">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content bd-0 tx-14">
                <div class="modal-header pd-y-15 pd-x-25 modal_header_1">
                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"> <i class="fas fa-edit"></i> Update Profile Image </h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fas fa-times"></i></span>
                    </button>
                </div>
                <div class="modal-body pd-20">
                    <span id="form_result"></span>
                    <div class="modal_body_inner">

                        @if($customer_details->customer_photo)
                        <div id="photo_update_div" class="text-center">
                            <div class="row">
                                <div class="col-4 msb-txt-center">
                                    <img width="140px" src="{{asset('customer_images/'.$customer_details->customer_photo)}}" alt="">
                                    <br>
                                    Profile Photo
                                </div>
                                <div class="col-4 msb-txt-center">
                                    <img width="140px" src="{{asset('customer_images/'.$customer_details->nid_frontside)}}" alt="">
                                    <br>
                                    Photo 1
                                </div>
                                <div class="col-4 msb-txt-center">
                                    <img width="140px" src="{{asset('customer_images/'.$customer_details->nid_backside)}}" alt="">
                                    <br>
                                    Photo 2
                                </div>
                            </div>
                        </div>
                        @endif
                        <hr>
                        <div id="img_upload_div">
                        {!! Form::open(['method'=>'POST','route'=>'customer.photo.update','files'=>true]) !!}
                        <div class="row m-0 mt-3">
                            <div class="col-sm-7 m-auto">
                                <div class="form-group row">
                                    <label for="form-label" class="col-sm-4 control-label form-label-1">Profile Photo : </label>
                                    <div class="col-sm-8 pl-0">
                                        <input type="file" accept="image/*" name="customer_photo" class="form-control pd-y-12" autocomplete="off" placeholder="Attachment">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row m-0 mt-3">
                            <div class="col-sm-7 m-auto">
                                <div class="form-group row">
                                    <label for="form-label" class="col-sm-4 control-label form-label-1">Photo 1 : </label>
                                    <div class="col-sm-8 pl-0">
                                        <input type="file" accept="image/*" name="nid_frontside" class="form-control pd-y-12" autocomplete="off" placeholder="Attachment">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row m-0 mt-3">
                            <div class="col-sm-7 m-auto">
                                <div class="form-group row">
                                    <label for="form-label" class="col-sm-4 control-label form-label-1">Photo 2 : </label>
                                    <div class="col-sm-8 pl-0">
                                        <input type="file" accept="image/*" name="nid_backside" class="form-control pd-y-12" autocomplete="off" placeholder="Attachment">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                         <input type="hidden" name="id" value="{{base64_encode($customer_details->id)}}">
                         <div class="form-group text-center col-sm-12">
                            <button class="btn btn-info btn-sm custom-btn-1 ml-2" id="department_add_btn" type="submit"> Upload</button>
                            <button type="button" class="btn btn-danger btn-sm custom-btn-1 tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium" data-dismiss="modal">Close</button>
                        </div>
                        {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- modal-dialog -->
    </div><!-- modal -->

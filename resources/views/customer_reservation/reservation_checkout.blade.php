@extends('layout.master')
@section('title','Reservation Checkout')
@section('extra_css')
    {{ Html::style('theme/css/resources/customer_reservation.css') }}
@endsection

@section('content')
    <!-- ########## START: MAIN PANEL ########## -->
    <div class="br-profile-page">
        <div class="card shadow-base bd-0 rounded-0 widget-4">
            <div class="card-header ht-5">
                <div class="hidden-xs-down">
                </div>
                <div class="tx-24 hidden-xs-down">

                </div>
            </div><!-- card-header -->
        </div><!-- card -->
        <div class="row">

            <div class="col-lg-6  m-auto">
                <div class="card">
                    <div class="card-header card_header_2 text-center">
                       <h3 class="msb-txt-white"> Reservation Checkout >> <b>{{$reservation_details->reservation_number}}</b></h3>
                    </div>
                    <div class="card-body card_body_2">
                        <div class="row">
                            <div class="col-md-12">
                                {!! Form::open(['method'=>'POST','route'=>'customer_reservation.checkout.store']) !!}
                                <table class="table row-border order-column table-bordered stripe table-hover">
                                    <thead>
                                        <tr>
                                            <td class="msb-txt-center">Customer Name</td>
                                            <td class="msb-txt-center">{{$reservation_details->customer_name}}</td>
                                        </tr>
                                        <tr>
                                            <td class="msb-txt-center">Customer Phone</td>
                                            <td class="msb-txt-center">{{$reservation_details->customer_phone}}</td>
                                        </tr>
                                        <tr>
                                            <td class="msb-txt-center">Reservation Room</td>
                                            <td class="msb-txt-center">@foreach($reservation_rooms as $item1){{$item1->room_category_name}} - {{$item1->room_name_number}} <br >@endforeach</td>
                                        </tr>
                                        <tr>
                                            <td class="msb-txt-center">Reservation Date</td>
                                            <td class="msb-txt-center">{{date('d-m-Y', strtotime($reservation_details->reservation_date))}}</td>
                                        </tr>
                                        <tr>
                                            <td class="msb-txt-center">Checkin/Checkout Date</td>
                                            <td class="msb-txt-center">{{date('d-m-Y', strtotime($reservation_details->check_in_date))}} to {{date('d-m-Y', strtotime($reservation_details->check_out_date))}}</td>
                                        </tr>
                                        <tr>
                                            <td class="msb-txt-center">Room Rent Amount</td>
                                            <td class="msb-txt-center">
                                                @money($reservation_details->billing_amount_room) <br>
                                            <div class="msb-txt-center msb-txt-fw-bold msb-txt-green">Discount Amount: <span id="discount_amount_lbl">0</span> <input hidden="" name="discount_amount" id="discount_amount"></div></td>
                                                <input id="current_room_rent_amount" value="{{$reservation_details->billing_amount_room}}" hidden="" >
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="msb-txt-center">Others Bill Amount</td>
                                            <td class="msb-txt-center">
                                                @money($reservation_details->billing_amount)
                                                <input id="current_billing_amount" value="{{$reservation_details->billing_amount}}" hidden="" >
                                                <input name="bill_amount" value="{{($reservation_details->billing_amount_room+$reservation_details->billing_amount)}}" hidden="" >
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="msb-txt-center msb-txt-fw-bold msb-txt-green">Advance Payment Amount</td>
                                            <td class="msb-txt-center msb-txt-fw-bold msb-txt-green">
                                                @money($reservation_details->payment_amount)
                                                <input id="current_payment_amount" name="advance_payment_amount" value="{{$reservation_details->payment_amount}}" hidden="" >
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="msb-txt-center msb-txt-fw-bold msb-txt-darkred;">Previous Due Amount</td>
                                            <td class="msb-txt-center msb-txt-fw-bold msb-txt-darkred;">
                                                @money($reservation_details->previous_due_amount)
                                                <input id="previous_due_amount" name="previous_due_amount" value="{{$reservation_details->previous_due_amount}}" hidden="" >
                                                <input id="previous_reservation_checkout_id" name="previous_reservation_checkout_id" value="{{$reservation_details->previous_reservation_checkout_id}}" hidden="" >
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="msb-txt-center msb-txt-fw-bold msb-txt-red;">Total Payable Amount</td>
                                            <td class="msb-txt-center msb-txt-fw-bold msb-txt-red;"><b><span id="total_payable_amount">@money(($reservation_details->billing_amount+$reservation_details->billing_amount_room+                                            $reservation_details->previous_due_amount)-($reservation_details->payment_amount))</span></b></td>
                                        </tr>
                                        <tr>
                                            <td class="msb-txt-center color: black; vertical-align:middle;" valign="center"><b>Discount Percentage (%) </b><br> (Only for Room Rent)</td>
                                            <td class="text-align: center; msb-txt-fw-bold msb-txt-black">
                                                <div class="input-group mb-3">
                                                    <input type="number" placeholder="Discount Percentage" minlength="0" max="100" class="form-control msb-txt-center" step="any" id="discount_percentage" autocomplete="off" value="0" name="discount_percentage">
                                                  <div class="input-group-append">
                                                    <span class="input-group-text" id="basic-addon2">%</span>
                                                  </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="msb-txt-center msb-txt-fw-bold msb-txt-black vertical-align:middle;" valign="center">Paid Amount</td>
                                            <td class="msb-txt-center msb-txt-fw-bold msb-txt-black"><input type="number" placeholder="Paid Amount" required="" minlength="0" class="form-control msb-txt-center" step="any" autocomplete="off" autofocus="" id="paid_amount" name="paid_amount"></td>
                                        </tr>
                                        <tr>
                                            <td class="msb-txt-center msb-txt-fw-bold msb-txt-red"><h3>Total Due Amount</h3></td>
                                            <td class="msb-txt-center msb-txt-fw-bold msb-txt-red"><h3><span id="total_due_amount"></span></h3>
                                                <input hidden="" id="total_due_amount_input" name="current_due_amount"></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2"  class="msb-txt-center">
                                                <input hidden="" id="calculate_payable_amount">
                                                <input hidden="" id="calculate_summary_amount">
                                                <input value="{{$reservation_details->id}}" name="reservation_id" hidden="">
                                                <button class="btn btn-info btn-sm custom-btn-1 ml-2" onclick="return confirm('Are you sure to Submit?')" type="submit"> Submit for Checkout </button>
                                            </td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                        </tr>
                                    </tbody>
                                </table>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- row -->
    </div>

@endsection
@section('extra_js')
{{ Html::script('theme/js/resources/customer_reservation.js') }}
@endsection
































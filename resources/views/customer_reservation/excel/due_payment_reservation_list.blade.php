<!DOCTYPE html>
<html>
<head>
  <title>DUE CUSTOMER LISTt</title>
</head>
<body>
  <style type="text/css">
    @page { sheet-size: A4; }
    .table2{
      border-collapse: collapse;
      width: 100%;
      text-align: center;
      border:1px solid black;

    }
    .table2 td,th{
      border:1px solid black;
    }
    .table2 td{
      font-size: 11px;
      padding: 2px 2px;
      border:1px solid black;
      color: #FF0a44;
    }
    .table2 th{
      font-size: 10px;
      padding: 3px 1px;
    }
    .table3{
      border-collapse: collapse;
      text-align: center;
      width: 100%;
      text-decoration: underline;
      font-weight: bold;
      font-size: 11px;
    }
    .table3 td{
      text-decoration: underline;
    }
    .table1{
      text-align: center;
      width: 100%;
    }
    .caddress{
      font-size: 11px;
    }

  </style>
   <table>
    <tr>
      <td colspan="12" style="font-size: 18px; font-weight: 800; text-transform: uppercase; background: #FFE57C;" > DUE CUSTOMER LIST</td>
    </tr>
    <tr> <td colspan="12" style="font-size: 11px; text-transform: uppercase;" > Download By: {{$data_list->downloaded_by}}</td> </tr>
    <tr> <td colspan="12" style="font-size: 11px; text-transform: uppercase;" > Download DateTime: {{date('d-m-Y H:i a')}}</td> </tr>
  </table>
  <table class="table2">
    <thead>
      <tr>
          <th style="text-align: center; border: 1px solid black; font-weight: 800;" width="5%">SN</th>
          <th style="text-align: center; border: 1px solid black; font-weight: 800;">Customer Name</th>
          <th style="text-align: center; border: 1px solid black; font-weight: 800;">Customer Phone</th>
          <th style="text-align: center; border: 1px solid black; font-weight: 800;">Recent Reservation Number</th>
          <th style="text-align: center; border: 1px solid black; font-weight: 800;">Reservation Date</th>
          <th style="text-align: center; border: 1px solid black; font-weight: 800;">Check in Date</th>
          <th style="text-align: center; border: 1px solid black; font-weight: 800;">Check out Date</th>
          <th style="text-align: center; border: 1px solid black; font-weight: 800;">Bill Amount</th>
          <th style="text-align: center; border: 1px solid black; font-weight: 800;">Paid Amount</th>
          <th style="text-align: center; border: 1px solid black; font-weight: 800;">Discount Amount</th>
          <th style="text-align: center; border: 1px solid black; font-weight: 800;">Previous Due Amount</th>
          <th style="text-align: center; border: 1px solid black; font-weight: 800;">Total Due Amount</th>
      </tr>
  </thead>
  <tbody>
  @php $i=0; $total_due = 0; @endphp
  @foreach($data_list as $item)
  @php $total_due += $item->current_due_amount; @endphp
      <tr>
          <td style="text-align: center; border: 1px solid black;">{{++$i}}</td>
          <td style="text-align: center; border: 1px solid black;">{{$item->customer_name}}</td>
          <td style="text-align: center; border: 1px solid black;">{{$item->customer_phone}}</td>
          <td style="text-align: center; border: 1px solid black;">{{$item->reservation_number}}</td>
          <td style="text-align: center; border: 1px solid black;">{{date('d-m-Y', strtotime($item->reservation_date))}}</td>
          <td style="text-align: center; border: 1px solid black;">{{date('d-m-Y', strtotime($item->check_in_date))}}</td>
          <td style="text-align: center; border: 1px solid black;">{{date('d-m-Y', strtotime($item->check_out_date))}}</td>
          <td style="text-align: center; border: 1px solid black;">{{$item->total_bill_amount}}</td>
          <td style="text-align: center; border: 1px solid black;">{{$item->advance_payment_amount+$item->paid_payment}}</td>
          <td style="text-align: center; border: 1px solid black;">{{$item->total_discount_amount}}</td>
          <td style="text-align: center; border: 1px solid black;">{{$item->previous_due_amount}}</td>
          <td style="text-align: center; border: 1px solid black;">{{$item->current_due_amount}}</td>
      </tr>
  @endforeach
  </tbody>
  <tfoot>
    <tr>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th style="text-align: center; font-weight: 800; border: 1px solid black;">Total</th>
      <th style="text-align: center; font-weight: 800; border: 1px solid black;">{{$total_due}}</th>
    </tr>
  </tfoot>
</table>
</body>
</html>

<!DOCTYPE html>
<html>
<head>
  <title>CANCELLED LIST</title>
</head>
<body>
  <style type="text/css">
    @page { sheet-size: A4; }
    .table2{
      border-collapse: collapse;
      width: 100%;
      text-align: center;
      border:1px solid black;

    }
    .table2 td,th{
      border:1px solid black;
    }
    .table2 td{
      font-size: 11px;
      padding: 2px 2px;
      border:1px solid black;
      color: #FF0a44;
    }
    .table2 th{
      font-size: 10px;
      padding: 3px 1px;
    }
    .table3{
      border-collapse: collapse;
      text-align: center;
      width: 100%;
      text-decoration: underline;
      font-weight: bold;
      font-size: 11px;
    }
    .table3 td{
      text-decoration: underline;
    }
    .table1{
      text-align: center;
      width: 100%;
    }
    .caddress{
      font-size: 11px;
    }

  </style>
   <table>
    <tr>
      <td colspan="12" style="font-size: 18px; font-weight: 800; text-transform: uppercase; background: #FFE57C;" > CANCELLED RESERVATION LIST</td>
    </tr>
    <tr> <td colspan="12" style="font-size: 11px; text-transform: uppercase;" > Download By: {{$data_list->downloaded_by}}</td> </tr>
    <tr> <td colspan="12" style="font-size: 11px; text-transform: uppercase;" > Download DateTime: {{date('d-m-Y H:i a')}}</td> </tr>
  </table>
  <table class="table2">
    <thead>
      <tr>

          <th style="text-align: center; border: 1px solid black; font-weight: 800;" width="5%">SN</th>
          <th style="text-align: center; border: 1px solid black; font-weight: 800;">Reservation Number</th>
          <th style="text-align: center; border: 1px solid black; font-weight: 800;">Customer Name</th>
          <th style="text-align: center; border: 1px solid black; font-weight: 800;">Customer Phone</th>
          <th style="text-align: center; border: 1px solid black; font-weight: 800;">Reserved Room</th>
          <th style="text-align: center; border: 1px solid black; font-weight: 800;">Reservation Date</th>
          <th style="text-align: center; border: 1px solid black; font-weight: 800;">Check in Date</th>
          <th style="text-align: center; border: 1px solid black; font-weight: 800;">Check out Date</th>
          <th style="text-align: center; border: 1px solid black; font-weight: 800;">Cancellation Reason</th>
      </tr>
  </thead>
  <tbody>
  @php $i=0; @endphp
  @foreach($data_list as $item)
      <tr>
          <td style="text-align: center; border: 1px solid black;">{{++$i}}</td>
          <td style="text-align: center; border: 1px solid black;">{{$item->reservation_number}}</td>
          <td style="text-align: center; border: 1px solid black;">{{$item->customer_name}}</td>
          <td style="text-align: center; border: 1px solid black;">{{$item->customer_phone}}</td>
          <td style="text-align: center; border: 1px solid black;">@foreach($item->reservation_room_list as $item1){{$item1->room_category_name}} - {{$item1->room_name_number}}, @endforeach</td>
          <td style="text-align: center; border: 1px solid black;">{{date('d-m-Y', strtotime($item->reservation_date))}}</td>
          <td style="text-align: center; border: 1px solid black;">{{date('d-m-Y', strtotime($item->check_in_date))}}</td>
          <td style="text-align: center; border: 1px solid black;">{{date('d-m-Y', strtotime($item->check_out_date))}}</td>
          <td style="text-align: center; border: 1px solid black;">{{$item->reason_of_cancellation}}</td>
      </tr>
  @endforeach
  </tbody>
</table>
</body>
</html>

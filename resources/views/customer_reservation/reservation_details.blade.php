@extends('layout.master')
@section('title','Reservation Summary')
@section('extra_css')
    {{ Html::style('theme/css/resources/customer_reservation.css') }}
@endsection

@section('content')
    <!-- ########## START: MAIN PANEL ########## -->
    <div class="br-profile-page">
        <div class="card shadow-base bd-0 rounded-0 widget-4">
            <div class="card-header ht-5">
                <div class="hidden-xs-down">
                </div>
                <div class="tx-24 hidden-xs-down">
                </div>
            </div><!-- card-header -->
        </div><!-- card -->
        <div class="row">

            <div class="col-lg-11  m-auto">
                <div class="card">
                    <div class="card-header card_header_2">
                        <div class="row">
                            <div class="col-md-6">
                                <p> Reservation Number: <b>{{$reservation_details->reservation_number}}</b></p>
                            </div>
                            <div class="col-md-6 msb-txt-right">
                                <a href="" class="btn btn-link btn-md" data-toggle="modal" data-target="#cancel_modal"> <i class="fas fa-times"></i> Cancel Reservation</a>
                                <a class="btn btn-md btn-danger" href="{{route('customer_reservation.reservation_checkout', base64_encode($reservation_details->id))}}"  onclick="return confirm('Are you sure to checkout?')" title="Checkout"><i class="fas fa-random"></i> Checkout</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body card_body_2">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table row-border order-column table-bordered stripe table-hover">
                                    <thead>
                                        <tr>
                                            <th class="msb-txt-center">Customer Name</th>
                                            <th class="msb-txt-center">Customer Phone</th>
                                            <th class="msb-txt-center">Reservation Room</th>
                                            <th class="msb-txt-center">Reservation Date</th>
                                            <th class="msb-txt-center">Checkin Date</th>
                                            <th class="msb-txt-center">Checkout Date</th>
                                            <th class="msb-txt-center">Previous Due Amount</th>
                                            <th class="msb-txt-center">Total Due Amount</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="msb-txt-center">{{$reservation_details->customer_name}}</td>
                                            <td class="msb-txt-center">{{$reservation_details->customer_phone}}</td>
                                            <td class="msb-txt-center">@foreach($reservation_rooms as $item1){{$item1->room_category_name}} - {{$item1->room_name_number}} <br >@endforeach</td>
                                            <td class="msb-txt-center">{{date('d-m-Y', strtotime($reservation_details->reservation_date))}}</td>
                                            <td class="msb-txt-center">{{date('d-m-Y', strtotime($reservation_details->check_in_date))}}</td>
                                            <td class="msb-txt-center">{{date('d-m-Y', strtotime($reservation_details->check_out_date))}}</td>
                                            <td class="msb-txt-center msb-txt-red"><h5><b>@money($reservation_details->previous_due_amount)</b></h5></td>
                                            <td class="msb-txt-center msb-txt-red"><h3><b>@money(($reservation_details->billing_amount+$reservation_details->previous_due_amount) - $reservation_details->payment_amount)</b></h3></td>
                                            
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <hr>
            </div>
            <div class="col-lg-6 m-auto">
                <div class="card">
                    <div class="card-header card_header_2">
                        <div class="row">
                            <div class="col-md-6">
                                <p>Bill Details</p>
                            </div>
                            <div class="col-md-6 msb-txt-right">
                                <a href="" class="btn btn-info btn-sm" data-toggle="modal" data-target="#add_bill_modal"> <i class="fas fa-plus-square"></i> Add Bill</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body card_body_2">
                        <div class="row">
                            <div class="col-md-12">
                                <table id="datatable1" class="table row-border order-column table-bordered stripe table-hover">
                                    <thead>
                                        <tr>
                                            <th>SN</th>
                                            <th>Billing Head</th>
                                            <th>Description</th>
                                            <th>Amount</th>
                                            <th>Date</th>
                                            <th>Created By</th>
                                            <th>#</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php $i=0; @endphp
                                        @foreach($billing_history as $bh)
                                            <tr>
                                                <td>{{++$i}}</td>
                                                <td>{{$bh->billing_head_name}}</td>
                                                <td>{{$bh->billing_description}}</td>
                                                <td>@money($bh->billing_amount)</td>
                                                <td>{{date('d-m-Y', strtotime($bh->bill_date))}}</td>
                                                <td>{{$bh->createdBy}}</td>
                                                <td>
                                                    <a class="msb-txt-red" href="{{route('customer_reservation.billing.destroy', base64_encode($bh->id))}}" onclick="return confirm('Are you sure to destroy?')" title="Destroy"><i class="fas fa-trash-alt"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th colspan="3" class="msb-txt-center">Total</th>
                                            <th>@money($reservation_details->billing_amount)</th>
                                            <th colspan="3"></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-5  m-auto">
                <div class="card">
                    <div class="card-header card_header_2">
                        <div class="row">
                            <div class="col-md-6">
                                <p>Payment Details</p>
                            </div>
                            <div class="col-md-6 msb-txt-right">
                                <a href="" class="btn btn-success btn-sm " data-toggle="modal" data-target="#add_payment_modal"> <i class="fas fa-plus-square"></i> Add Payment</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body card_body_2">
                        <div class="row">
                            <div class="col-md-12">
                                <table id="datatable2" class="table row-border order-column table-bordered stripe table-hover">
                                    <thead>
                                        <tr>
                                            <th>SN</th>
                                            <th>Description</th>
                                            <th>Amount</th>
                                            <th>Date</th>
                                            <th>Created By</th>
                                            <th>#</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php $i=0; @endphp
                                        @foreach($payment_history as $ph)
                                            <tr>
                                                <td>{{++$i}}</td>
                                                <td>{{$ph->payment_description}}</td>
                                                <td>@money($ph->payment_amount)</td>
                                                <td>{{date('d-m-Y', strtotime($ph->payment_date))}}</td>
                                                <td>{{$ph->createdBy}}</td>
                                                <td>
                                                    <a class="msb-txt-red" href="{{route('customer_reservation.payment.destroy', base64_encode($ph->id))}}" onclick="return confirm('Are you sure to destroy?')" title="Destroy"><i class="fas fa-trash-alt"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th colspan="2" class="msb-txt-center">Total</th>
                                            <th>@money($reservation_details->payment_amount)</th>
                                            <th colspan="3"></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div><!-- row -->
    </div>


    <div id="add_bill_modal" class="modal fade effect-sign">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content bd-0 tx-14">
                <div class="modal-header pd-y-15 pd-x-25 modal_header_1">
                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="fa fa-plus-circle"></i> Add New Bill </h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fas fa-times"></i></span>
                    </button>
                </div>
                <div class="modal-body pd-20">
                    <span id="form_result"></span>
                    <div class="modal_body_inner">
                        {!! Form::open(['method'=>'POST','route'=>'customer_reservation.billing_information.store']) !!}
                        <div class="row">
                            <div class="col-12 mb-3">
                                <h6>New Billing Information</h6>
                                <hr>
                            </div>

                            <div class="col-sm-12">
                                <div class="form-group row ">
                                    <label for="form-label" class="col-sm-4 control-label form-label-1"> Billing Head</label>
                                    <div class="col-sm-8 pl-0">
                                        <select class="form-control" name="billing_head_id" id="billing_head_id" data-placeholder="Billing Head" required="">
                                        <option value="">Select a Billing Head </option>
                                        @foreach($billing_head_list as $data)
                                            <option value='{{ $data->id }}'>{{ $data->billing_head_name }}</option>
                                        @endforeach
                                    </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <label for="form-label" class="col-sm-4 control-label form-label-1"> Description </label>
                                    <div class="col-sm-8 pl-0">
                                        <textarea name="billing_description" id="description" cols="" rows="3" class="form-control" placeholder="Description"></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <label for="form-label" class="col-sm-4 control-label form-label-1"> Billing Amount </label>
                                    <div class="col-sm-8 pl-0">
                                        <input type="number" step="any" placeholder="Billing Amount" name="billing_amount" class="form-control" required="" >
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <label for="form-label" class="col-sm-4 control-label form-label-1"> Billing Date </label>
                                    <div class="col-sm-8 pl-0">
                                        <input type="text" placeholder="Billing Date" name="bill_date" class="form-control fc-datepicker" required="" >
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-sm-12"><br></div>

                        <div class="form-group text-center col-sm-12">
                            <input name="reservation_id" hidden="" value="{{$reservation_details->id}}">
                            <button class="btn btn-info btn-sm custom-btn-1 ml-2" type="submit"><i class="fa fa-save"></i> Submit</button>
                            <button type="button" class="btn btn-danger btn-sm custom-btn-1 tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div><!-- modal-dialog -->
    </div><!-- modal -->


    <div id="add_payment_modal" class="modal fade effect-sign">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content bd-0 tx-14">
                <div class="modal-header pd-y-15 pd-x-25 modal_header_1">
                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="fa fa-plus-circle"></i> Add New Payment </h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fas fa-times"></i></span>
                    </button>
                </div>
                <div class="modal-body pd-20">
                    <span id="form_result"></span>
                    <div class="modal_body_inner">
                        {!! Form::open(['method'=>'POST','route'=>'customer_reservation.payment_information.store']) !!}
                        <div class="row">
                            <div class="col-12 mb-3">
                                <h6>New Payment Information</h6>
                                <hr>
                            </div>

                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <label for="form-label" class="col-sm-4 control-label form-label-1">Payment Description </label>
                                    <div class="col-sm-8 pl-0">
                                        <textarea name="payment_description" id="description" cols="" rows="3" class="form-control" placeholder="Payment Description"></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <label for="form-label" class="col-sm-4 control-label form-label-1"> Payment Amount </label>
                                    <div class="col-sm-8 pl-0">
                                        <input type="number" step="any" placeholder="Payment Amount" name="payment_amount" class="form-control" required="" >
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <label for="form-label" class="col-sm-4 control-label form-label-1"> Payment Date </label>
                                    <div class="col-sm-8 pl-0">
                                        <input type="text" placeholder="Payment Date" name="payment_date" class="form-control fc-datepicker" required="" >
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-sm-12"><br></div>

                        <div class="form-group text-center col-sm-12">
                            <input name="reservation_id" hidden="" value="{{$reservation_details->id}}">
                            <button class="btn btn-info btn-sm custom-btn-1 ml-2" type="submit"><i class="fa fa-save"></i> Submit</button>
                            <button type="button" class="btn btn-danger btn-sm custom-btn-1 tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div><!-- modal-dialog -->
    </div><!-- modal -->

    <div id="cancel_modal" class="modal fade effect-sign">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content bd-0 tx-14">
                <div class="modal-header pd-y-15 pd-x-25 modal_header_1">
                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="fa fa-times"></i> Reservation Cancellation </h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fas fa-times"></i></span>
                    </button>
                </div>
                <div class="modal-body pd-20">
                    <span id="form_result"></span>
                    <div class="modal_body_inner">
                        {!! Form::open(['method'=>'POST','route'=>'customer_reservation.cancellation.store']) !!}
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <input name="reservation_id" value="{{$reservation_details->id}}" hidden="">
                                        <textarea class="form-control" rows="5" name="reason_of_cancellation" required="" placeholder="Reason of Reservation Cancellation"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12"><br></div>

                        <div class="form-group text-center col-sm-12">
                            <button class="btn btn-info btn-sm custom-btn-1 ml-2" type="submit"><i class="fa fa-save"></i> Submit</button>
                            <button type="button" class="btn btn-danger btn-sm custom-btn-1 tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div><!-- modal-dialog -->
    </div><!-- modal -->


@endsection
@section('extra_js')
    {{ Html::script('theme/js/resources/customer_reservation.js') }}
@endsection
































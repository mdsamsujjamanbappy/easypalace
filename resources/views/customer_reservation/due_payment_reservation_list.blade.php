@extends('layout.master')
@section('title','Due Payment Customer List')
@section('extra_css')
    {{ Html::style('theme/css/resources/customer_reservation.css') }}
@endsection
@section('content')

    <div class="br-pagetitle my-pagetitle">
        <div>
            <h4> <i class="icon icon ion-ios-bookmarks-outline"></i>Due Payment Customer List</h4>
        </div>

        <div class="pagetitle-btn">
            <a href="{{route('customer_reservation.due_payment_reservation_list_excel')}}" class="btn  btn-info btn-sm custom-btn-1 btn-1 tx-11 tx-uppercase pd-y-8 pd-x-18 tx-mont tx-medium"> <i class="fas fa-download"></i> Download as Excel</a>
        </div>
    </div>
    <div class="br-pagebody br-pagebody-1">
        <div class="br-section-wrapper">
            <div class="table-wrapper scrollme">
            
                <table id="datatable" class="table row-border order-column table-bordered stripe  table-hover" >
                    <thead>
                        <tr>
                            <th class="msb-txt-center" width="5%">SN</th>
                            <th class="msb-txt-center">Customer Name</th>
                            <th class="msb-txt-center">Customer Phone</th>
                            <th class="msb-txt-center">Recent Reservation Number</th>
                            <th class="msb-txt-center">Reservation Date</th>
                            <th class="msb-txt-center">Check in Date</th>
                            <th class="msb-txt-center">Check out Date</th>
                            <th class="msb-txt-center">Bill Amount</th>
                            <th class="msb-txt-center">Paid Amount</th>
                            <th class="msb-txt-center">Discount Amount</th>
                            <th class="msb-txt-center">Previous Due Amount</th>
                            <th class="msb-txt-center">Total Due Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                    @php $i=0; @endphp
                    @foreach($reservation_list as $item)
                        <tr>
                            <td class="msb-txt-center">{{++$i}}</td>
                            <td class="msb-txt-center">{{$item->customer_name}}</td>
                            <td class="msb-txt-center">{{$item->customer_phone}}</td>
                            <td class="msb-txt-center">{{$item->reservation_number}}</td>
                            <td class="msb-txt-center">{{date('d-m-Y', strtotime($item->reservation_date))}}</td>
                            <td class="msb-txt-center">{{date('d-m-Y', strtotime($item->check_in_date))}}</td>
                            <td class="msb-txt-center">{{date('d-m-Y', strtotime($item->check_out_date))}}</td>
                            <td class="msb-txt-center">@money($item->total_bill_amount)</td>
                            <td class="msb-txt-center">@money($item->advance_payment_amount+$item->paid_payment)</td>
                            <td class="msb-txt-center">@money($item->total_discount_amount)</td>
                            <td class="msb-txt-center">@money($item->previous_due_amount)</td>
                            <td class="msb-txt-center">@money($item->current_due_amount)</td>
                            
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

        </div>
    </div>
@endsection

@section('extra_js')
    {{ Html::script('theme/js/resources/customer_reservation.js') }}
@endsection




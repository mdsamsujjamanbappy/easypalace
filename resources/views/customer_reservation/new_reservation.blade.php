@extends('layout.master')
@section('title','Add New Reservation')
@section('extra_css')
    {{ Html::style('theme/css/resources/customer_reservation.css') }}
@endsection
@section('content')

    <div class="br-pagetitle my-pagetitle">
        <div>
            <h4> <i class="icon fa fa-user"></i> New Reservation Information</h4>
        </div>

        <div class="pagetitle-btn">
            <a href="{{route('customer_reservation.reservation_list')}}" class="btn  btn-info btn-sm custom-btn-1 btn-1 tx-11 tx-uppercase pd-y-8 pd-x-18 tx-mont tx-medium"> <i class="fas fa-users"></i> Reservation List</a>
        </div>
    </div>
    <div class="br-pagebody br-pagebody-1">
        <div class="br-section-wrapper">
            <div class="table-wrapper">
               {!! Form::open(['method'=>'POST','route'=>'customer_reservation.new_reservation.store', 'files' =>true]) !!}
                    <div class="row">
                
                        <div class="col-sm-6">
                            <div class="form-group row select_2_row_modal">
                                <label for="form-label" class="col-sm-3 control-label form-label-1"> Customer <span class="msb-txt-red">*</span></label>
                                <div class="col-sm-8 pl-0">
                                    <select class="form-control" name="customer_id" id="customer_id" data-placeholder="Customer" required="">
                                        <option value="">Select Customer</option>
                                        @foreach($customer_list as $customer)
                                        <option value='{{ $customer->id }}'>{{ $customer->customer_name }} ({{ $customer->customer_phone }})</option>
                                    @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6"></div>

                        <div class="col-sm-6">
                            <div class="form-group row select_2_row_modal">
                                <label for="form-label" class="col-sm-3 control-label form-label-1"> Room Category  <span class="msb-txt-red">*</span></label>
                                <div class="col-sm-8 pl-0">
                                    <select class="form-control" name="room_category_id" id="room_category_id" data-placeholder="Room Category" required="">
                                        <option value="">Select a Category </option>
                                        @foreach($room_category_list as $data)
                                            <option value='{{ $data->id }}'>{{ $data->room_category_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    
                        <div class="col-sm-6">
                            <div class="form-group row select_2_row_modal">
                                <label for="form-label" class="col-sm-3 control-label form-label-1">Initial Booking Room  <span class="msb-txt-red">*</span></label>
                                <div class="col-sm-8 pl-0">
                                    <select class="form-control" name="room_id" id="room_id" data-placeholder="Room" required="">
                                        <option value="">Select a Room </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-3 control-label form-label-1">Check In Date <span class="msb-txt-red">*</span></label>
                                <div class="col-sm-8 pl-0">
                                    <input type="text" class="form-control fc-datepicker" name="check_in_date" autocomplete="off" placeholder="Checkin Date" required="" >
                                </div>
                            </div>
                        </div>
                        
                        
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-3 control-label form-label-1">Check Out Date <span class="msb-txt-red">*</span></label>
                                <div class="col-sm-8 pl-0">
                                    <input type="text" class="form-control fc-datepicker" name="check_out_date" autocomplete="off" placeholder="Check Out Date" required="" >
                                </div>
                            </div>
                        </div>
                            
                        
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-3 control-label form-label-1">No of Adults  <span class="msb-txt-red">*</span></label>
                                <div class="col-sm-8 pl-0">
                                    <input type="text" class="form-control" name="adult_number" autocomplete="off" placeholder="No of Adults" required="" >
                                </div>
                            </div>
                        </div>
                            
                        
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-3 control-label form-label-1">No of Kid</label>
                                <div class="col-sm-8 pl-0">
                                    <input type="text" class="form-control" name="children_number" autocomplete="off" placeholder="No of Kid" >
                                </div>
                            </div>
                        </div>
                            
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-3 control-label form-label-1">Remarks</label>
                                <div class="col-sm-8 pl-0">
                                    <textarea type="text" class="form-control" name="remarks" autocomplete="off" placeholder="Remarks"></textarea>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <hr>
                    <div class="form-group text-center col-sm-12">
                        <button class="btn btn-info btn-sm custom-btn-1 ml-2" type="submit"> Save Information</button>
                    </div>
                    {!! Form::close() !!}
                
            </div>
        </div>
    </div>
@endsection

@section('extra_js')

<script>
    $(document).ready(function(){
        'use strict';
        //get Room
        $('#room_category_id').on('change',function () {
            var id = $("#room_category_id").val();
            $.ajax({
                type: "GET",
                url:"{{url('/ajax/room_type/get_hotel_room/')}}"+"/"+id,
                success:function (response) {
                    $('#room_id').html(response);
                    $("#room_id").select2({
                        placeholder: "Select a Room"
                    });
                },
                error:function(response){
                  console.log(response);
                }
            });
        });
    });
</script>
{{ Html::script('theme/js/resources/customer_reservation.js') }}
@endsection




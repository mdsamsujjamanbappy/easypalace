@extends('layout.master')
@section('title','Cancelled Reservation List')
@section('extra_css')
    {{ Html::style('theme/css/resources/customer_reservation.css') }}
@endsection
@section('content')

    <div class="br-pagetitle my-pagetitle">
        <div>
            <h4> <i class="icon icon ion-ios-bookmarks-outline"></i> Cancelled Reservation List</h4>
        </div>

        <div class="pagetitle-btn">
            <a href="{{route('customer_reservation.cancelled_reservation_list_excel')}}" class="btn  btn-info btn-sm custom-btn-1 btn-1 tx-11 tx-uppercase pd-y-8 pd-x-18 tx-mont tx-medium"> <i class="fas fa-download"></i> Download as Excel</a>
        </div>
    </div>
    <div class="br-pagebody br-pagebody-1">
        <div class="br-section-wrapper">
            <div class="table-wrapper scrollme">
            
                <table id="datatable" class="table row-border order-column table-bordered stripe  table-hover" >
                    <thead>
                        <tr>
                            <th class="msb-txt-center" width="5%">SN</th>
                            <th class="msb-txt-center">Reservation Number</th>
                            <th class="msb-txt-center">Customer Name</th>
                            <th class="msb-txt-center">Customer Phone</th>
                            <th class="msb-txt-center">Reserved Room</th>
                            <th class="msb-txt-center">Reservation Date</th>
                            <th class="msb-txt-center">Check in Date</th>
                            <th class="msb-txt-center">Check out Date</th>
                            <th class="msb-txt-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @php $i=0; @endphp
                    @foreach($data_list as $item)
                        <tr>
                            <td class="msb-txt-center">{{++$i}}</td>
                            <td class="msb-txt-center">{{$item->reservation_number}}</td>
                            <td class="msb-txt-center">{{$item->customer_name}}</td>
                            <td class="msb-txt-center">{{$item->customer_phone}}</td>
                            <td class="msb-txt-center">@foreach($item->reservation_room_list as $item1){{$item1->room_category_name}} - {{$item1->room_name_number}} <br >@endforeach</td>
                            <td class="msb-txt-center">{{date('d-m-Y', strtotime($item->reservation_date))}}</td>
                            <td class="msb-txt-center">{{date('d-m-Y', strtotime($item->check_in_date))}}</td>
                            <td class="msb-txt-center">{{date('d-m-Y', strtotime($item->check_out_date))}}</td>
                            <td class="msb-txt-center">
                                <button type="button" value="{{base64_encode($item->id)}}" class="view_details btn btn-sm btn-success" title="View Details"><i class="fa fa-eye"></i></button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

        </div>
    </div>

    <div id="details_modal" class="modal fade effect-sign">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content bd-0 tx-14">
                <div class="modal-header pd-y-15 pd-x-25 modal_header_1">
                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="fa fa-list"></i> Cancellation Information</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fas fa-times"></i></span>
                    </button>
                </div>
                <div class="modal-body pd-20">
                    <span id="form_result"></span>
                    <div class="modal_body_inner">
                        <div class="row">
                        <div class="col-sm-12">
                           <table class="table table-bordered">
                              <tr>
                                    <td>
                                        Reason of Reservation Cancellation <hr>    
                                        <div class="msb-txt-fw-700" id="cancellation_reason_details"></div>
                                    </td>
                              </tr>
                           </table>
                        </div>

                        <div class="form-group text-center col-sm-12">
                            <button type="button" class="btn btn-danger btn-sm custom-btn-1 tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                        </div>

                        </div>
                    </div>
                </div>
            </div>
        </div><!-- modal-dialog -->
    </div><!-- modal -->
@endsection

@section('extra_js')
    <script type="text/javascript">
        $(document).on('click', '.view_details', function(){
          'use strict';
          var id = $(this).attr('value');
          $.ajax({
           type: "GET",
           url:"{{url('/customer/reservation/cancelled/details')}}"+"/"+id,
           dataType:"json",
           success:function(response){
             // console.log(response);
             $("#details_modal").modal('show');
             $('#cancellation_reason_details').html(response.reason_of_cancellation);
           },
            error:function(response){
                console.log(response);
            },
          })
        });
    </script>
    {{ Html::script('theme/js/resources/customer_reservation.js') }}
@endsection




@extends('layout.master')
@section('title','Dashboard')
@section('extra_css')
@endsection
@section('content')
    <div class="br-pagetitle">
        <i class="tx-40 fa fa-user"></i>
        <div>
            <h3>Hello <b>{{Auth::user()->name}}</b></h3>
            <p class="mg-b-0">Welcome to customer panel.</p>
        </div>
    </div>

    <div class="br-pagebody">
        <div class="row row-sm row-cards-one">
            <div class="col-sm-12 col-xl-12">
               
            </div><!-- col-3 -->
        </div><!-- row -->
    </div>
@endsection

@section('extra_js')
@endsection
<?php
$url=request()->route()->getName();
?>
<div class="br-sideleft sideleft-scrollbar">
    <label class="sidebar-label pd-x-10 mg-t-20 op-3"></label>
    <ul class="br-sideleft-menu">
        
        <li class="br-menu-item">
            <a href="{{route('dashboard')}}" class="br-menu-link @if( $url=='dashboard' || $url=='home' || $url=='dashboard') active @endif ">
                <i class="menu-item-icon icon ion-ios-home-outline tx-24"></i>
                <span class="menu-item-label">Dashboard</span>
            </a><!-- br-menu-link -->
        </li><!-- br-menu-item -->

        <?php if(checkPermission(['super_admin']) || checkPermission(['admin']) || checkPermission(['executive'])){ ?>
        <li class="br-menu-item">
            <a href="#" class="br-menu-link with-sub @if( $url=='customer_reservation.reservation_list' || $url=='customer_reservation.new_reservation' || $url=='customer_reservation.due_payment_reservation_list' || $url=='customer_reservation.cancelled_reservation_list' || $url=='customer_reservation.new_reservation' ) active @endif">
                <i class="menu-item-icon icon ion-ios-photos-outline tx-20"></i>
                <span class="menu-item-label">Reservation Management</span>
            </a><!-- br-menu-link -->
            <ul class="br-menu-sub">
                <li class="sub-item"><a href="{{route('customer_reservation.reservation_list')}}" class="sub-link @if( $url=='customer_reservation.reservation_list') active @endif "> Reservation List</a> </li>
                <li class="sub-item"><a href="{{route('customer_reservation.due_payment_reservation_list')}}" class="sub-link @if( $url=='customer_reservation.due_payment_reservation_list') active @endif "> Due Customer List</a> </li>
                <li class="sub-item"><a href="{{route('customer_reservation.cancelled_reservation_list')}}" class="sub-link @if( $url=='customer_reservation.cancelled_reservation_list') active @endif "> Cancelled Reservation List</a> </li>
                <li class="sub-item"><a href="{{route('customer_reservation.new_reservation')}}" class="sub-link @if( $url=='customer_reservation.new_reservation') active @endif "> New Reservation</a> </li>
            </ul>
        </li>
        <?php } ?>

        <?php if(checkPermission(['super_admin']) || checkPermission(['admin']) || checkPermission(['executive'])){ ?>
        <li class="br-menu-item">
            <a href="#" class="br-menu-link with-sub @if( $url=='customer_request.pending_list' || $url=='customer_request.onprocessing_list' || $url=='customer_request.completed_list' || $url=='customer_request.rejected_list') active @endif">
                <i class="menu-item-icon icon ion-ios-photos-outline tx-20"></i>
                <span class="menu-item-label">Service Request</span>
            </a><!-- br-menu-link -->
            <ul class="br-menu-sub">
                <li class="sub-item"><a href="{{route('customer_request.pending_list')}}" class="sub-link @if( $url=='customer_request.pending_list') active @endif "> Pending Request</a> </li>
                <li class="sub-item"><a href="{{route('customer_request.onprocessing_list')}}" class="sub-link @if( $url=='customer_request.onprocessing_list') active @endif "> On Processing Request</a> </li>
                <li class="sub-item"><a href="{{route('customer_request.completed_list')}}" class="sub-link @if( $url=='customer_request.completed_list') active @endif "> Completed Request</a> </li>
                <li class="sub-item"><a href="{{route('customer_request.rejected_list')}}" class="sub-link @if( $url=='customer_request.rejected_list') active @endif "> Rejected Request</a> </li>
            </ul>
        </li>
        <?php } ?>

        <?php if(checkPermission(['super_admin']) || checkPermission(['admin']) || checkPermission(['executive'])){ ?>
        <li class="br-menu-item">
            <a href="#" class="br-menu-link with-sub @if( $url=='customer.list' || $url=='customer.details' || $url=='customer.edit' || $url=='customer.update' || $url=='customer.create') active @endif">
                <i class="menu-item-icon icon ion-ios-photos-outline tx-20"></i>
                <span class="menu-item-label">Customer Management</span>
            </a><!-- br-menu-link -->
            <ul class="br-menu-sub">
                <li class="sub-item"><a href="{{route('customer.list')}}" class="sub-link @if( $url=='customer.list' || $url=='customer.details' || $url=='customer.edit' || $url=='customer.update') active @endif "> Customer List</a> </li>
                <li class="sub-item"><a href="{{route('customer.create')}}" class="sub-link @if( $url=='customer.create') active @endif "> Add New Customer</a> </li>
            </ul>
        </li>
        <?php } ?>

        <?php  if(checkPermission(['super_admin']) || checkPermission(['admin'])){ ?>
        <li class="br-menu-item">
            <a href="#" class="br-menu-link with-sub @if($url=='employee.list' || $url=='employee.new.create') active @endif ">
                <i class="menu-item-icon icon ion-ios-photos-outline tx-20"></i>
                <span class="menu-item-label">Employee Management</span>
            </a><!-- br-menu-link -->
            <ul class="br-menu-sub">
                <li class="sub-item"><a href="{{route('employee.list')}}" class="sub-link @if($url=='employee.list') active @endif "> Employee List</a> </li>
                <li class="sub-item"><a href="{{route('employee.new.create')}}" class="sub-link @if($url=='employee.new.create') active @endif "> Create Employee</a> </li>
            </ul>
        </li>
        <?php } ?>

        <?php if(checkPermission(['super_admin']) || checkPermission(['admin']) || checkPermission(['executive'])){ ?>
        <li class="br-menu-item">
            <a href="#" class="br-menu-link with-sub @if($url=='employee.attendance.daily_attendance' || $url=='employee.attendance.myself_date_wise_attendance_view' || $url=='employee.attendance.myself_date_wise_attendance_data' || $url=='employee.attendance.import_from_excel' || $url=='employee.attendance.todays_attendance' || $url=='employee.attendance.daily_attendance_view' || $url=='employee.attendance.date_wise_view' || $url=='employee.attendance.daily_attendance_view' || $url == 'employee.attendance.date_wise_attendance_view' || $url == 'employee.attendance.date_wise_attendance_data' || $url == 'employee.attendance.give_attendance_by_app') active @endif ">
                <i class="menu-item-icon icon ion-ios-photos-outline tx-20"></i>
                <span class="menu-item-label">Attendance Management</span>
            </a><!-- br-menu-link -->
            <ul class="br-menu-sub">
                <?php if(checkPermission(['super_admin'])){ ?>
                <?php }else{ ?>
                    <li class="sub-item"><a href="{{route('employee.attendance.myself_date_wise_attendance_view')}}" class="sub-link @if($url=='employee.attendance.myself_date_wise_attendance_view' || $url=='employee.attendance.myself_date_wise_attendance_data') active @endif "> My Attendance History</a> </li>
                <?php } ?>

                <?php if(checkPermission(['super_admin']) || checkPermission(['admin'])){ ?>
                    <li class="sub-item"><a href="{{route('employee.attendance.todays_attendance')}}" class="sub-link @if($url=='employee.attendance.todays_attendance') active @endif "> Today's History</a> </li>
                    <li class="sub-item"><a href="{{route('employee.attendance.daily_attendance_view')}}" class="sub-link @if($url=='employee.attendance.daily_attendance_view') active @endif ">Daily Attendance History</a> </li>
                    <li class="sub-item"><a href="{{route('employee.attendance.date_wise_attendance_view')}}" class="sub-link @if($url=='employee.attendance.date_wise_attendance_view' || $url == 'employee.attendance.date_wise_attendance_data') active @endif "> Date Wise History</a> </li>
                    <li class="sub-item"><a href="{{route('employee.attendance.import_from_excel')}}" class="sub-link @if($url=='employee.attendance.import_from_excel') active @endif "> Import Attendance</a> </li>
                <?php } ?>
            </ul>
        </li>
        <?php } ?>


        <?php if(checkPermission(['super_admin']) || checkPermission(['admin']) || checkPermission(['executive'])){ ?>
        <li class="br-menu-item">
            <a href="#" class="br-menu-link with-sub @if($url=='employee.leave.type_list' || $url=='settings.leave.holidays_observances_list' || $url=='employee.leave.assign.view' || $url == 'employee.leave.new_request' ) active @endif ">
                <i class="menu-item-icon icon ion-ios-photos-outline tx-20"></i>
                <span class="menu-item-label">Leave Management</span>
            </a><!-- br-menu-link -->
            <ul class="br-menu-sub">
                <?php if(checkPermission(['super_admin'])){ ?>
                <?php }else{ ?>
                    <li class="sub-item"><a href="{{route('employee.leave.new_request')}}" class="sub-link @if($url=='employee.leave.new_request') active @endif "> Leave Request</a> </li>
                <?php } ?>
                <?php if(checkPermission(['super_admin']) || checkPermission(['admin'])){ ?>
                    <li class="sub-item"><a href="{{route('employee.leave.assign.view')}}" class="sub-link @if($url=='employee.leave.assign.view') active @endif "> Assign Leave </a> </li>
                    <li class="sub-item"><a href="{{route('settings.leave.holidays_observances_list')}}" class="sub-link @if($url=='settings.leave.holidays_observances_list') active @endif "> Holidays and Observances </a> </li>
                    <li class="sub-item"><a href="{{route('employee.leave.type_list')}}" class="sub-link @if($url=='employee.leave.type_list') active @endif "> Leave Setting </a> </li>
                <?php } ?>
            </ul>
        </li>
        <?php } ?>

        <?php if(checkPermission(['super_admin']) || checkPermission(['admin']) || checkPermission(['executive'])){ ?>
        <li class="br-menu-item">
            <a href="#" class="br-menu-link with-sub @if( $url == 'employee_award.view' || $url == 'employee_award.data' || $url == 'employee_award.new') active @endif ">
                <i class="menu-item-icon fas fa-award tx-20"></i>
                <span class="menu-item-label">Employee Award</span>
            </a><!-- br-menu-link -->
            <ul class="br-menu-sub">
                <li class="sub-item"><a href="{{route('employee_award.view')}}" class="sub-link @if($url=='employee_award.view' || $url == 'employee_award.data') active @endif "> Award History</a> </li>
                <li class="sub-item"><a href="{{route('employee_award.new')}}" class="sub-link @if($url=='employee_award.new') active @endif "> New Award </a> </li>
            </ul>
        </li>
        <?php } ?>

        <?php if(checkPermission(['super_admin']) || checkPermission(['admin']) || checkPermission(['executive'])){ ?>
        <li class="br-menu-item">
            <a href="#" class="br-menu-link with-sub @if( $url == 'notice_list.list' || $url == 'notice_list.new') active @endif ">
                <i class="menu-item-icon fas fa-bullhorn tx-20"></i>
                <span class="menu-item-label">Notice Management</span>
            </a><!-- br-menu-link -->
            <ul class="br-menu-sub">
                <li class="sub-item"><a href="{{route('notice_list.list')}}" class="sub-link @if($url=='notice_list.list') active @endif "> All Notice</a> </li>
                <li class="sub-item"><a href="{{route('notice_list.new')}}" class="sub-link @if($url=='notice_list.new') active @endif "> Add New Notice </a> </li>
            </ul>
        </li>
        <?php } ?>
        <?php  if(checkPermission(['super_admin']) || checkPermission(['admin'])){ ?>
        <li class="br-menu-item">
            <a href="#" class="br-menu-link with-sub @if($url=='expense_category.list' || $url == 'expense_list.view' || $url == 'expense_list.data' || $url == 'expense_list.new') active @endif ">
                <i class="menu-item-icon icon ion-ios-photos-outline tx-20"></i>
                <span class="menu-item-label">Expense Management</span>
            </a><!-- br-menu-link -->
            <ul class="br-menu-sub">
                <li class="sub-item"><a href="{{route('expense_list.view')}}" class="sub-link @if($url=='expense_list.view' || $url == 'expense_list.data') active @endif "> Expense History</a> </li>
                <li class="sub-item"><a href="{{route('expense_category.list')}}" class="sub-link @if($url=='expense_category.list') active @endif "> Expense Category List</a> </li>
                <li class="sub-item"><a href="{{route('expense_list.new')}}" class="sub-link @if($url=='expense_list.new') active @endif "> New Expense </a> </li>
            </ul>
        </li>
        <?php } ?>

        <?php  if(checkPermission(['super_admin']) || checkPermission(['admin'])){ ?>
        <li class="br-menu-item">
            <a href="#" class="br-menu-link with-sub @if($url=='room_category.list' || $url=='hotel_room.list') active @endif ">
                <i class="menu-item-icon icon ion-ios-photos-outline tx-20"></i>
                <span class="menu-item-label">Hotel Setup</span>
            </a><!-- br-menu-link -->
            <ul class="br-menu-sub">
                <li class="sub-item"><a href="{{route('hotel_room.list')}}" class="sub-link @if($url=='hotel_room.list') active @endif "> Hotel Room List</a> </li>
                <li class="sub-item"><a href="{{route('room_category.list')}}" class="sub-link @if($url=='room_category.list') active @endif "> Room Category List</a> </li>
            </ul>
        </li>
        <?php } ?>

        <?php if(checkPermission(['super_admin']) || checkPermission(['admin'])){ ?>
        <li class="br-menu-item">
            <a href="#" class="br-menu-link with-sub @if($url=='company_information.list' || $url=='department.list' || $url=='designation.list' || $url=='gender.list') active @endif">
                <i class="menu-item-icon fa fa-cog tx-20"></i>
                <span class="menu-item-label">System Settings</span>
            </a><!-- br-menu-link -->
            <ul class="br-menu-sub ">
                <li class="sub-item"><a href="{{route('department.list')}}" class="sub-link @if($url=='department.list') active @endif"> Department List</a> </li>
                <li class="sub-item"><a href="{{route('designation.list')}}" class="sub-link @if($url=='designation.list') active @endif"> Designation List</a> </li>
                <li class="sub-item"><a href="{{route('gender.list')}}" class="sub-link @if($url=='gender.list') active @endif"> Gender List</a> </li>
                <li class="sub-item"><a href="{{route('work_shift.list')}}" class="sub-link @if($url=='gender.list') active @endif"> Work Shift List</a> </li>
                <li class="sub-item"><a href="{{route('weekend_holiday.list')}}" class="sub-link @if($url=='gender.list') active @endif"> Weekend Holiday List</a> </li>
            </ul>
        </li>
        <?php } ?>
        
        <?php if(checkPermission(['executive'])){ ?>

        <li class="br-menu-item">
            <a href="{{route('notice_list.general')}}" class="br-menu-link @if($url=='notice_list.general') active @endif ">
                <i class="menu-item-icon fas fa-bullhorn tx-20"></i> &nbsp;
                <span class="menu-item-label">Notice Board</span>
            </a><!-- br-menu-link -->
        </li><!-- br-menu-item -->

        <li class="br-menu-item">
            <a href="{{route('employee.list.general')}}" class="br-menu-link @if($url=='employee.list.general') active @endif ">
                <i class="menu-item-icon icon ion-person-stalker tx-24"></i>
                <span class="menu-item-label">Employee List</span>
            </a><!-- br-menu-link -->
        </li><!-- br-menu-item -->
        <?php } ?>

        <?php if(checkPermission(['customer'])){ ?>
        <li class="br-menu-item">
            <a href="{{route('customer_portal.reservation_list')}}" class="br-menu-link @if($url=='customer_portal.reservation_list') active @endif ">
                <i class="menu-item-icon icon ion-ios-photos-outline tx-24"></i>
                <span class="menu-item-label">Reservation List</span>
            </a><!-- br-menu-link -->
        </li><!-- br-menu-item -->
        <li class="br-menu-item">
            <a href="{{route('customer_portal.service_request_list')}}" class="br-menu-link @if($url=='customer_portal.service_request_list') active @endif ">
                <i class="menu-item-icon fa fa-list tx-20"></i>
                <span class="menu-item-label">Existing Service Request</span>
            </a><!-- br-menu-link -->
        </li><!-- br-menu-item -->
        <li class="br-menu-item">
            <a href="{{route('customer_portal.new_service_request')}}" class="br-menu-link @if($url=='customer_portal.new_service_request') active @endif ">
                <i class="menu-item-icon fa fa-plus-square tx-24"></i>
                <span class="menu-item-label">New Service Request</span>
            </a><!-- br-menu-link -->
        </li><!-- br-menu-item -->
        <?php } ?>
        
        <li class="br-menu-item">
            <a href="{{url('logout')}}" class="br-menu-link">
                <i class="fas fa-sign-out-alt tx-18"></i>
                <span class="menu-item-label">Log out</span>
            </a>
        </li>

        <li class="br-menu-item" style="text-align: center; color: #aaa; margin-top: 10px; padding-top: 5px; border-top: 1px solid #555">
            EasyPalace V1.1
        </li>

    </ul>
    <br>
</div>

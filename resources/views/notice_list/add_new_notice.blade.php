@extends('layout.master')
@section('title','Add New Notice')
@section('extra_css')
@endsection
@section('content')
    <div class="br-pagetitle my-pagetitle">
        <div>
            <h4> <i class="icon icon ion-ios-bookmarks-outline"></i>Add New Notice</h4>
        </div>

        <div class="pagetitle-btn">
        </div>
    </div>
    <div class="br-pagebody br-pagebody-1">
        <div class="br-section-wrapper">
            <div class="table-wrapper">
                {!! Form::open(['method'=>'POST', 'route'=>'notice_list.new.store', 'files' =>true]) !!}
                    <div class="row">
                
                        <div class="col-sm-10">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-3 control-label form-label-1">Notice Title <span class="msb-txt-red">*</span></label>
                                <div class="col-sm-8 pl-0">
                                    <input type="text" class="form-control" name="notice_title" autocomplete="off" placeholder="Notice Title" required="" >
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-10">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-3 control-label form-label-1"> Notice Start date<span class="msb-txt-red">*</span></label>
                                <div class="col-sm-8 pl-0">
                                    <input type="text" class="form-control fc-datepicker" name="start_date" autocomplete="off" placeholder="Notice Start date" required="">
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-10">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-3 control-label form-label-1"> Notice End Date<span class="msb-txt-red">*</span></label>
                                <div class="col-sm-8 pl-0">
                                    <input type="text" class="form-control fc-datepicker" name="end_date" autocomplete="off" placeholder="Notice End Date" required="">
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-10">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-3 control-label form-label-1">Notice Description <span class="msb-txt-red">*</span></label>
                                <div class="col-sm-8 pl-0">
                                    <textarea class="form-control" rows="6" name="description" id="mytextare" autocomplete="off" placeholder="Notice Description" required=""></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-10">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-3 control-label form-label-1">Notice Attachment</label>
                                <div class="col-sm-8 pl-0">
                                    <input type="file" class="form-control" name="attachment" autocomplete="off" placeholder="Notice Attachment">
                                </div>
                            </div>
                        </div>

                    </div>
                    <hr>
                    <div class="form-group text-center col-sm-8">
                        <button class="btn btn-info btn-sm custom-btn-1 ml-2" type="submit"> <i class="fa fa-save"></i> Save Information</button>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
@section('extra_js')
@endsection

@extends('layout.master')
@section('title','Notice List')
@section('extra_css')
@endsection
@section('content')

    <div class="br-pagetitle my-pagetitle">
        <div>
            <h4> <i class="icon icon ion-ios-bookmarks-outline"></i> Notice List</h4>
        </div>

        <div class="pagetitle-btn">
        </div>
    </div>
    <div class="br-pagebody br-pagebody-1">
        <div class="br-section-wrapper">
            <div class="table-wrapper scrollme">
                <table id="datatable" class="table row-border order-column table-bordered table-hover" >
                    <thead>
                        <tr>    
                            <th class="msb-txt-center msb-bg-table-td" >SN</th>
                            <th class="msb-txt-center msb-bg-table-td" >Notice Name</th>
                            <th class="msb-bg-table-td" width="45%">Description</th>
                            <th class="msb-txt-center msb-bg-table-td" >Notice Period</th>
                            <th class="msb-txt-center msb-bg-table-td" >Attachment</th>
                            <th class="msb-txt-center msb-bg-table-td" >Status</th>
                            <th class="msb-txt-center msb-bg-table-td" >Created By</th>
                        </tr>
                    </thead>
                    <tbody>
                    @php $i=0; @endphp
                    @foreach($notice_list as $item)
                        <tr>
                            <td class="msb-txt-center">{{++$i}}</td>
                            <td class="msb-txt-center">{{$item->notice_title}}</td>
                            <td>{{$item->description}}</td>
                            <td class="msb-txt-center">{{date('M-Y', strtotime($item->start_date))}} - {{date('M-Y', strtotime($item->end_date))}}</td>
                            <td class="msb-txt-center">
                                @if(!empty($item->attachment))
                                    <a href="{{asset('notice_attachment/'.$item->attachment)}}" target="new" class=""><i class="fa fa-download"></i> Download</a>
                                @endif
                            </td>
                            <td class="msb-txt-center">@if($item->status==1) <span class="msb-txt-green">Active</span> @elseif($item->status==0) <span class="msb-txt-red">Inactive</span> @endif</td>
                            <td class="msb-txt-center">
                                {{$item->name}}
                            </td>
                        </tr>
                    @endforeach
                    </tbody> 
                </table>
            </div>
        </div>
    </div>
@endsection
@section('extra_js')
@endsection

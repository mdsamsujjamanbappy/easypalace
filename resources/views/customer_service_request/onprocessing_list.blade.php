@extends('layout.master')
@section('title','On Proccessing Service Request List')
@section('extra_css')
@endsection
@section('content')

    <div class="br-pagetitle my-pagetitle">
        <div>
            <h4> <i class="icon icon ion-ios-bookmarks-outline"></i> On Proccessing Service Request List</h4>
        </div>

        <div class="pagetitle-btn">
        </div>
    </div>
    <div class="br-pagebody br-pagebody-1">
        <div class="br-section-wrapper">
            <div class="table-wrapper scrollme">
                <table id="datatable" class="table row-border order-column table-bordered table-hover" >
                    <thead>
                        <tr>    
                            <th class="msb-txt-center msb-bg-table-td" >SN</th>
                            <th class="msb-txt-center msb-bg-table-td" >Request Date</th>
                            <th class="msb-txt-center msb-bg-table-td" >Customer Phone</th>
                            <th class="msb-txt-center msb-bg-table-td" >Reservation Number</th>
                            <th class="msb-txt-center msb-bg-table-td" >Service Title</th>
                            <th class="msb-txt-center msb-bg-table-td" >Request Details</th>
                            <th class="msb-txt-center msb-bg-table-td" >Status</th>
                            <th class="msb-txt-center msb-bg-table-td" >Accepted By</th>
                            <th class="msb-txt-center msb-bg-table-td" >Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @php $i=0; @endphp
                    @foreach($onprocessing_list as $item)
                        <tr>
                            <td class="msb-txt-center">{{++$i}}</td>
                            <td class="msb-txt-center">{{date('d-m-Y', strtotime($item->request_date))}}</td>
                            <td class="msb-txt-center">{{$item->customer_phone}}</td>
                            <td class="msb-txt-center"><a href="{{route('customer_reservation.reservation_details', base64_encode($item->reservation_id))}}" target="_BLANK" title="">{{$item->reservation_number}}</a></td>
                            <td class="msb-txt-center">{{$item->service_title}}</td>
                            <td class="msb-txt-center">{{$item->request_details}}</td>
                            <td class="msb-txt-center">
                                @if($item->status==0) <span class="msb-txt-black">Pending</span> 
                                @elseif($item->status==1) <span class="msb-txt-blue">On processing</span> 
                                @elseif($item->status==2) <span class="msb-txt-green">Completed</span> 
                                @endif</td>
                            <td class="msb-txt-center">{{$item->acceptedBy}}</td>
                            <td class="msb-txt-center">
                                <button type="button" value="{{base64_encode($item->id)}}" class="accept msb-txt-white msb-bg-green" title="Complete">Mark as Completed</button>
                                <button type="button" value="{{base64_encode($item->id)}}" class="reject msb-txt-white msb-bg-red" title="Reject Request"><i class="fa fa-trash-alt"></i></button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody> 
                </table>
            </div>
        </div>
    </div>

    <div id="accept_modal" class="modal fade effect-sign">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content bd-0 tx-14">
                <div class="modal-header pd-y-15 pd-x-25 modal_header_1">
                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="fas fa-calendar-check"></i>  Service Request </h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fas fa-check"></i></span>
                    </button>
                </div>
                <div class="modal-body pd-20">
                    <span id="form_result"></span>
                    <div class="modal_body_inner">
                        {!! Form::open(['method'=>'POST','route'=>'customer_request.accept_request']) !!}
                        <div class="row">
                            <div class="col-12 mb-3">
                                <h6>Completance Note</h6>
                                <textarea type="text" class="form-control" name="request_feedback" autocomplete="off" required="" placeholder="Completance Note" rows="4"></textarea>
                            </div>
                        </div>
                        <div class="col-sm-12"><br /></div>

                        <div class="form-group text-center col-sm-12">
                            <input type="hidden" name="id" id="accept_id">
                            <input type="hidden" name="status" value="2">
                            <button class="btn btn-info btn-sm custom-btn-1 ml-2" type="submit"><i class="fa fa-save"></i> Submit </button>
                            <button type="button" class="btn btn-danger btn-sm custom-btn-1 tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div><!-- modal-dialog -->
    </div><!-- modal -->

    <div id="reject_modal" class="modal fade effect-sign">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content bd-0 tx-14">
                <div class="modal-header pd-y-15 pd-x-25 modal_header_1">
                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="fa fa-trash-alt "></i> Service Request Rejection</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fas fa-times"></i></span>
                    </button>
                </div>
                <div class="modal-body pd-20">
                    <span id="form_result"></span>
                    <div class="modal_body_inner">
                        {!! Form::open(['method'=>'POST','route'=>'customer_request.reject_request']) !!}
                        <div class="row">
                            <div class="col-12 mb-3">
                                <h6>Reason of Rejection</h6>
                                <textarea type="text" class="form-control" name="request_feedback" autocomplete="off" required="" placeholder="Reason of Rejection" id="reason" rows="4"></textarea>
                            </div>
                        </div>
                        <div class="col-sm-12"><br /></div>

                        <div class="form-group text-center col-sm-12">
                            <input type="hidden" name="id" id="reject_id">
                            <button class="btn btn-info btn-sm custom-btn-1 ml-2" type="submit"><i class="fa fa-save"></i> Submit </button>
                            <button type="button" class="btn btn-danger btn-sm custom-btn-1 tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div><!-- modal-dialog -->
    </div><!-- modal -->

@endsection
@section('extra_js')
<script>
    $(document).ready(function(){
        'use strict';

        $(document).on('click', '.reject', function(){
             var id = $(this).attr('value');
             $("#reject_modal").modal('show');
             $('#reject_id').val(id);
             $('#reason').focus();
        });

        $(document).on('click', '.accept', function(){
             var id = $(this).attr('value');
             $("#accept_modal").modal('show');
             $('#accept_id').val(id);
        });
    });
</script>

@endsection
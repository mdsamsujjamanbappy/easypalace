@extends('layout.master')
@section('title','Service Request List')
@section('extra_css')
@endsection
@section('content')

    <div class="br-pagetitle my-pagetitle">
        <div>
            <h4> <i class="icon icon ion-ios-bookmarks-outline"></i> Service Request List</h4>
        </div>

        <div class="pagetitle-btn">
        </div>
    </div>
    <div class="br-pagebody br-pagebody-1">
        <div class="br-section-wrapper">
            <div class="table-wrapper scrollme">
                <table id="datatable" class="table row-border order-column table-bordered table-hover" >
                    <thead>
                        <tr>    
                            <th class="msb-txt-center msb-bg-table-td" >SN</th>
                            <th class="msb-txt-center msb-bg-table-td" >Request Date</th>
                            <th class="msb-txt-center msb-bg-table-td" >Customer Phone</th>
                            <th class="msb-txt-center msb-bg-table-td" >Reservation Number</th>
                            <th class="msb-txt-center msb-bg-table-td" >Service Title</th>
                            <th class="msb-txt-center msb-bg-table-td" >Request Details</th>
                            <th class="msb-txt-center msb-bg-table-td" >Request Feedback</th>
                            <th class="msb-txt-center msb-bg-table-td" >Updated By</th>
                            <th class="msb-txt-center msb-bg-table-td" >Status</th>
                        </tr>
                    </thead>
                    <tbody>
                    @php $i=0; @endphp
                    @foreach($data_list as $item)
                        <tr>
                            <td class="msb-txt-center">{{++$i}}</td>
                            <td class="msb-txt-center">{{date('d-m-Y', strtotime($item->request_date))}}</td>
                            <td class="msb-txt-center">{{$item->customer_phone}}</td>
                            <td class="msb-txt-center">{{$item->reservation_number}}</td>
                            <td class="msb-txt-center">{{$item->service_title}}</td>
                            <td class="msb-txt-center">{{$item->request_details}}</td>
                            <td class="msb-txt-center">{{$item->request_feedback}}</td>
                            <td class="msb-txt-center">{{$item->updatedBy}}</td>
                            <td class="msb-txt-center">
                                @if($item->status==0)
                                    <span class="msb-txt-black">Pending</span>
                                @elseif($item->status==1)
                                    <span class="msb-txt-blue">On Processing</span>
                                @elseif($item->status==2)
                                    <span class="msb-txt-green">Completed</span>
                                @elseif($item->status==3)
                                    <span class="msb-txt-red">Cancelled</span>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody> 
                </table>
            </div>
        </div>
    </div>

    <div id="reject_modal" class="modal fade effect-sign">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content bd-0 tx-14">
                <div class="modal-header pd-y-15 pd-x-25 modal_header_1">
                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="fa fa-trash-alt "></i> Service Request Rejection</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fas fa-times"></i></span>
                    </button>
                </div>
                <div class="modal-body pd-20">
                    <span id="form_result"></span>
                    <div class="modal_body_inner">
                        {!! Form::open(['method'=>'POST','route'=>'customer_request.reject_request']) !!}
                        <div class="row">
                            <div class="col-12 mb-3">
                                <h6>Reason of Rejection</h6>
                                <textarea type="text" class="form-control" name="request_feedback" autocomplete="off" required="" placeholder="Reason of Rejection" id="reason" rows="4"></textarea>
                            </div>
                        </div>
                        <div class="col-sm-12"><br /></div>

                        <div class="form-group text-center col-sm-12">
                            <input type="hidden" name="id" id="reject_id">
                            <button class="btn btn-info btn-sm custom-btn-1 ml-2" type="submit"><i class="fa fa-save"></i> Submit </button>
                            <button type="button" class="btn btn-danger btn-sm custom-btn-1 tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div><!-- modal-dialog -->
    </div><!-- modal -->
@endsection
@section('extra_js')
<script>
    $(document).ready(function(){
        'use strict';

        $(document).on('click', '.reject', function(){
             var id = $(this).attr('value');
             $("#reject_modal").modal('show');
             $('#reject_id').val(id);
             $('#reason').focus();
        });
    });
</script>

@endsection

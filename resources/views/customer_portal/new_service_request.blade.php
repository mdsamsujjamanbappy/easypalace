@extends('layout.master')
@section('title','New Service Request')
@section('extra_css')
@endsection
@section('content')
    <div class="br-pagetitle my-pagetitle">
        <div>
            <h4> <i class="icon icon ion-ios-bookmarks-outline"></i> New Service Request</h4>
        </div>

        <div class="pagetitle-btn">
        </div>
    </div>
    <div class="br-pagebody br-pagebody-1">
        <div class="br-section-wrapper">
            <div class="table-wrapper">
                {!! Form::open(['method'=>'POST', 'route'=>'customer_portal.new_service_request.store']) !!}
                    <div class="row">
                
                        <div class="col-sm-8">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-3 control-label form-label-1"> Reservation Number<span class="msb-txt-red">*</span></label>
                                <div class="col-sm-8 pl-0">
                                    <select class="form-control" name="reservation_id" id="reservation_id" required="">
                                        <option value="">Select Reservation Number</option>
                                        @foreach($reservations as $item)
                                          <option value="{{$item->id}}">{{$item->reservation_number}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-8">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-3 control-label form-label-1">Service Title<span class="msb-txt-red">*</span></label>
                                <div class="col-sm-8 pl-0">
                                    <input type="text" class="form-control" name="service_title" autocomplete="off" placeholder="Example: Food" required="" >
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-8">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-3 control-label form-label-1">Request Details<span class="msb-txt-red">*</span></label>
                                <div class="col-sm-8 pl-0">
                                    <textarea class="form-control" rows="4" name="request_details" autocomplete="off" placeholder="Request Details"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group text-center col-sm-8">
                        <button class="btn btn-info btn-sm custom-btn-1 ml-2" type="submit"> <i class="fa fa-save"></i> Save Information</button>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
@section('extra_js')
@endsection

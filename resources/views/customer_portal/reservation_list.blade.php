@extends('layout.master')
@section('title','Reservation List')
@section('extra_css')
    {{ Html::style('theme/css/resources/customer_reservation.css') }}
@endsection
@section('content')

    <div class="br-pagetitle my-pagetitle">
        <div>
            <h4> <i class="icon icon ion-ios-bookmarks-outline"></i> Reservation List</h4>
        </div>

        <div class="pagetitle-btn">
        </div>
    </div>
    <div class="br-pagebody br-pagebody-1">
        <div class="br-section-wrapper">
            <div class="table-wrapper scrollme">
            
                <table id="datatable" class="table row-border order-column table-bordered stripe  table-hover" >
                    <thead>
                        <tr>
                            <th class="msb-txt-center" width="5%">SN</th>
                            <th class="msb-txt-center">Reservation Number</th>
                            <th class="msb-txt-center">Customer Name</th>
                            <th class="msb-txt-center">Customer Phone</th>
                            <th class="msb-txt-center">Reserved Room</th>
                            <th class="msb-txt-center">Reservation Date</th>
                            <th class="msb-txt-center">Check in Date</th>
                            <th class="msb-txt-center">Check out Date</th>
                            <th class="msb-txt-center">Total Bill</th>
                            <th class="msb-txt-center">Payment</th>
                            <th class="msb-txt-center">Due Amount</th>
                            <th class="msb-txt-center">Status</th>
                        </tr>
                    </thead>
                    <tbody>
                    @php $i=0; @endphp
                    @foreach($reservation_list as $item)
                        <tr>
                            <td class="msb-txt-center">{{++$i}}</td>
                            <td class="msb-txt-center">{{$item->reservation_number}}</td>
                            <td class="msb-txt-center">{{$item->customer_name}}</td>
                            <td class="msb-txt-center">{{$item->customer_phone}}</td>
                            <td class="msb-txt-center">@foreach($item->reservation_room_list as $item1){{$item1->room_category_name}} - {{$item1->room_name_number}} <br >@endforeach</td>
                            <td class="msb-txt-center">{{date('d-m-Y', strtotime($item->reservation_date))}}</td>
                            <td class="msb-txt-center">{{date('d-m-Y', strtotime($item->check_in_date))}}</td>
                            <td class="msb-txt-center">{{date('d-m-Y', strtotime($item->check_out_date))}}</td>
                            <td class="msb-txt-center">@money($item->billing_history)</td>
                            <td class="msb-txt-center">@money($item->payment_history)</td>
                            <td class="msb-txt-center">@money($item->billing_history - $item->payment_history)</td>
                            <td class="msb-txt-center">
                                @if($item->status==1)
                                    <span class="msb-txt-blue">Active</span>
                                @elseif($item->status==2)
                                    <span class="msb-txt-green">Closed</span>
                                @elseif($item->status==3)
                                    <span class="msb-txt-red">Cancelled</span>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

        </div>
    </div>
@endsection

@section('extra_js')
    {{ Html::script('theme/js/resources/customer_reservation.js') }}
@endsection
@extends('layout.master')
@section('title',' Employee Award History')
@section('extra_css')
@endsection
@section('content')
    <div class="br-pagetitle my-pagetitle">
        <div>
            <h4> <i class="icon icon ion-ios-bookmarks-outline"></i> Employee  Award History</h4>
        </div>

        <div class="pagetitle-btn">
        </div>
    </div>
    <div class="br-pagebody br-pagebody-1">
        <div class="br-section-wrapper">
            <div class="table-wrapper">
                {!! Form::open(['method'=>'POST','route'=>'employee_award.data']) !!}
                <div class="row">
                    <div class="col-sm-7 col-offset-sm-2">
                         <div class="row">

                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <label for="form-label" class="col-sm-4 control-label form-label-1"> Award Period <span class="msb-txt-red">*</span></label>
                                    <div class="col-sm-4 pl-0">
                                        <input type="text" class="form-control fc-monthpicker" name="start_date" autocomplete="off" value="{{date('Y-m')}}" required="" placeholder="Click here to select date">
                                    </div>
                                    <div class="col-sm-4 pl-0">
                                        <input type="text" class="form-control fc-monthpicker" name="end_date" autocomplete="off" value="{{date('Y-m')}}" required="" placeholder="Click here to select date">
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-sm-12"><br></div>

                        <div class="form-group text-center col-sm-12">
                            <button class="btn btn-primary btn-sm custom-btn-1 ml-2" name="submitBtn" value="view" type="submit"><i class="fa fa-search"></i> View Award History </button>
                        </div>   
                    </div>    
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

@endsection

@section('extra_js')
@endsection
